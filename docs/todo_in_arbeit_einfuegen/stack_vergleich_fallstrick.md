# Stack muss genau gleich groß sein
Die stacks müssen genau gleich groß sein.
Bei präemptiven Multitasking muss sichergestellt werden, dass die Timings, zwischen denen der Kontextwechsel kommt exakt (auf die instruktion genau) eingehalten werden.
Treten in dem Fall interrupts auf bei den Prozessen, ist das ganze doof.

# Fallstrick beim Stackvergleich
Manche Controller erfordern es, dass beim Eintritt in einen Exception Handler der Stack an einer 8-byte Grenze ausgerichtet ist.

Ist dies der Fall, ergibt sich ein Unterschied im Stack, wenn mit Interrupts gearbeitet wird.

- Stack von P1 befindet sich bei 0x2000'0f00 (an grenze ausgerichtet)
- Interrupt tritt auf, stack wird mit r0-r3,pc,lr,xpsr beschrieben
- Interrupt beendet, stack wird zurückgebaut
- Eintritt in eine Funktion, Stackpointer wird um 4 byte erhöht
  - Stack is nicht mehr 8-byte aligned
- Interrupt tritt auf 
  - Controller überspringt beim Stack sichern eine adresse und beginnt bei psp+4
  - Nun steht im Stack ein Wert, abhängig davon, ob vorher ein Interrupt passiert ist oder nicht

Z.B.:

1.
```

00 00 00 00   00 00 00 00   00 00 00 00   00 00 00 00
00 00 00 00   00 00 00 00   00 00 00 00   00 00 00 00
<   SP    >
```
2. Interrupt tritt ein
```
    SP                                          
<e4 2f 00 20>   04 00 00 00   00 00 00 00   e4 2f 00 20 
 00 00 00 00    99 80 00 08   64 80 00 08   00 00 00 01

```
3. Interrupt beendet
```
e4 2f 00 20   04 00 00 00   00 00 00 00   e4 2f 00 20 
00 00 00 00   99 80 00 08   64 80 00 08   00 00 00 01
<   SP    >
```
4. Funktionseintritt
```
e4 2f 00 20   04 00 00 00   00 00 00 00    e4 2f 00 20 
00 00 00 00   99 80 00 08   64 80 00 08   <00 00 00 01>
```
- Hier kann es auch schon zu einem Problem kommen, wenn die Funktion mehrere Datenworte auf dem Stack reserviert und dann der Interrupt kommt, bevor die reservierten Werte beschrieben werden


```
00 00 00 00   00 00 00 00   'e4 2f 00 20   04 00 00 00   
00 00 00 00   e4 2f 00 20    00 00 00 00   9d 80 00 08
5a 80 00 08   00 02 00 01'   54 80 00 08   <e0 2f 00 20> 
                            ___________   - SP nach Eintritt in die Funktion
                            -Datenwort, welches durch Interrupt 1 Aufruf noch gesetzt ist (wird nicht überschrieben)
              ___________
              Erstes Datenwort, welches beim Eintritt in Interrupt 2 beschrieben wird
```
Die durch den Interrupt beschrieben Werte sind durch ' ' markiert.

# Weiterer Fallstrick, auch mit Interrupts, unabhängig vom Stack alignment
Sind die Werte unterhalb des aktuellen Stackpointers von einem Interrupt beschrieben (ausgangslage: siehe oben 3.), kann es auch noch in einem anderen Fall zu einem Unterschied im Stack kommen.
Werden (z.B. beim Funktionseintritt) z.B. 3 Worte reserviert (Stackpointer wird um 3x4 erhöht) und dann, bevor diese Worte beschrieben werden, kommt es zu einem Interrupt, sind die reservierten Worte zwar Teil des Stacks, sind aber in dem Prozess wo Interrupt 1 aufgetreten ist unterschiedlich zu den anderen Prozessen, in denen Interrupt 1 nicht (oder nicht an dieser Stelle, oder ein anderer Interrupt) aufgetreten ist.

```
times_2_plus_2(int):
        push    {r7}
        sub     sp, sp, #20
        -------------------- Hier kommt der interrupt
        add     r7, sp, #0
        str     r0, [r7, #4]
        ldr     r3, [r7, #4]
        lsls    r3, r3, #1
        str     r3, [r7, #12]
```


# Lösung des Problems
Tja.

1. Letzten Stackframe ignorieren
   - Wie soll ich rausbekommen, was der letzte Stackframe war.
     - Frame Pointer (kann nicht verwendet werden)
        - von https://stackoverflow.com/questions/15752188/arm-link-register-and-frame-pointer :
        ```
        Some register calling conventions are dependent on the ABI (Application Binary Interface). The FP is required in the APCS standard and not in the newer AAPCS (2003). For the AAPCS (GCC 5.0+) the FP does not have to be used but certainly can be; debug info is annotated with stack and frame pointer use for stack tracing and unwinding code with the AAPCS. If a function is static, a compiler really doesn't have to adhere to any conventions.
        ```
        Die Verwendung des Framepointers wird nicht mehr für die AAPCS erforderlich. Der GCC verwendet den nicht.
        Mit Framepointer wäre der Funktionsprolog folgendermaßen:
        ```asm 
        push {fp, lr}
        mov fp, sp              // FP points to frame record
        ```
        Der FP würde also immer eine Position markieren, die vor den lokalen Variablen des aktuellen stackframes ist. damit wäre das problem gelöst.
        
        Der GCC kompiliert dies allerdings nicht so. Er verwendet keinen Frame-Pointer, mit der auf vorherige Stackframes zeigt.
        Stattdessen passiert dies:
        ```asm
        // fp = r7
        push    {fp}
        sub     sp, sp, #28
        add     fp, sp, #0
        ```
        Der Framepointer wird erst beschrieben, wenn schon platz auf dem Stack für alle Variablen reserviert wurde.
        Damit kann das Register `fp` nicht verwendet werden, um von dort aus den Stack zu vergleichen, da sich die gleichen Probleme ergeben wie oben beschrieben.
     
     - Stack Unwind Tables?\
       Vielleicht wäre es möglich, die Informationen aus den Unwind Tables herauszufinden. Dies wurde aufgrund von zeitlichem Mangel nicht weiter untersucht.
    

     - Gibt es eine maximale Stackframe Größe?\
        Doof, da die Chance fehlerhafte Werte zu übersehen größer ist, je mehr Werte ignoriert werden.
        
     - Im Code nach der letzten stelle suchen, an der der Stackpointer modifiziert wurde? Zu viele Sonderfälle, zu aufwendig.
2. Keine Interrupts während der Ausführung der Prozesse, Interruptbehandlung nur nach dem Scheduling

# Konsequenzen
## Echtzeitfähigkeit
Wenn keine Interrupts während der Ausführungszeit der Anwendung zugelassen sind, müssen die Interrupts zum Zeitpunkt des Schedulings ausgeführt werden. 

Dies muss beim Planen der Echtzeitfähigkeit berücksichtigt werden.
Je nach Anwendungsfall könnte die Echtzeitfähigkeit beeinträchtigt werden.

Abschätzen, ob die Echtzeitschranken eingehalten werden können, kann man, in dem man das zum Zeitpunkt des Scheduling die Worst Case Interrupt Laufzeit ermittelt.

```
| Anwendungslaufzeit      | Scheduling | Worst Case Interrupts    | Anwendung

------------------------------------------------------------------- Worst case Zeitspanne

```