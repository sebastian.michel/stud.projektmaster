> sektionen binär anzeigen:
arm-none-eabi-objdump -s

> nur ausführbare sektionen disassemblieren:
arm-none-eabi-objdump -S (ohne sourcecode: -d)

> alles mit source disassemblieren:
arm-none-eabi-objdump -DS

> nur eine spezielle sektion disassemblieren:
arm-none-eabi-objdump --disassemble=<sektionname>

> sektions informationen anzeigen:
arm-none-eabi-readelf -S

> symbole anzeigen
arm-none-eabi-nm --demangle