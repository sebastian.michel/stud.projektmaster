wenn wir unser programm mit arm-none-eabi-gcc kompilieren, wird implizit die newlib c-library mitgelinkt [1].

wir können verhindern, dass eine c standardlibrary mitgelinkt wird, wenn wir "-nostdlib" dem linker mitgeben

kontrollieren, welche version der newlib verwendet wird, machen wir über den linker flag "--specs=<xxx>"
dabei:
    - --specs=nano.specs : newlib-nano wird gelinkt
    - --specs=nosys.specs :


/////////////////////////// Größe von clib funktionen
Newlib nano macht die funktionen der C library kleiner
z.B. wenn printf verwendet wird und newlib-nano:
ROM:        4704 B        32 KB     14.36%

wird newlib nano nicht verwendet, sondern die normale newlib (--specs wird weggelassen):
ROM:       33436 B        32 KB    102.04%

wird nosys specs verwendet, ist die größe der applikation circa so groß wie unter der verwendung von der normalen newlib:
ROM:       33596 B        32 KB    102.53%

-> Eine code-größen reduzierung findet nur bei newlib-nano statt



[1] https://interrupt.memfault.com/blog/boostrapping-libc-with-newlib