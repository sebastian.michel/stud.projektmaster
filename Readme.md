# Windows installation instructions

## Prerequisites:
- Clion IDE https://www.jetbrains.com/de-de/clion/download/#section=windows
- git for windows https://git-scm.com/downloads
- arm-none-eabi toolchain for Windows (Download the .exe!), version 11.2 (most recent version at the time of this writing, if you want to use a newer version please adapt the paths in toolchain-windows.cmake) https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads
- openocd to flash the software to the microcontroller (https://github.com/xpack-dev-tools/openocd-xpack/releases , select the ...win32-x64.exe)

## Proceed with the normal installation
- Install all the required software listed in section Prerequisites (use the default installation location)
- pull repository `git clone --recurse-submodules https://gitlab.com/sebastian.michel/stud.projektmaster.git`
- open clion
- open git repository folder 
- setup toolchain (see below)

## Setting up the toolchain
When promted, specify the toolchain file using 
`-DCMAKE_TOOLCHAIN_FILE=toolchain-windows.cmake`

![](res/Readme/1.png)

Next select MinGW Makefiles as the generator

![](res/Readme/2.png)


Press okay and apply, switch to the cmake tab in the bottom bar
![](res/Readme/3.png)


Press Reload CMake Project
![](res/Readme/4.png)


- There should be no errors and you should be able to build the software now.

## Add release target to build the release version of this software
![](res/Readme/7.png)
![](res/Readme/8.png)


## Build
try and build the project by selecting 

![](res/Readme/5.png)
