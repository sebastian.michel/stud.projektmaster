//
// Created by seb on 08.10.20.
//

#include "register_io.h"

void setBit_test(volatile uint32_t& target_regitster, uint32_t mask){

}

void setBit(volatile uint32_t* target_register, uint8_t bitNum, bool value) {
    uint32_t mask = (uint32_t) 1 << bitNum;

    if ( value == 1 ) {
        *target_register = (*target_register) | mask;
    } else if ( value == 0 ) {
        *target_register = (*target_register) & ~mask;
    }
}

void setBits(volatile uint32_t* target_register, uint32_t value, uint8_t position) {
    uint32_t clearmask = (~value) << position;
    uint32_t setmask = value << position;
    uint32_t val = *target_register;
    val &= clearmask;
    val |= setmask;
    //*target_register &= (uint32_t)!mask;
    *target_register = val;
}

void resetBits(volatile uint32_t* target_register, uint32_t value, uint8_t position) {
    uint32_t notMask = !(value << position);

    *target_register &= notMask;
}


void setBit_8bitRegister(volatile uint8_t* target_register, uint8_t bitNum, bool value) {
    uint8_t mask = (uint8_t) 1 << bitNum;

    if ( value == 1 ) {
        *target_register = (*target_register) | mask;
    } else if ( value == 0 ) {
        *target_register &= (uint8_t) ~mask;
    }
}

bool readBit(volatile const uint32_t* target_register, uint8_t bitNum) {
    uint32_t mask = (uint32_t) 1 << bitNum;

    uint32_t val = (*target_register) & mask;

    return val != 0;
}

bool readBit_8bitRegister(volatile const uint8_t* target_register, uint8_t bitNum) {
    uint8_t mask = (uint8_t) 1 << bitNum;

    uint8_t val = (*target_register) & mask;

    return val != 0;
}

uint32_t readMasked(volatile const uint32_t* target_register, uint32_t mask) {
    uint32_t current_register_value = *target_register;

    return current_register_value & mask;
}

void writeRegister1Byte(volatile uint8_t* target_register, uint8_t data) {
    (*target_register) = data;
}

uint8_t readRegister1Byte(volatile const uint8_t* target_register) {
    return (*target_register);
}

void writeRegister(volatile uint32_t* target_register, uint32_t data) {
    (*target_register) = data;
}

uint32_t readRegister(volatile const uint32_t* target_register) {
    return (*target_register);
}

void clearRegister(volatile uint32_t* target_register) {
    (*target_register) = 0;
}

uint32_t createMask(uint8_t size, uint8_t* initializer) {
    uint32_t mask = 0;
    for ( uint8_t i = 0; i < size; i++ ) {
        setBit(&mask, initializer[i], 1);
    }

    return mask;
}

void writeMasked(volatile uint32_t* target_register, uint8_t size, uint8_t* array) {
    *target_register = createMask(size, array);
}
