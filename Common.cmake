function(GET_SOURCES_IN_PROJECT src_list)
    file(GLOB_RECURSE source_files
            "*.h"
            "*.cpp"
            "*.c"
            "*.s"
            )
    list(REMOVE_ITEM source_files os-cmake-test/main.cpp)

    #exclude generic assembly files (without assembler specific header)
    list(FILTER source_files EXCLUDE REGEX "^.+.inc.s")

    #exclude arm assembler files
    list(FILTER source_files EXCLUDE REGEX "^.+src-asm-arm.+$")


    if(CMAKE_VERBOSE_MAKEFILE)
        message("")
        message("Source Files:")

        foreach(line IN LISTS source_files)
            message("${line}")
        endforeach()
        message("")
    endif()

    set(${src_list} "${source_files}" PARENT_SCOPE)
endfunction()

function(CREATE src_list)
    message("creating library ${PROJECT_NAME} with source files: ${src_list}")

    add_library(${PROJECT_NAME} "${src_list}")

    message("created lib")

    if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/main.cpp)
        message("main.cpp exists")
        add_executable(${PROJECT_NAME}_app ${CMAKE_CURRENT_SOURCE_DIR}/main.cpp)
        target_link_libraries(${PROJECT_NAME}_app PRIVATE ${PROJECT_NAME})
        message("created app")
    endif()

    if(EXISTS ${CMAKE_CURRENT_LIST_DIR}/tests)
        message("tests exists")

        add_subdirectory(tests)
        #gtest_discover_tests()
    endif()

endfunction()