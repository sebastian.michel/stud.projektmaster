#include <switchContext.h>

#include <RandomGenerator.h>
#include <handler_HardFault.h>
#include <application_header_struct.h>
#include <configure_process_mpu.h>
#include <debug.h>
#include <first_context.h>
#include <loader.h>
#include <logging_config.h>
#include <random.h>
#include <stm32g4xx.h>
#include <syscall_impl.h>
#include <syscalls.h>
#include <systick.h>
#include <timer_general.h>
#include <usart.h>

#include "system_init.h"
#include "systemtimer.h"
#include "user_led.h"

#include "timer.h"

#include "ErrorGenerator.h"

#include "global_variables.h"


using namespace zebos;

void init(){
    init_data_addresses();
    system_init();
    user_led_set();

	zebos::test_status = {};
}

void init_error_timer(){
    /*
    timer_error_generation.prescaler(16000);
    uint32_t random = rng->generate(1050);
     timer_error_generation.setReloadValue(random+1); // at least 1 ms
     */
    timer_error_generation.prescaler(1600);
    uint32_t random = rng->generate(740);
    timer_error_generation.setReloadValue(random+10); // at least 1 ms

    timer_error_generation.disableInterrupt();
}

extern "C"{
	void timer_restart(){
		timer3_start();
	}
}


#include "asm_stack_reset.h"
void init_stack_reset_ram_routine(){
	memcpy(
			&_os_proc_shared_rw,
			&_rom_os_proc_shared_rw,
			(&_os_proc_shared_rw_end - &_os_proc_shared_rw) * 4
			);
}



int main() {
	init();
	init_stack_reset_ram_routine();

	uint32_t rngseed;
	if constexpr (testConfig.disable_random){
		rngseed = 643164616
                ;
	} else {
		rngseed = generate_random_number();
	}

	RandomGenerator random_number_generator(rngseed);

	rng = &random_number_generator;

    init_error_timer();

	lpuart1_init();

    if constexpr (testConfig.log_random_seed){
        char strbuf[15];
        sprintf(strbuf, "%u", rngseed);
        strbuf[11] = '\0';
        zebos::log::msg(strbuf);
        zebos::log::msg(',');
    }

    if constexpr(testConfig.error_generator_register){
        error_generator.config_operation_mode(error_generator_config::OperationMode::Register);
        error_generator.config_enable_all_registers();
    } else if constexpr (testConfig.error_generator_datasegment){
        error_generator.config_operation_mode(error_generator_config::OperationMode::AddressRange);
    }

	timer3_init();
    timer3_prescaler(16000);
	timer3_apply();

	rcc_enable_timer2();

	NVIC_SetPriority(PendSV_IRQn, 255); //pendsv lowest prio

	ProcessDataStorage storage1(process_data_storage[0], process_data_storage_size);
    ProcessDataStorage storage2(process_data_storage[1], process_data_storage_size);
    ProcessDataStorage storage3(process_data_storage[2], process_data_storage_size);

	auto process_group = create_process_group<num_process_group_members>(
			applicationStartAddress,
            {storage1,storage2,storage3}
			).get();

	processGroup = &process_group;

#define SYSTICK 1
#if SYSTICK
    enable_reoccuring_call(20, startContextSwitch);
#else
	{
		//system timer
		rcc_enable_timer5();
		timer_reload(TIM5,10*16000);
		timer_only_update_on_overflow_underflow(TIM5);
		timer_enable_interrupt(TIM5);
		systick_set_interrupt_function(startContextSwitch);
		timer_apply(TIM5);
	}
#endif

    initProcessMpu();

    timer_error_generation.start();
    firstContext();

    zassert(0);
    while(1);
}
