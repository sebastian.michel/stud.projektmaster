cmake_minimum_required(VERSION 3.10)
set (TARGET_NAME ${PROJECT_NAME}.elf)

set(CMAKE_CXX_STANDARD 20)

add_subdirectory(demoapp)
add_subdirectory(os)
add_subdirectory(core-stm32g431)
add_subdirectory(system)
add_subdirectory(cpp_util)

add_executable(${TARGET_NAME} main.cpp os/include/check_test_status.h)
target_include_directories(${TARGET_NAME} PUBLIC CMSIS/Core/Include)
target_include_directories(${TARGET_NAME} PUBLIC os/include)
target_include_directories(${TARGET_NAME} PUBLIC )


# os has to be linked first, otherwise if os references functions from core these function will not get linked as the linker discards the "unused" functions because it didn't yet see they are required from os.a
# also core has to be linked first so it can use reference functions from os (syscall routine)
# solution: use the --start-group and --end-group linker arguments to resolve circular dependency between libraries

# --whole-archive will correctly resolve weak symbols in external libraries
target_link_libraries(${TARGET_NAME} PUBLIC -Wl,--whole-archive -Wl,--start-group os core-stm32g431 -Wl,--end-group -Wl,--no-whole-archive )


#add_dependencies(${TARGET_NAME} zebOS-demoapp.elf)


set(LINKER_SCRIPT ${CMAKE_SOURCE_DIR}/STM32G431RBTX_FLASH.ld)
target_link_options(${TARGET_NAME} PUBLIC -Wl,-fatal-warnings,-Map=${CMAKE_SOURCE_DIR}/${TARGET_NAME}_${CMAKE_BUILD_TYPE}.map,-T ${LINKER_SCRIPT})

target_link_options(${TARGET_NAME} PUBLIC "-Wl,--print-memory-usage")

target_link_options(${TARGET_NAME} PUBLIC "-Wl,-v")
#target_link_options(${TARGET_NAME} PUBLIC "-Wl,-gc-sections")
target_link_options(${TARGET_NAME} PUBLIC "--specs=nano.specs")
target_link_options(${TARGET_NAME} PUBLIC "--specs=nosys.specs")
#target_link_options(${TARGET_NAME} PUBLIC "-Wl,--verbose")



#target_link_options(${TARGET_NAME} PUBLIC "-Wl,--gc-sections")
#target_link_options(${TARGET_NAME} PUBLIC "-Wl,--print-gc-sections")


target_link_options(${TARGET_NAME} PUBLIC "-Wl,--undefined=PendSV_Handler_asm")
target_link_options(${TARGET_NAME} PUBLIC "-Wl,--undefined=SVC_Handler")
target_link_options(${TARGET_NAME} PUBLIC "-flto")



set(HEX_FILE ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}.hex)
set(BIN_FILE ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}.bin)
add_custom_command(TARGET ${TARGET_NAME} POST_BUILD
        COMMAND ${CMAKE_OBJCOPY} -Oihex $<TARGET_FILE:${TARGET_NAME}> ${HEX_FILE}
        COMMAND ${CMAKE_OBJCOPY} -Obinary $<TARGET_FILE:${TARGET_NAME}> ${BIN_FILE}
        COMMENT "
Building ${HEX_FILE}
Building ${BIN_FILE}")

#add_custom_command(TARGET ${TARGET_NAME} POST_BUILD
#        COMMAND arm-none-eabi-objdump -DSC "${TARGET_NAME}" > "${CMAKE_SOURCE_DIR}/disassembly_map/${TARGET_NAME}_${CMAKE_BUILD_TYPE}.disassembly"
#        COMMENT "Disassembling ${TARGET_NAME} into ${CMAKE_SOURCE_DIR}/disassembly_map/${TARGET_NAME}_${CMAKE_BUILD_TYPE}.disassembly")

add_custom_target(Disassemble_${TARGET_NAME}
        COMMAND ${CMAKE_COMMAND} -E make_directory "${CMAKE_SOURCE_DIR}/disassembly_map"
        COMMAND arm-none-eabi-objdump -DSC "${TARGET_NAME}" > "${CMAKE_SOURCE_DIR}/disassembly_map/${TARGET_NAME}_${CMAKE_BUILD_TYPE}.disassembly"
        COMMENT "Disassembling ${TARGET_NAME} into ${CMAKE_SOURCE_DIR}/disassembly_map/${TARGET_NAME}_${CMAKE_BUILD_TYPE}.disassembly")

add_custom_target(UPLOAD openocd -f ${CMAKE_SOURCE_DIR}/stm32g431.cfg -c "program $<TARGET_FILE:${TARGET_NAME}> verify reset exit")

target_compile_options(
        ${TARGET_NAME} PUBLIC
        -w
)


add_custom_target(OS_and_demoapp)
add_dependencies(OS_and_demoapp ${TARGET_NAME})
add_dependencies(OS_and_demoapp zebOS-demoapp.elf)