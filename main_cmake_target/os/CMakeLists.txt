cmake_minimum_required(VERSION 3.17)

include(../../Common.cmake)

set(TARGET_NAME os)

project(${TARGET_NAME} CXX)

GET_SOURCES_IN_PROJECT(source_files)
CREATE("${source_files}")

target_include_directories(${TARGET_NAME} PUBLIC include)
target_include_directories(${TARGET_NAME} PUBLIC  ../system)
target_include_directories(${TARGET_NAME} PUBLIC  ../cpp_util/include)
target_include_directories(${TARGET_NAME} PUBLIC  ../gsl-lite)

set_target_properties(${TARGET_NAME} PROPERTIES LINKER_LANGUAGE CXX)

target_compile_options(
        ${TARGET_NAME} PUBLIC
        -w
)