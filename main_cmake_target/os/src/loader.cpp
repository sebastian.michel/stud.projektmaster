//
// Created by seb on 29.10.20.
//

#include <MemoryRegion.h>
#include <cstring>
#include <patch_memory_locations.h>
#include <process.h>

#include "address_pointer_helpers.h"
#include "application_header_struct.h"
#include "loader.h"
#include "memory_space.h"
#include "ProcessMemoryLayout.h"

namespace zebos{
#if EXTERN_GLOBAL
	Stack<uint32_t*, 8> data_addresses_process;
#endif

	inline void copyData(uint32_t* dataSourceStart, const uint32_t* dataSourceEnd, uint32_t* dataDestinationStart){
		memcpy(dataDestinationStart, dataSourceStart, numBytes(dataSourceStart, dataSourceEnd));
	}

	inline void nullMemory(uint32_t* dataStart, const uint32_t* dataEnd){
		memset(dataStart, 0, numBytes(dataStart, dataEnd));
	}

	static inline void patchAddressesFromRelDynSection(uint32_t patchOffset_b, const ApplicationHeaderType& applicationHeader){
		for(auto relocation_table_address = applicationHeader._reldyn_start; relocation_table_address != applicationHeader._reldyn_end; relocation_table_address+=2){
			auto relocation_address = reinterpret_cast<uint32_t*>(*relocation_table_address);
			bool relocation_address_is_in_ram = inRange(relocation_address, getAddressRamStart(), getAddressRamEnd());

			if ( relocation_address_is_in_ram ){
				auto relocation_address_in_process_datasegment = addressOffset(relocation_address, patchOffset_b);
				patchMemoryLocation(relocation_address_in_process_datasegment, patchOffset_b, addressToValue(getAddressRamStart()), addressToValue(getAddressRamEnd()));
			}
		}
	}

	pid_t load_application(uint32_t* process_ramStart, const ApplicationHeaderType& applicationHeader, uint32_t processNumber) {
		auto process_ramEnd = addressOffset(process_ramStart, getProcessRamSizeBytes()); // end address of the whole available ram

		auto adr_process_rwdata_start 	= process_ramStart;
		auto adr_process_gotStart 		= process_ramStart;

        MemoryRegion mem_preinitialized_rwdata(
				process_ramStart,
				applicationHeader._end_preinitialized_rwdata_rom - applicationHeader._start_preinitialized_rwdata_rom);

		// for this to work you have to make sure there is no section between the data section and the mem_bss section
		// normally the .got.plt section would be placed after the data section
		auto offset_bss_from_data = applicationHeader._start_bss_ram - (applicationHeader._linkage_adr_ram + mem_preinitialized_rwdata.size());

		//with the recent changes, this offset should always be 0
		zebos::zassert(offset_bss_from_data == 0);

        MemoryRegion mem_bss(
				mem_preinitialized_rwdata.end(),
				applicationHeader._end_bss_ram - applicationHeader._start_bss_ram);

		auto adr_process_rwdata_end = mem_bss.end();

		//copy all preinitialized rwdata from rom to ram
		// this includes the following sections:
		// - .got
		// - .got.plt
		// - .data
		copyData(applicationHeader._start_preinitialized_rwdata_rom, applicationHeader._end_preinitialized_rwdata_rom, mem_preinitialized_rwdata.start());

		//null mem_bss section
		nullMemory(mem_bss.start(), mem_bss.end());

        MemoryRegion mem_stack(
                mem_bss.end(),
                process_ramEnd - mem_bss.end()
                );

        // null stack section
        nullMemory(mem_stack.start(), mem_stack.end());

		//copy got from rom to ram
		//because got needs to be patched
		//copyData(applicationHeader.dataGotStart, applicationHeader.dataGotEnd, adr_process_gotStart);

		// the application expects all ram variables to start at applicationHeader._linkage_adr_ram
		// the actual start of ram data is at process_ramStart
		auto patch_offset = numBytes(applicationHeader._linkage_adr_ram, process_ramStart);

		// apply an offset to each address in the got which points to ram
		// this has to be done because all the data and mem_bss sections (all ram sections) are moved by the start_application routine
		patchAddressesFromRelDynSection(patch_offset, applicationHeader);

        processMemoryInfo[processNumber] = {.rwdata=mem_preinitialized_rwdata,.bss=mem_bss,.stack=mem_stack};

		//start a process for the application
		return create_process(process_ramEnd, applicationHeader.fun_entry,
							  adr_process_gotStart, adr_process_rwdata_start, adr_process_rwdata_end);
	}



	void init_data_addresses() {
		auto maxNumProcesses = getMaxNumProcesses();
		const auto addressProcessRamStart = getAddressProcessRamStart();
		for(auto i = maxNumProcesses; i > 0; i-- ){
		    auto adr = addressOffset(addressProcessRamStart, (i-1) * getProcessRamSizeBytesPow2Aligned());
			data_addresses_process.push(reinterpret_cast<uint32_t*>(adr));
		}
	}
}
