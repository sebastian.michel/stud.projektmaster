//
// Created by seb on 23.11.21.
//

#include "datasegment_relative_address.h"

uint32_t zebos::getDatasegmentRelativeAdress(zebos::pid_t pid, uint32_t absolute_address) {
	return absolute_address - reinterpret_cast<uint32_t>(processTable[pid.val].data_start);
}

uint32_t zebos::getReverseDatasegmentRelativeAddress(zebos::pid_t pid, uint32_t datasegment_relative_address) {
	return datasegment_relative_address + reinterpret_cast<uint32_t>(processTable[pid.val].data_start);
}

zebos::datasegment_relative_address::datasegment_relative_address(zebos::pid_t pid, uint32_t value)
	: dra_value(getDatasegmentRelativeAdress(pid,value)) { }

uint32_t zebos::datasegment_relative_address::getInverseDRA(zebos::pid_t pid) const {
	return getReverseDatasegmentRelativeAddress(pid,dra_value);
}

bool zebos::datasegment_relative_address::operator<(const zebos::datasegment_relative_address& other) const {
	return this->dra_value < other.dra_value;
}
