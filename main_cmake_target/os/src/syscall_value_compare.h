//
// Created by seb on 20.04.21.
//

#ifndef ZEBOS_SYSCALL_VALUE_COMPARE_H
#define ZEBOS_SYSCALL_VALUE_COMPARE_H


#include <cstdint>
#include "syscall_impl.h"

namespace zebos{
	void syscall_valueCompare(ProcessStack* stack, uint8_t * address, uint32_t numBytes);
}

#endif //ZEBOS_SYSCALL_VALUE_COMPARE_H
