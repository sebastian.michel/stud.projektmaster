//
// Created by seb on 08.12.21.
//

#include "is_power_of_two.h"

bool is_power_of_two(uint32_t n) {
    return (n!=0) && (n & (n - 1)) == 0;
}