//
// Created by seb on 27.11.20.
//

#include <math_util.h>

uint32_t log2Base(uint32_t v) {
    //from http://graphics.stanford.edu/~seander/bithacks.html#IntegerLogDeBruijn
    static const int MultiplyDeBruijnBitPosition[32] =
            {
                    0, 9, 1, 10, 13, 21, 2, 29, 11, 14, 16, 18, 22, 25, 3, 30,
                    8, 12, 20, 28, 15, 17, 24, 7, 19, 27, 23, 6, 26, 5, 4, 31
            };

    v |= v >> 1u; // first round down to one less than a power of 2
    v |= v >> 2u;
    v |= v >> 4u;
    v |= v >> 8u;
    v |= v >> 16u;
    return MultiplyDeBruijnBitPosition[(uint32_t)(v * 0x07C4ACDDU) >> 27];
}

uint32_t nextPowOfTwo(uint32_t v, uint32_t offset) {
    return 1ul << log2Base(v-1) + 1 + offset;
}

uint32_t nextPowOfTwo(uint32_t v) {
    nextPowOfTwo(v,0);
}



