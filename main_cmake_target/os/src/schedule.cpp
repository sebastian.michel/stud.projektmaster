//
// Created by seb on 09.10.20.
//

#include <process_defines.h>
#include "schedule.h"
using namespace zebos;

void schedule() {

    static uint32_t i = 0;
    if (i == num_processes) {
        i = 0;
    }

    // process task
    if (currentPcbPtr) {
        (*currentPcbPtr).state = READY;
        // set last run to the actual systick counter
        //todo implement counter
        //(*currentPcbPtr).systick_last_run = millisecond_counter;
        (*currentPcbPtr).systick_last_run = 0;
    }
    currentPcbPtr = nullptr;

    // iterate over tasks
    for (; i < num_processes; i++) {
        // calc timediff between last run and actual counter
        //todo implement counter
        //uint32_t diff = millisecond_counter - processTable[i].systick_last_run;
        uint32_t diff = 0;
        // get the interval the task
        uint32_t interval = processTable[i].interval;

        // check if the task is ready to run
        if (diff >= interval && processTable[i].state == READY) {
            processTable[i].state = RUNNING;

            //save stackpointer of operating system

            //andere funktion, welche den entry vom aktuellen task aufruft
            //in der anderen funktion steht im lr regsitster die r�cksprungadresse, welche wir brauchen, wenn wir aus dem task wieder rauswechseln
            currentPcbPtr = &processTable[i];
            i++;
            break;
        }
    }

}