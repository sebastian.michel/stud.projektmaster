//
// Created by seb on 06.12.21.
//

#include "../core-stm32g431/include/timer.h"
#include "check_test_status.h"
#include "debug.h"
#include "global_variables.h"
#include <reset.h>
#include "handler_HardFault.h"
#include "three_way_memory_comparison.h"

namespace zebos{
	void log_test_time(){
		static bool test_time_logged = false;
		if(!test_time_logged){
			timer3_stop();

			test_time_logged = true;

			auto val = timer3_getValue();
			std::array<char,20> buffer;
			auto numb = snprintf(std::begin(buffer),20,"%d,",val);
			zebos::log::msg({buffer.begin(), static_cast<size_t>(numb)});

		}
	}

	void log_test_detection_type(){
		zebos::log::msg(zebos::test_status.getDetectionTypeString());
		zebos::log::msg(',');
	}

	void log_hardfault_status(){
		std::array<char,20> buffer;
		auto numb = snprintf(std::begin(buffer),20,"0x%x",zebos::test_status.hardFault_status);
		zebos::log::msg({buffer.begin(), static_cast<size_t>(numb)});
		zebos::log::msg(',');
	}
}


void check_test_status(){
	using zebos::three_way_memory_comparison;

	if (currentPcbPtr->process_number == 2){
        if(zebos::test_done_counter >=2){
            zebos::log_test_time();
            bool result_is_correct = three_way_memory_comparison(zebos::memory_comparison_range.adresses,zebos::memory_comparison_range.numWords);

            if(zebos::test_status.errorWasCorrected()){
                if(result_is_correct){
                    zebos::on_test_finished(zebos::TestResult::ErrorDetected_Corrected{});
                } else {
                    zebos::on_test_finished(zebos::TestResult::ErrorDetected_Corrected_ErrorInResult{});
                }
            } else if(zebos::test_status.errorWasDetected()){
                zebos::on_test_finished(zebos::TestResult::ErrorDetected_But_Not_Corrected{});
            } else {
                if(result_is_correct){
                    zebos::on_test_finished(zebos::TestResult::NoError{});
                } else{
                    zebos::on_test_finished(zebos::TestResult::ErrorUndetected{});
                }
            }

            while(1);
        }

        zebos::test_done_counter = 0;
    } else {
        //"watchdog"
		if constexpr(testConfig.enable_watchdog){
			if(timer3_getValue() > 5000) {
				zebos::on_test_finished(zebos::TestResult::DeadlockSituation_Watchdog{});
				while (1);
			}
		}
    }
}