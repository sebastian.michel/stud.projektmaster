//
// Created by seb on 06.04.21.
//

#include "ErrorGenerator.h"
#include "global_variables.h"
#include <RandomGenerator.h>
#include <bit_util.h>
#include <cstdint>
#include <debug.h>
#include <is_power_of_two.h>
#include <logging_config.h>
#include <random.h>
#include <random>

using namespace zebos;

extern "C" {

void on_error_generation(uint32_t* psp) {
    error_generator.on_error_generation(psp);
}

void check_error_generation() {
	if constexpr( not testConfig.disable_error_generation_from_interrupt){
		if(zebos::error_generation_scheduled){
			auto psp = currentPcbPtr->stack;
			zebos::error_generation_scheduled = false;
			error_generator.on_error_generation(psp);
		}
	}
}

}



namespace zebos{


	void ErrorGenerator::on_error_generation(uint32_t* psp) {
        if constexpr (testConfig.break_on_error_generation){
            software_breakpoint();
        }

		switch (this->config.operationMode){
			case error_generator_config::OperationMode::Disabled: return;
			case error_generator_config::OperationMode::Register: {
				generate_error_register(psp);} break;
			case error_generator_config::OperationMode::AddressRange: {
				generate_error_address_range();} break;
		}
	}

	void ErrorGenerator::config_set_error_registers(error_generator_config::ErrorRegister reg) {
		this->config.error_registers[static_cast<uint32_t>(reg)] = true;
	}

	void ErrorGenerator::config_operation_mode(error_generator_config::OperationMode mode) {
		this->config.operationMode = mode;
	}

    void ErrorGenerator::config_enable_all_registers(){
        std::for_each(std::begin(config.error_registers), std::end(config.error_registers), [](auto& reg){reg = true;});
        config.error_registers[static_cast<uint32_t>(error_generator_config::ErrorRegister::SP)] = false;
    }

	void ErrorGenerator::config_set_address_range(uint32_t* start, uint32_t numBytes) {
		this->config.error_address_range = error_generator_config::Range{start,numBytes};
	}

	uint8_t* ErrorGenerator::chose_error_address(){
		auto dice_roll = rng->generate(config.error_address_range.get_num_bytes()-1);

		return reinterpret_cast<uint8_t*>(config.error_address_range.startAddress) + dice_roll;
	}

	uint8_t ErrorGenerator::get_error_bitpos(uint8_t max_bitpos){
		if(config.bitposMode == error_generator_config::BitposMode::Random){
			return get_random_bit_pos(max_bitpos);
		} else {
			return config.bitpos;
		}
	}

	void ErrorGenerator::generate_error_address_range() {
		if(!config.error_address_range.isValid() ) { return; }

		auto error_adr = chose_error_address();
		auto error_bitpos = get_error_bitpos(7);



        zebos::log::msg(error_adr);
        zebos::log::msg(',');

		generate_error(error_adr, error_bitpos);
	}

	static int get_register_stack_offset(error_generator_config::ErrorRegister reg){
		// psp - 0 = r4
		// psp - 1 = r5
		// psp - 2 = r6
		// ..
		// psp - 7 = r11
		// psp - 8 = r0
		// psp - 9 = r1
		// psp - 10 = r2
		// psp - 11 = r3
		// psp - 12 = r12
		// psp - 13 = lr
		// psp - 14 = pc
		// psp - 15 = xpsr

		switch (reg) {
			case error_generator_config::ErrorRegister::R4  : return 0 ;
			case error_generator_config::ErrorRegister::R5  : return 1 ;
			case error_generator_config::ErrorRegister::R6  : return 2 ;
			case error_generator_config::ErrorRegister::R7  : return 3 ;
			case error_generator_config::ErrorRegister::R8  : return 4 ;
			case error_generator_config::ErrorRegister::R9  : return 5 ;
			case error_generator_config::ErrorRegister::R10 : return 6 ;
			case error_generator_config::ErrorRegister::R11 : return 7 ;

			case error_generator_config::ErrorRegister::S16 : return 8  ;
			case error_generator_config::ErrorRegister::S17 : return 9  ;
			case error_generator_config::ErrorRegister::S18 : return 10 ;
			case error_generator_config::ErrorRegister::S19 : return 11 ;
			case error_generator_config::ErrorRegister::S20 : return 12 ;
			case error_generator_config::ErrorRegister::S21 : return 13 ;
			case error_generator_config::ErrorRegister::S22 : return 14 ;
			case error_generator_config::ErrorRegister::S23 : return 15 ;
			case error_generator_config::ErrorRegister::S24 : return 16 ;
			case error_generator_config::ErrorRegister::S25 : return 17 ;
			case error_generator_config::ErrorRegister::S26 : return 18 ;
			case error_generator_config::ErrorRegister::S27 : return 19 ;
			case error_generator_config::ErrorRegister::S28 : return 20 ;
			case error_generator_config::ErrorRegister::S29 : return 21 ;
			case error_generator_config::ErrorRegister::S30 : return 22 ;
			case error_generator_config::ErrorRegister::S31 : return 23 ;

			case error_generator_config::ErrorRegister::R0  : return 8 +16;
			case error_generator_config::ErrorRegister::R1  : return 9 +16;
			case error_generator_config::ErrorRegister::R2  : return 10+16;
			case error_generator_config::ErrorRegister::R3  : return 11+16;
			case error_generator_config::ErrorRegister::R12 : return 12+16;
			case error_generator_config::ErrorRegister::LR  : return 13+16;
			case error_generator_config::ErrorRegister::PC  : return 14+16;
			case error_generator_config::ErrorRegister::xPSR: return 15+16;

            case error_generator_config::ErrorRegister::S0 : return 16+16;
            case error_generator_config::ErrorRegister::S1 : return 17+16;
            case error_generator_config::ErrorRegister::S2 : return 18+16;
            case error_generator_config::ErrorRegister::S3 : return 19+16;
            case error_generator_config::ErrorRegister::S4 : return 20+16;
            case error_generator_config::ErrorRegister::S5 : return 21+16;
            case error_generator_config::ErrorRegister::S6 : return 22+16;
            case error_generator_config::ErrorRegister::S7 : return 23+16;
            case error_generator_config::ErrorRegister::S8 : return 24+16;
            case error_generator_config::ErrorRegister::S9 : return 25+16;
            case error_generator_config::ErrorRegister::S10: return 26+16;
            case error_generator_config::ErrorRegister::S11: return 27+16;
            case error_generator_config::ErrorRegister::S12: return 28+16;
            case error_generator_config::ErrorRegister::S13: return 29+16;
            case error_generator_config::ErrorRegister::S14: return 30+16;
            case error_generator_config::ErrorRegister::S15: return 31+16;
            case error_generator_config::ErrorRegister::FPSCR : return 32+16;

			default:
				zassert(false);
				while(1);
		}
	}

	error_generator_config::ErrorRegister ErrorGenerator::chose_error_register(){
		if (std::count_if(begin(config.error_registers), end(config.error_registers), [](auto reg){return reg == true;}) == 1){
            auto element_number = std::distance(begin(config.error_registers), std::find(begin(config.error_registers), end(config.error_registers),true));
			auto error_reg = static_cast<error_generator_config::ErrorRegister>( element_number);
			return error_reg;
		}

		uint32_t dice_roll;
		do {
			dice_roll = rng->generate(config.error_registers.size());
		} while (not config.error_registers[dice_roll]);

		return static_cast<error_generator_config::ErrorRegister>(dice_roll);
	}

	uint8_t ErrorGenerator::get_random_bit_pos(uint8_t maxPos) {
		return rng->generate(maxPos);
	}

	void ErrorGenerator::generate_error(uint8_t* address, uint8_t bitpos){
		(*address) ^= 1 << bitpos;
        if constexpr (testConfig.break_after_error_generation){
            software_breakpoint();
        }
	}

	void ErrorGenerator::generate_error(uint32_t* address, uint8_t bitpos){
		auto before = *address;
        if constexpr (not testConfig.disable_error_generation){
            (*address) = (*address) ^ (1 << bitpos);
        }
        auto after = *address;

        if constexpr (testConfig.break_after_error_generation){
            software_breakpoint();
        }

        if constexpr (testConfig.log_error_extended){
            std::array<char,100> buffer;
            auto numchars = snprintf ( begin(buffer), 100, ", / %#x -> %#x", before, after);
            zebos::log::msgnl({begin(buffer),static_cast<size_t>(numchars)});
        }
	}

	static const char* get_reg_name(error_generator_config::ErrorRegister reg){
        switch (reg)
        {
            case error_generator_config::ErrorRegister::R0:    return "R0";
            case error_generator_config::ErrorRegister::R1:    return "R1";
            case error_generator_config::ErrorRegister::R2:    return "R2";
            case error_generator_config::ErrorRegister::R3:    return "R3";
            case error_generator_config::ErrorRegister::R4:    return "R4";
            case error_generator_config::ErrorRegister::R5:    return "R5";
            case error_generator_config::ErrorRegister::R6:    return "R6";
            case error_generator_config::ErrorRegister::R7:    return "R7";
            case error_generator_config::ErrorRegister::R8:    return "R8";
            case error_generator_config::ErrorRegister::R9:    return "R9";
            case error_generator_config::ErrorRegister::R10:   return "R10";
            case error_generator_config::ErrorRegister::R11:   return "R11";

			case error_generator_config::ErrorRegister::S16 : return "S16" ;
			case error_generator_config::ErrorRegister::S17 : return "S17" ;
			case error_generator_config::ErrorRegister::S18 : return "S18" ;
			case error_generator_config::ErrorRegister::S19 : return "S19" ;
			case error_generator_config::ErrorRegister::S20 : return "S20" ;
			case error_generator_config::ErrorRegister::S21 : return "S21" ;
			case error_generator_config::ErrorRegister::S22 : return "S22" ;
			case error_generator_config::ErrorRegister::S23 : return "S23" ;
			case error_generator_config::ErrorRegister::S24 : return "S24" ;
			case error_generator_config::ErrorRegister::S25 : return "S25" ;
			case error_generator_config::ErrorRegister::S26 : return "S26" ;
			case error_generator_config::ErrorRegister::S27 : return "S27" ;
			case error_generator_config::ErrorRegister::S28 : return "S28" ;
			case error_generator_config::ErrorRegister::S29 : return "S29" ;
			case error_generator_config::ErrorRegister::S30 : return "S30" ;
			case error_generator_config::ErrorRegister::S31 : return "S31" ;

            case error_generator_config::ErrorRegister::R12:   return "R12";
            case error_generator_config::ErrorRegister::LR:    return "LR";
            case error_generator_config::ErrorRegister::PC:    return "PC";
            case error_generator_config::ErrorRegister::SP:    return "SP";
            case error_generator_config::ErrorRegister::xPSR:  return "xPSR";

            case error_generator_config::ErrorRegister::S0:     return "S0";
            case error_generator_config::ErrorRegister::S1:     return "S1";
            case error_generator_config::ErrorRegister::S2:     return "S2";
            case error_generator_config::ErrorRegister::S3:     return "S3";
            case error_generator_config::ErrorRegister::S4:     return "S4";
            case error_generator_config::ErrorRegister::S5:     return "S5";
            case error_generator_config::ErrorRegister::S6:     return "S6";
            case error_generator_config::ErrorRegister::S7:     return "S7";
            case error_generator_config::ErrorRegister::S8:     return "S8";
            case error_generator_config::ErrorRegister::S9:     return "S9";
            case error_generator_config::ErrorRegister::S10:    return "S10";
            case error_generator_config::ErrorRegister::S11:    return "S11";
            case error_generator_config::ErrorRegister::S12:    return "S12";
            case error_generator_config::ErrorRegister::S13:    return "S13";
            case error_generator_config::ErrorRegister::S14:    return "S14";
            case error_generator_config::ErrorRegister::S15:    return "S15";
            case error_generator_config::ErrorRegister::FPSCR:  return "FPSCR";


            default: return "?";
        }
    }

	void ErrorGenerator::generate_error_register(uint32_t* psp) {
        bool no_register_selected = std::none_of(std::begin(config.error_registers), end(config.error_registers), [](auto elem) {return elem == true;});
		if(no_register_selected) { return; }

		auto chosen_reg = chose_error_register();

		if(chosen_reg != error_generator_config::ErrorRegister::SP) {
			auto reg_stack_offset = get_register_stack_offset(chosen_reg);

			auto error_adr = psp + reg_stack_offset;
			auto error_bitpos = get_error_bitpos(31);

            if constexpr (testConfig.log_error_extended){
                zebos::log::msg("error reg:");
                zebos::log::msg(get_reg_name(chosen_reg));
            } else {
				zebos::log::msg(get_reg_name(chosen_reg));
				zebos::log::msg(',');
			}

			//bitflip in the register which is safed on psp
			generate_error(error_adr, error_bitpos);
		} else {
			// this is not enabled yet as a modification of the sp requires some magic.
			// because the sp is modified on exception return, the interrupt handler which calls the error-generating code
			// is not able to modify the sp. (if it would, exception return would likely fail)
            zassert(false);
		}
	}

	void ErrorGenerator::set_config(const error_generator_config::Config& config) {
		this->config = config;
	}
}
