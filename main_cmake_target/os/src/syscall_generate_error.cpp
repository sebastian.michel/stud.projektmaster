//
// Created by seb on 10.05.21.
//

#include "syscall_generate_error.h"
#include "../../../CMSIS/Core/Include/cmsis_compiler.h"
#include "../../core-stm32g431/include/systick.h"
#include "ErrorGenerator.h"
#include "global_variables.h"

namespace zebos {
    constexpr bool oneshot_mode = true;
    static bool oneshot_fired = false;

    void syscall_generateError(uint32_t* address, uint32_t numBytes) {
        if(oneshot_mode && !oneshot_fired)
        {
            oneshot_fired = true;

            error_generator.config_operation_mode(error_generator_config::OperationMode::AddressRange);
            error_generator.config_set_address_range(address, numBytes);
            error_generator.generate_error_address_range();
        }

        //perform a systick reset, so all processes still have the same systick counter even though one process had more work
        __ISB();
        __DSB();
        systick_reset();
    }
}
