//
// Created by seb on 07.05.21.
//

#include "error_detection.h"
#include "TestResult.h"
#include "datasegment_relative_address.h"
#include "debug.h"
#include "global_variables.h"
#include "process_data_storage.h"
#include "syscalls.h"
#include "zfunctional.h"
#include "../../../CMSIS/Core/Include/cmsis_compiler.h"
#include <ProcessStack.h>
#include <handler_HardFault.h>
#include <error_correction.h>
#include <error_detection_stack.h>
#include <software_breakpoint.h>
#include <yield.h>

namespace zebos {
    constexpr bool process_data_storage_comparison_required_always = true;

    bool process_data_storage_comparison_required = false;

	/*
	 * Fehlererkennung über MPU Handler (Invalider Speicherzugriff)
	 * - vormerken welcher Prozess den Fehler verursacht hat
	 * - wenn prozess = letzter prozess der gruppe: einleiten der fehlerkorrektur
	 * 										 sonst: scheduling, solange bis letzter prozess ausgeführt wurde, dann fehlerbehebung
	 *
	 */



    void on_error_occured(pid_t pid) {
        auto memberId = processGroup->pid_to_member_id(pid);
		processgroup_member_with_error = memberId;

        if constexpr (testConfig.break_if_error_occured){
            software_breakpoint();
        }

    }

    // All processes returned an error
    bool error_occured_all_procs() {
        zebos::zassert(false);
    }

	template<typename T>
	struct lazy_three_way_comparison{
		lazy_three_way_comparison(const T& elem0, const T& elem1, const T& elem2){
			all_unequal = false;

			auto equ_0_1 = elem0 == elem1;
			auto equ_1_2 = elem1 == elem2;

			all_equal = equ_0_1 && equ_1_2;
			if (all_equal) return;
			else{
				if(equ_0_1){
					unequal_index = 2;
					return;
				} else if (equ_1_2){
					unequal_index = 0;
					return;
				} else {
					auto equ_0_2 = elem0 == elem2;
					if(equ_0_2) {
						unequal_index = 1;
						return;
					} else {
						all_unequal = true;
						return;
					}
				}
			}
		}

		bool all_equal;
		bool all_unequal;
		uint8_t unequal_index;
	};

    bool errors_in_processdatastorage() {
		using std::array, std::reference_wrapper;

        zebos::zassert(processGroup->size() == 3);

		const array<reference_wrapper<ProcessDataStorage>, 3> storage {
				processGroup->get_data_storage().at(0).get(),
				processGroup->get_data_storage().at(1).get(),
				processGroup->get_data_storage().at(2).get()
		};
		array<Failable<const ProcessDataStorage::Element*>, 3> elements_or_error {
				storage[0].get().get_first(),
				storage[1].get().get_first(),
				storage[2].get().get_first()
		};

		const array datasegments_start{
				storage[0].get().get_data_start_adr(),
				storage[1].get().get_data_start_adr(),
				storage[2].get().get_data_start_adr(),
		};

		std::array<const ProcessDataStorage::CachedElement, 3> cached_elements{
			storage[0].get().get_cached_element(),
			storage[1].get().get_cached_element(),
			storage[2].get().get_cached_element()
		};

		lazy_three_way_comparison cached_element_comparison(
			ProcessDataStorage::ElementComparator<ProcessDataStorage::CachedElement>{storage[0].get().get_cached_element(), datasegments_start[0]},
			ProcessDataStorage::ElementComparator<ProcessDataStorage::CachedElement>{storage[1].get().get_cached_element(), datasegments_start[1]},
			ProcessDataStorage::ElementComparator<ProcessDataStorage::CachedElement>{storage[2].get().get_cached_element(), datasegments_start[2]}
		);

		if(cached_element_comparison.all_equal){
			//do nothing
		}
		else if(cached_element_comparison.all_unequal){
			error_occured_all_procs();

			return true;
		} else {
			on_error_occured(processGroup->member_id_to_pid(cached_element_comparison.unequal_index));

			return true;
		}

        while (true)
        {
			array<bool,3> storage_empty{
					elements_or_error[0].has_error_occured(),
					elements_or_error[1].has_error_occured(),
					elements_or_error[2].has_error_occured(),
			};

			lazy_three_way_comparison storages_empty_comparison(storage_empty[0], storage_empty[1], storage_empty[2]);
			auto all_empty = storages_empty_comparison.all_equal && storage_empty[0];
			auto all_not_empty = storages_empty_comparison.all_equal && ( ! storage_empty[0] );

			if( all_empty ){
				//no elements_or_error left in any storage anymore

				// we are done, no errors occured

				return false;
			} else if ( ! all_not_empty){
				on_error_occured(processGroup->member_id_to_pid(storages_empty_comparison.unequal_index));

				return true;
			}
			else {
				// all have at least 1 element left

				const array elements{
						ProcessDataStorage::ElementComparator<ProcessDataStorage::Element>{*elements_or_error[0].get(), datasegments_start[0]},
						ProcessDataStorage::ElementComparator<ProcessDataStorage::Element>{*elements_or_error[1].get(), datasegments_start[1]},
						ProcessDataStorage::ElementComparator<ProcessDataStorage::Element>{*elements_or_error[2].get(), datasegments_start[2]},
				};

				lazy_three_way_comparison comparison(elements[0],elements[1],elements[2]);

				if(comparison.all_equal){
					//all elements are equal
					// proceed to next element
					elements_or_error = {
							storage[0].get().get_next(&elements[0].elem),
							storage[1].get().get_next(&elements[1].elem),
							storage[2].get().get_next(&elements[2].elem)
					};

					continue;
				}
				else if(comparison.all_unequal) {
					// all elements_or_error are not equal to one another
					// this should rarely happen

					error_occured_all_procs();

					return true;
				} else {
					on_error_occured(processGroup->member_id_to_pid(comparison.unequal_index));

					return true;
				}
			}
        }
    }

    void set_flag_process_data_storage_comparison_required() {
		process_data_storage_comparison_required = true;
    }

    void reset_flag_process_data_storage_comparison_required() {
		process_data_storage_comparison_required = false;
    }

    bool stackpointers_are_equal(){
	    auto pids = std::array{
	        processGroup->member_id_to_pid(0).get(),
	        processGroup->member_id_to_pid(1).get(),
	        processGroup->member_id_to_pid(2).get() };

	    auto stackpointers = std::array{
	        datasegment_relative_address(pids[0], reinterpret_cast<uint32_t>(processTable[0].stack)),
	        datasegment_relative_address(pids[1], reinterpret_cast<uint32_t>(processTable[1].stack)),
	        datasegment_relative_address(pids[2], reinterpret_cast<uint32_t>(processTable[2].stack)) };

        return zfunctional::all_equal(stackpointers);
    }

    bool programm_counters_are_equal(){
	    auto pids = std::array{
	        processGroup->member_id_to_pid(0).get(),
	        processGroup->member_id_to_pid(1).get(),
	        processGroup->member_id_to_pid(2).get() };

	    auto stacks = std::array{
	        reinterpret_cast<ProcessStack*>(processTable[0].stack),
	        reinterpret_cast<ProcessStack*>(processTable[1].stack),
	        reinterpret_cast<ProcessStack*>(processTable[2].stack) };

        auto pcs = std::array{
            stacks[0]->pc,
			stacks[1]->pc,
			stacks[2]->pc };

		volatile auto pcs_debug = pcs;

        volatile auto all_equal __attribute__((used)) = zfunctional::all_equal( pcs );
		__NOP();
        return all_equal;
    }

}// namespace zebos

extern "C"{
    void on_invalid_memory_access_error(zebos::pid_t pid){
        on_error_occured(pid);
    }

    void error_detection() {
        using namespace zebos;

		constexpr bool force_error_correction = false;

		bool last_process_of_group_executed = currentPcbPtr->process_number == processGroup->member_id_to_pid(2).get();
		// execute this after all processes have completed to run
		// checking for pid of member 0 because the call to check_error_detection_required() happens after scheduling. this means that all processes have finished running

		if (last_process_of_group_executed){
			if (processgroup_member_with_error == -1 && !force_error_correction){ // no error detected yet,
			    if( not	programm_counters_are_equal() ){
                    test_status.on_error_detected(zebos::handler_HardFault::DetectionType::Invalid_PC);

			        run_error_correction();

			    } else if (not stackpointers_are_equal() ){
                    test_status.on_error_detected(zebos::handler_HardFault::DetectionType::Invalid_SP);

			        run_error_correction();

			    } else if ((process_data_storage_comparison_required_always || process_data_storage_comparison_required) && errors_in_processdatastorage()){
                    reset_flag_process_data_storage_comparison_required();
                    test_status.on_error_detected(zebos::handler_HardFault::DetectionType::SafeData);
					//software_breakpoint();
			        run_error_correction();
                }
			} else { // error already detected, run correction
                test_status.on_error_detected(zebos::handler_HardFault::DetectionType::MemmanageHandler);

                run_error_correction();
			}

			processGroup->resetStorages();
			processgroup_member_with_error = -1;
		}
    }
}

