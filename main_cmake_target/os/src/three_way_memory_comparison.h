//
// Created by seb on 16.12.21.
//

#ifndef ZEBOS_THREE_WAY_COMPARISON_H
#define ZEBOS_THREE_WAY_COMPARISON_H

#include <cstdint>
#include <array>
#include <logging_config.h>

namespace zebos{
	DONT_OPTIMIZE_IF_DEBUG bool three_way_memory_comparison(std::array<uint32_t*,3> adresses, uint32_t numWords);
}



#endif //ZEBOS_THREE_WAY_COMPARISON_H
