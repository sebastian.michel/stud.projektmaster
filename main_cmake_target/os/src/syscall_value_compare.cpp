//
// Created by seb on 20.04.21.
//

#include "syscall_value_compare.h"
#include "error_detection.h"
#include "global_variables.h"
#include <process_group.h>
#include <array>
#include <process_defines.h>


namespace zebos {
    void syscall_valueCompare(ProcessStack* stack, uint8_t* address, uint32_t numBytes) {
        auto pid = currentPcbPtr->process_number;
        auto lr = stack->lr;

        processGroup->put(pid, lr, address, numBytes);
    }
}// namespace zebos