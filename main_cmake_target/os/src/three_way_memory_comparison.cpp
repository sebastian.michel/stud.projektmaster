//
// Created by seb on 16.12.21.
//

#include "three_way_memory_comparison.h"
#include <cstdint>
#include <span>


namespace zebos{
	/*
	 * returns false if error was detected
	 */
	bool three_way_memory_comparison(std::array<uint32_t*, 3> adresses, uint32_t numWords) {
		const auto memoryRegions = std::array{
				std::span<uint32_t>{adresses[0], numWords},
				std::span<uint32_t>{adresses[1], numWords},
				std::span<uint32_t>{adresses[2], numWords}
		};

		for( auto adr0 = begin(memoryRegions[0]), adr1 = begin(memoryRegions[1]), adr2 = begin(memoryRegions[2]);
			 adr0 != end(memoryRegions[0]);
			 adr0++,adr1++,adr2++ ){

			std::array<uint32_t, 3> words{*adr0,*adr1,*adr2};

			if (words[0] == words[1] && words[1] == words[2]){
				continue;
			} else {
				return false;
			}
		}

		return true;
	}
}
