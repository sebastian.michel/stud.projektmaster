//
// Created by seb on 04.11.21.
//

#include "error_detection_stack.h"
#include "process_defines.h"
#include "software_breakpoint.h"
#include "error_correction.h"
#include "datasegment_relative_address.h"

template< class InputIt1, class InputIt2, class Predicate >
InputIt1 find_unequal(InputIt1 iter1, const InputIt1 iter1_end,
					  InputIt2 iter2,
					  InputIt2 iter3,
					  Predicate p) {
	for (; iter1 != iter1_end; ++iter1, ++iter2, ++iter3) {
		if (!(p(*iter1, *iter2, *iter3))) {
			return iter1;
		}
	}
	return iter1_end;
}

#include <ranges>
#include <zassert.h>


namespace zebos {
    template <typename T>
    bool all_equal(const std::array<T,3>& values){
        return values[0] == values[1] && values[1] == values[2];
    }

	template <typename T>
	bool any_of(const std::array<T,3>& values, uint32_t value){
		return
		values[0] == value ||
		values[1] == value ||
		values[2] == value;
	}

    void error_correction_stack(const std::array<zebos::pid_t,3>& pids ) {
	    const auto pcb = std::array{
                processTable[pids[0]],
                processTable[pids[1]],
                processTable[pids[2]] };

        const auto stacks = std::array{
            std::span<uint32_t>{ pcb[0].stack, pcb[0].stack_end },
            std::span<uint32_t>{ pcb[1].stack, pcb[1].stack_end },
            std::span<uint32_t>{ pcb[2].stack, pcb[2].stack_end } };

		if (stacks[0].size() != stacks[1].size() || stacks[1].size() != stacks[2].size()) {
			zassert(false);
			zebos::software_breakpoint();
			while (1);//todo this should not happen
		}

        for (auto iter0 = std::begin(stacks[0]), iter1 = std::begin(stacks[1]), iter2 = std::begin(stacks[2]); iter0 != std::end(stacks[0]); iter0++, iter1++, iter2++){
            auto words = std::array{*iter0,*iter1,*iter2};
            if(all_equal(words)){
                continue;
            } 
            else if(all_equal( std::array{
                datasegment_relative_address(pids[0],*iter0),
                datasegment_relative_address(pids[1],*iter1),
                datasegment_relative_address(pids[2],*iter2), }))
            {
                continue;
            } 
            else 
            {
                correct_error_unkown_if_dra(pids, std::array{ iter0.base() , iter1.base(), iter2.base() } );
            }
        }
	}

}// namespace zebos
