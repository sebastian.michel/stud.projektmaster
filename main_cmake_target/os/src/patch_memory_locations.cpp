//
// Created by seb on 03.11.20.
//


#include <address_pointer_helpers.h>

void patchMemoryLocation(uint32_t* address, uint32_t patchOffset, uint32_t patchRangeStart, uint32_t patchRangeEnd) {
    auto valueAtAddress = *address;
    if ( inRange(valueAtAddress, patchRangeStart, patchRangeEnd) ){
        *address += patchOffset;
    }
}

void
patchMemoryLocations(uint32_t* addressStart, const uint32_t* addressEnd, uint32_t patchOffset, uint32_t patchRangeStart,
                     uint32_t patchRangeEnd) {
    for(auto address = addressStart; address != addressEnd; address++){
        patchMemoryLocation(address, patchOffset, patchRangeStart, patchRangeEnd);
    }
}
