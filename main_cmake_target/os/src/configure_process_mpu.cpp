//
// Created by seb on 17.11.20.
//

#include "configure_process_mpu.h"
#include "../../core-stm32g431/include/asm_stack_reset.h"
#include "mpu.h"
#include <address_pointer_helpers.h>
#include <address_space.h>
#include <application_header_struct.h>
#include <process_defines.h>
#include <zassert.h>

extern uint32_t ram_function_exec_memory_op;

using namespace zebos;
/*
void configureProcessMpuRegion_ro(){
	//allow access to stack
	//mpu_set_region_rw(1, addressToValue(currentPcbPtr->mpu_protected_region_end), numBytes(currentPcbPtr->mpu_protected_region_end,currentPcbPtr->stack_end));

	mpu_set_region_rw(2, addressToValue(currentPcbPtr->mpu_protected_region_end), numBytes(currentPcbPtr->mpu_protected_region_end,currentPcbPtr->stack_end));
	mpu_set_region_ro(3, addressToValue(currentPcbPtr->data_start), numBytes(currentPcbPtr->data_start, currentPcbPtr->mpu_protected_region_end));
	//also allow rw access but disable it, so it can be enabled by mpu handler
	mpu_set_region_rw(4, addressToValue(currentPcbPtr->data_start), numBytes(currentPcbPtr->data_start, currentPcbPtr->mpu_protected_region_end));
	mpu_disable_region(4);
}

void configureProcessMpuRegion_rw(){
	mpu_set_region_rw(3, addressToValue(currentPcbPtr->data_start), numBytes(currentPcbPtr->data_start, currentPcbPtr->mpu_protected_region_end));
}
*/

void mpu_enable_4k_rw_at(uint32_t address) {
    auto address_is_4k_aligned = address % 0x1000 == 0;
    zebos::zassert(address_is_4k_aligned);

    // 1/8 von 8k = 1k
    // subregion disable muss sein: 1100 0000
    // das heißt: deaktiviere die oberen 2k

    mpu_set_region_rw(3, address, 0x1000, 0);
    current_process_valid_rw_range = {currentPcbPtr->data_start, currentPcbPtr->data_start + 0x1000 / 4};
}

void mpu_enable_6k_rw_at(uint32_t address) {
    auto address_is_8k_aligned = address % 0x2000 == 0;
    zebos::zassert(address_is_8k_aligned);

    // 1/8 von 8k = 1k
    // subregion disable muss sein: 1100 0000
    // das heißt: deaktiviere die oberen 2k

    mpu_set_region_rw(3, address, 0x2000, 0b1100'0000);
    current_process_valid_rw_range = {currentPcbPtr->data_start, currentPcbPtr->data_start + 0x1800 / 4};
}

void mpu_enable_16k_rw_at(uint32_t address) {
    constexpr auto size_bytes = 0x4000;

    auto address_is_16k_aligned = address % size_bytes == 0;
    zebos::zassert(address_is_16k_aligned);

    mpu_set_region_rw(3, address, size_bytes, 0);
    current_process_valid_rw_range = {currentPcbPtr->data_start, currentPcbPtr->data_start + size_bytes / 4};
}


void configureProcessMpuRegion_rw() {
    if (APPLICATION_HEADER->applicationMemoryLen == 0x1000)
    {
        mpu_enable_4k_rw_at(addressToValue(currentPcbPtr->data_start));
    } else if (APPLICATION_HEADER->applicationMemoryLen == 0x1800)
    {
        mpu_enable_6k_rw_at(addressToValue(currentPcbPtr->data_start));
    } else if (APPLICATION_HEADER->applicationMemoryLen == 0x4000)
    {
        mpu_enable_16k_rw_at(addressToValue(currentPcbPtr->data_start));
    } else
    {
        zebos::zassert(false);
    }

    //mpu_set_region_rw(3, addressToValue(currentPcbPtr->data_start), numBytes(currentPcbPtr->data_start, currentPcbPtr->stack_end),0);
    //current_process_valid_rw_range = { currentPcbPtr->data_start, currentPcbPtr->stack_end };
}

extern "C" {
extern uint32_t _os_proc_shared_rw;
}

namespace zebos {

    void initProcessMpu() {
        //this region will always be the same, so only call this function once
        mpu_set_region_rw(0, addressToValue(address_application_text_start), numBytes(address_application_text_start, address_application_text_end), 0);

        mpu_set_region_rw(1, (uint32_t) (&_os_proc_shared_rw), 128, 0);
    }
}// namespace zebos
