//
// Created by seb on 05.10.21.
//

#include "error_correction.h"
#include "address_pointer_helpers.h"
#include "datasegment_relative_address.h"
#include "global_variables.h"
#include "rel_dyn_entry.h"
#include "software_breakpoint.h"
#include <TestResult.h>
#include <handler_HardFault.h>
#include <application_header_struct.h>
#include <debug.h>
#include <error_detection_stack.h>
#include <iterator>
#include <process_pointer_helper.h>
#include <zfunctional.h>

using namespace std;

namespace zebos {
	template <typename T>
    uint8_t getErrorIndex(std::array<T,3> words){
        if (words[0] == words[1]){
            return 2;
        } else if (words[0] == words[2]){
            return 1;
        } else if (words[1] == words[2]){
            return 0;
        } else {
            on_test_finished(TestResult::ErrorDetected_Uncorrectable{});

            // all values are different
            // this is an unrecoverable error
            zebos::zassert(false);



            return 3;
        }
    }

    uint8_t get_correct_index(uint8_t error_index){
	    auto correct_index = error_index;
	    if(error_index == 0){
	        correct_index++;
	    } else {
	        correct_index--;
	    }
        return correct_index;
    }

    void correct_error(const std::array<uint32_t, 3>& words, const std::array<uint32_t*, 3>& addresses){
	    auto error_index = getErrorIndex(words);
	    auto correct_index = get_correct_index(error_index);

		auto errorWord = *addresses[error_index];

	    *addresses[error_index] = *addresses[correct_index];

        test_status.on_error_corrected(addresses[error_index], errorWord, *addresses[correct_index], false);
    }

    void correct_error_dra(const std::array<pid_t,3>& pids,const std::array<datasegment_relative_address,3>& dras, const std::array<uint32_t *,3>& addresses){
	    auto error_index = getErrorIndex(dras);
        auto correct_index = get_correct_index(error_index);

        auto fixedAbsoluteAddress = dras[correct_index].getInverseDRA(pids[error_index]);

		auto errorWord = *addresses[error_index];

        *addresses[error_index] = fixedAbsoluteAddress;

        test_status.on_error_corrected(addresses[error_index], errorWord, fixedAbsoluteAddress, true);
    }


    void correct_error_unkown_if_dra( const std::array<zebos::pid_t,3>& pids, const std::array<uint32_t *,3>& addresses) {
	    const std::array<uint32_t,3> words_no_dra = {
	            *addresses[0],
	            *addresses[1],
	            *addresses[2]
	    };

	    if(zfunctional::all_different(words_no_dra)){
	        std::array<datasegment_relative_address,3> words_dra = {
					datasegment_relative_address(pids[0], *addresses[0]),
					datasegment_relative_address(pids[1], *addresses[1]),
					datasegment_relative_address(pids[2], *addresses[2])
	        };

	        if(zfunctional::all_different(words_dra)){
                on_test_finished(TestResult::ErrorDetected_Uncorrectable{});
                while(1); //unrecoverable error
            } else{
	            correct_error_dra(pids, words_dra, addresses);
            }
        } else {
	        correct_error(words_no_dra, addresses);
        }
    }

    template <size_t SIZE>
    auto all_equal_dra(const std::array<uint32_t ,SIZE>& arr, const std::array<pid_t ,SIZE>& pid){
        std::array<uint32_t, SIZE> dras;

		auto zip = zip_iterator_generator(arr, pid);
        //todo zip arr and pid
        std::transform(begin(zip), end(zip), begin(dras), [] (auto&& tuple) {
			const auto [val, pid] = tuple;
			return getDatasegmentRelativeAdress(pid,val);
		});

		return zfunctional::all_equal(dras);
    }

    /*
	 * This algorithm will perform a full check of the memory segment in a process group
	 * Required steps are:
	 * 1. Get list of known addresses which contain pointers to data
	 * 2. Get the Adresses of all proces data segments
	 */
    void error_correction_rwdata(const std::array<zebos::pid_t,3>& pids) {
		//1. Get list of known addresses which contain pointers to data
		//This information is included in the .rel.dyn section of the application
		auto rel_dyn_entries = getRelDynEntries();

		//what is data_end? end of bss segment
		auto rwdata_num_words = procpointers::get_app_rwdata_num_words();



		const auto data_segments = std::array{
		    std::span<uint32_t>{procpointers::get_proc_rw_start(pids[0]), rwdata_num_words},
		    std::span<uint32_t>{procpointers::get_proc_rw_start(pids[1]), rwdata_num_words},
		    std::span<uint32_t>{procpointers::get_proc_rw_start(pids[2]), rwdata_num_words} };

		auto iter_end = end(data_segments[0]);
        volatile auto end_ = iter_end.base();

        for(auto iter0 = begin(data_segments[0]), iter1 = begin(data_segments[1]), iter2 = begin(data_segments[2]); iter0 != iter_end; iter0++, iter1++, iter2++ ){
            const auto words = std::array{*iter0,*iter1,*iter2};

            if(!zfunctional::all_equal(words)){
                auto linktime_address_current_word = applicationHeader()->_linkage_adr_ram + std::distance(begin(data_segments[0]), iter0);
                auto word_is_address_pointer = zfunctional::is_in(linktime_address_current_word, rel_dyn_entries);

                auto adresses = std::array{ iter0.base(), iter1.base(), iter2.base() };
                volatile auto adresses_ = adresses;

                if(word_is_address_pointer){
                    auto dras = std::array{
                            datasegment_relative_address(pids[0], words[0]),
                            datasegment_relative_address(pids[1], words[1]),
                            datasegment_relative_address(pids[2], words[2])  };

                    if(zfunctional::num_equal(dras) == 0){
                        //all dras are different, it might be a pointer to an adress in rom
                        correct_error(words, adresses);
                    } else if(!zfunctional::all_equal(dras))
                    {
                        // Detected erroneous address, dra
                        correct_error_dra(pids, dras, adresses);
                    }
                } else{
                    // Detected erroneous address, no dra
                    correct_error(words, adresses);
                }
            }
        }
	}

	void run_error_correction(){
        auto pids = std::array{
            processGroup->member_id_to_pid(0).get(),
            processGroup->member_id_to_pid(1).get(),
            processGroup->member_id_to_pid(2).get() };

        auto stackpointers = std::array{
            datasegment_relative_address(pids[0], reinterpret_cast<uint32_t>(processTable[0].stack)),
            datasegment_relative_address(pids[1], reinterpret_cast<uint32_t>(processTable[1].stack)),
            datasegment_relative_address(pids[2], reinterpret_cast<uint32_t>(processTable[2].stack)) };

        if(!zfunctional::all_equal(stackpointers)){
            auto index_fault = getErrorIndex(stackpointers);
            auto index_correct_stack = get_correct_index(index_fault);

            auto corrected_stack = stackpointers[ index_correct_stack ].getInverseDRA( pids[index_fault] );
            processTable[ pids[index_fault] ].stack = reinterpret_cast<uint32_t*>( corrected_stack );
        }

		error_correction_stack(pids);
		error_correction_rwdata(pids);

        if(testConfig.break_after_error_correction){
            software_breakpoint();
        }
    }

	/*
    uint32_t draFix(pid_t pid_source, uint32_t data_source, pid_t pid_target){
		auto dra=getDatasegmentRelativeAdress(pid_source,data_source);
		auto reverse_dra = getReverseDatasegmentRelativeAddress(pid_target, dra);
		return reverse_dra;
	}
    */

	/*
	auto get_correct_value_unknown_dra(uint32_t data_1, uint32_t data_2, pid_t pid_data_1, pid_t pid_data_2, pid_t pid_target){
		if (data_1 == data_2){ return data_1; }
		else if(getDatasegmentRelativeAdress(pid_data_1, data_1) == getDatasegmentRelativeAdress(pid_data_2, data_2) ) {
			return draFix(pid_data_1, data_1, pid_target);
		} else{
			software_breakpoint();
			while(1);
		}
	}
    */

	/*
	 * nur wenn der stack gleich groß ist, ist eine inplace korrktur möglich
	 * wenn der stack unterschiedlich groß ist in dem prozess, in dem ein fehler aufgetreten ist, kann das gar nicht funktionieren
	 * weil ja allein schon die stack große kaputt ist.
	 *
	 * wenn dies der fall ist, mache ein kompletten stack-replace
	 */
/*
	void replace_stack(int processgroup_member_with_error){
		auto pids =  std::array{ processGroup->member_id_to_pid(0), processGroup->member_id_to_pid(1), processGroup->member_id_to_pid(2) };
		auto pids_good = std::array<pid_t, 2>();

		const auto pid_bad = processGroup->member_id_to_pid(processgroup_member_with_error).get();

		copy_if(begin(pids), end(pids), pids_good.begin(), [pid_bad](pid_t pid){ return pid != pid_bad; });

		auto stack_good_0 = gsl::span<uint32_t>(procpointers::get_proc_stack_start(pids_good[0]), procpointers::get_proc_stack_end(pids_good[0]));
		auto stack_good_1 = gsl::span<uint32_t>(procpointers::get_proc_stack_start(pids_good[1]), procpointers::get_proc_stack_end(pids_good[1]));

		if(stack_good_0.size() != stack_good_1.size()){
			//cant perform stack replace, both good stacks vary in size
			software_breakpoint();
			while(1);
		}

		//fix broken stack pointer
		const auto fixed_sp = draFix(pids_good[0], reinterpret_cast<uint32_t>(processTable[pids_good[0]].stack), pid_bad);
		processTable[pid_bad].stack = reinterpret_cast<uint32_t*>(fixed_sp);

		auto stack_bad_start = reinterpret_cast<uint32_t*>(fixed_sp);

		for(auto iter_0 = stack_good_0.begin(), iter_1 = stack_good_1.begin(); iter_0 != stack_good_0.end(); iter_0++, iter_1++ ){
			*stack_bad_start = get_correct_value_unknown_dra(*iter_0,*iter_1,pids_good[0],pids_good[1],pid_bad);
			stack_bad_start++;
		}
		return;
	}
*/


}
