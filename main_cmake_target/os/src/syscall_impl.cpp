//
// Created by seb on 03.11.20.
//

#include "syscall_impl.h"
#include "process_defines.h"
#include <cstdint>
#include <cstring>
#include <global_variables.h>
#include <loader.h>
#include <patch_memory_locations.h>
#include <prepareFirstContext.h>
#include <process.h>
#include <syscalls.h>
#include <yield.h>
#include <handler_HardFault.h>

#include "../../../CMSIS/Core/Include/cmsis_compiler.h"
#include "../../core-stm32g431/include/systick.h"
#include "address_pointer_helpers.h"
#include "syscall_generate_error.h"
#include "syscall_value_compare.h"

extern "C"{
    void firstContext_mark();
}

namespace zebos{
	static void syscall_exit(uint32_t exitcode){
		currentPcbPtr->state = ProcessState::SUSPENDED;
		yield();
		//todo
	}

	static int syscall_fork(){
		//it's not possible to fork on cortex m
		// reason: see comment block below
		return -1;
#if false
		if (currentPcbPtr == nullptr) { return -1; }

		__asm__ volatile (
		"    ;/*  ;speicher aktuellen stack in die pcb_t struktur (.stack)*/\n"
		"    ldr r0, =currentPcbPtr  ;/* currentPcbPtr entspricht adresse auf die variable*/\n"
		"    ldr r1, [r0] ;/* [currentPcbPtr] entspricht adresse auf das erste element im struct*/\n"
		"    add r1, #20 ;/* adresse auf das erste element im struct +20 =adresse vom .stack feld*/\n"
		"    MRS r3, psp\n"
		"    str r3, [r1] ;/*speichere den stackpointer in das .stack feld des aktuellen pcb*/");


		auto ram_start = currentPcbPtr->data_start;
		auto ram_size_b  = numBytes(currentPcbPtr->data_start, currentPcbPtr->got_end);

		auto fork_ram_start = data_addresses_process.pop();
		auto fork_data_start = fork_ram_start;
		auto fork_ram_end = addressOffset(fork_ram_start, ram_size_b);

		auto offsetBetweenProcesses = numBytes(currentPcbPtr->data_start, fork_ram_start);

		//copy ram segment including data, bss, got
		memcpy(fork_ram_start, ram_start, ram_size_b);

		//-8 because we also want to copy the saved registers r4-r11
		auto stack_current = currentPcbPtr->stack - 8;
		auto stack_end = currentPcbPtr->stack_end;
		auto stack_size_b = numBytes(stack_current, stack_end);

		auto fork_stack_current = addressOffset(stack_current,offsetBetweenProcesses);
		auto fork_stack_end = addressOffset(stack_end,offsetBetweenProcesses);

		//copy stack
		memcpy(fork_stack_current, stack_current, stack_size_b);

		auto fork_got_start = addressOffset(currentPcbPtr->got_start, offsetBetweenProcesses);
		auto fork_got_end = addressOffset(currentPcbPtr->got_end, offsetBetweenProcesses);

		//patch all addresses which point to the old forks ram
		patchMemoryLocations(fork_got_start,fork_got_end,offsetBetweenProcesses,reinterpret_cast<uint32_t>(getAddressRamStart()),reinterpret_cast<uint32_t>(getAddressRamEnd()));

		/**
		 * PROBLEM
		 *
		 * The idea here is to patch all addresess in data space and stack which point to a ram address.
		 * Unfortunately, this is not possible.
		 * We need to patch all pointers in dataspace (.data, .bss, stack) which point to a ram address.
		 * Determining which value in dataspace is a pointer is not possible.
		 *
		 * If we would assume that all values in dataspace which are in range of ram start - ram end are pointers and patch these values
		 * there could be addresses included which are not pointers but happen to have a value which seems like a pointer to memory
		 *
		 * There is a way to find out, which addresses in the .data and .bss section need to be patched:
		 * when we link the application as a pie, the linker puts that info into the .reldyn section
		 * so .data and .bss section are no problem...
		 *
		 * The only problem remaining is the stack...
		 */
		//patchMemoryLocations(fork_ram_start,fork_ram_end,offsetBetweenProcesses,reinterpret_cast<uint32_t>(getAddressRamStart()),reinterpret_cast<uint32_t>(getAddressRamEnd()));
		patchMemoryLocations(fork_stack_current,fork_stack_end,offsetBetweenProcesses,reinterpret_cast<uint32_t>(getAddressRamStart()),reinterpret_cast<uint32_t>(getAddressRamEnd()));

		duplicatePidEntry(currentPcbPtr->process_number, offsetBetweenProcesses);
#endif
	}

	void syscall_firstContext(){
		prepareFirstContext();
		firstContext_mark();
	}

    void syscall_yield(){
	    startContextSwitch();
    }

    void syscall_setErrorGeneratorConfig(const error_generator_config::Config* config) {
        static bool oneshot_fired = false;

        // only perform this for one process
        if(!oneshot_fired){
            error_generator.set_config(*config);
            oneshot_fired = true;
        }

        //perform a systick reset, so all processes still have the same systick counter even though one process had more work
	    __ISB();
	    __DSB();
	    systick_reset();
	}

	void syscall_generateErrorFromConfig(ProcessStack* stack){
	    static bool oneshot_fired = false;

	    // only perform this for one process
	    if(!oneshot_fired){
            error_generator.on_error_generation(reinterpret_cast<uint32_t*>(stack));
            oneshot_fired = true;
	    }

	    //perform a systick reset, so all processes still have the same systick counter even though one process had more work
        __ISB();
        __DSB();
	    systick_reset();
    }

	void syscall_schedule_error_correction(){
		processgroup_member_with_error = processGroup->pid_to_member_id(currentPcbPtr->process_number);
	}

	void syscall_test_done(){
        test_done_counter++;
        set_flag_process_data_storage_comparison_required();
		syscall_yield();
	}

	void syscall_compare_result_memory(uint32_t* adr, uint32_t numWords){
        zebos::memory_comparison_range.adresses[currentPcbPtr->process_number] = adr;
		zebos::memory_comparison_range.numWords = numWords;
	}
}

using namespace zebos;

void syscall(ProcessStack* stack) {
	auto syscall_arg_0 = stack->r0;
	auto syscall_arg_1 = stack->r1;
	auto syscall_arg_2 = stack->r2;
	auto syscall_arg_3 = stack->r3;

	//fetch the instruction from the address before previous pc
	auto previous_pc = stack->pc;
	auto address_prev_pc = reinterpret_cast<uint8_t*>(previous_pc);
	auto svc_number = address_prev_pc[-2];

	if constexpr(testConfig.log_syscalls){
		zebos::log::msg("Syscall ");
		zebos::log::msg(svc_number + '0');
		zebos::log::msg(' ');
	}


	switch (svc_number) {
		case SVC_EXIT:
			syscall_exit(syscall_arg_0);
			break;
		case SVC_FORK:
			syscall_fork();
			break;
		case SVC_FIRST_CONTEXT:
			syscall_firstContext();
			break;
		case SVC_VALUE_COMPARE:
			syscall_valueCompare(stack, (uint8_t*) syscall_arg_0, syscall_arg_1);
			break;
	    case SVC_GENERATE_ERROR:
	        syscall_generateError((uint32_t*) syscall_arg_0, syscall_arg_1);
	        break;
        case SVC_SET_ERROR_GENERATOR_CONFIG:
            syscall_setErrorGeneratorConfig((const error_generator_config::Config*) syscall_arg_0);
            break;

        case SVC_GENERATE_ERROR_FROM_CONFIG:
            syscall_generateErrorFromConfig(stack);
            break;

        case SVC_YIELD:
            syscall_yield();
            break;

		case SVC_SCHEDULE_ERROR_CORRECTION:
			syscall_schedule_error_correction();
			break;

		case SVC_TEST_DONE:
			syscall_test_done();
			break;

		case SVC_COMPARE_RESULT_MEMORY:
			syscall_compare_result_memory( (uint32_t*) syscall_arg_0, (uint32_t) syscall_arg_1);
			break;
		default:
			break;

	}
}

void stub(ProcessStackInterruptEntry* syscallNumber) {
}
