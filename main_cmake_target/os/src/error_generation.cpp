//
// Created by seb on 06.10.21.
//

#include "error_generation.h"
#include "software_breakpoint.h"
#include <ErrorGenerator.h>
#include <cstdint>

enum class ErrorGenerationStrategy{
    Oneshot,
    Always,
};

static constexpr auto generation_stategy = ErrorGenerationStrategy::Always;

extern "C" {

	//todo check if this is still required
    void error_generation_from_scheduler(uint32_t* psp){
        static uint32_t scheduler_counter = 0;

        /*
         * Goal:
         * Generate errors according to the error configuration
         *
         * When should an error be generated?
         * -> Option:
         * - Only for first process
         * - Only once
         */

        if constexpr (generation_stategy == ErrorGenerationStrategy::Oneshot){
            if(scheduler_counter == 3){ // process
                on_error_generation(psp);
            }
        } else if constexpr (generation_stategy == ErrorGenerationStrategy::Always){
            if(scheduler_counter % 3 == 0){ // process
                on_error_generation(psp);
            }
        }

        scheduler_counter++;
    }
}
