//
// Created by seb on 10.05.21.
//

#ifndef ZEBOS_SYSCALL_GENERATE_ERROR_H
#define ZEBOS_SYSCALL_GENERATE_ERROR_H

#include <cstdint>
namespace zebos{
    void syscall_generateError(uint32_t* address, uint32_t numBytes);
}


#endif //ZEBOS_SYSCALL_GENERATE_ERROR_H
