//
// Created by seb on 30.10.2021.
//

#include "next_power_of_2.h"
#include <bit>

std::size_t log2_round_up(size_t v) {
    return sizeof(size_t) * 8 - std::countl_zero(v - 1);// nice
}

std::size_t next_power_of_2(std::size_t v) {
    return 1 << (log2_round_up(v));
}