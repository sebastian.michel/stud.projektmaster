//
// Created by seb on 28.10.19.
//

#include "process.h"
#include "process_defines.h"
#include <ProcessStack.h>
#include <address_pointer_helpers.h>
#include <math_util.h>
#include <mpu.h>

using namespace zebos;

#if EXTERN_GLOBAL
pcb_t* currentPcbPtr = nullptr;
uint32_t num_processes = 0;
pcb_t processTable[MaxNumProcesses];
//uint32_t stack[MaxNumProcesses][StackSize];
#endif

namespace zebos{
	//constexpr uint8_t numRegisterFirstContext = 8;
	constexpr uint8_t numRegisterFirstContext = 16;

	static uint32_t pid_counter = 0;



	uint32_t* get_ram_base_address(pid_t pid) {
		return processTable[pid.val].data_start;
	}

	pid_t duplicatePidEntry(pid_t pid_other_process, uint32_t offset_b) {

		processTable[pid_counter] = processTable[pid_other_process.val];
		processTable[pid_counter].process_number = pid_t{pid_counter};
		processTable[pid_counter].state = READY;


		processTable[pid_counter].stack_end =   addressOffset(processTable[pid_counter].stack_end, offset_b);
		processTable[pid_counter].stack =       addressOffset(processTable[pid_counter].stack, offset_b);

		processTable[pid_counter].data_start =  addressOffset(processTable[pid_counter].data_start, offset_b);

		//processTable[pid_counter].got_start =   addressOffset(processTable[pid_counter].got_start, offset_b);
		//processTable[pid_counter].got_end =     addressOffset(processTable[pid_counter].got_end, offset_b);
		processTable[pid_counter].mpu_protected_region_end = addressOffset(processTable[pid_counter].mpu_protected_region_end, offset_b);

		pid_counter++;
		num_processes++;

		return pid_t{pid_counter -1};
	}



	pid_t create_process(
			uint32_t* ramEnd,
			void_function entry,
			uint32_t* gotStartAddress,
			uint32_t* process_ram_start,
			uint32_t* process_ram_rwdata_end
	){

		processTable[pid_counter].entry 		= entry;
		processTable[pid_counter].state 		= READY;
		processTable[pid_counter].process_number = pid_t{pid_counter};
		processTable[pid_counter].systick_last_run = 0;
		processTable[pid_counter].interval 		= 0;
		processTable[pid_counter].stack_end 	= ramEnd;
		processTable[pid_counter].data_start 	= process_ram_start;


		auto process_ram_size = numBytes(process_ram_start, ramEnd);
		auto process_ram_rwdata_size = numBytes(process_ram_start, process_ram_rwdata_end);

		auto mpu_region_size = process_ram_size; // mpu regions can only be power of 2
		processTable[pid_counter].mpu_protected_region_end =  addressOffset(process_ram_start, mpu_region_size);


		uint32_t* lastAdressProcessStack = ramEnd;

		auto stack_end = static_cast<uint32_t*>(lastAdressProcessStack);

		//set stack address to last address minus all registers that will be popped in switchContext
		//processTable[pid_counter].stack = static_cast<uint32_t*>(lastAdressProcessStack-(numRegisterFirstContext));

        auto ps = reinterpret_cast<ProcessStack*>(stack_end - sizeof(ProcessStack)/4);

        processTable[pid_counter].stack = reinterpret_cast<uint32_t*>(ps);

		//set programm status register to thumb mode
        ps->xpsr = static_cast<uintptr_t>(0x01000000);

		//set entry function pointer, which will be popped into pc in switchContext
		// +1 is required so the T-bit is not cleared uppon loading this value into pc!!! https://developer.arm.com/documentation/ddi0337/e/programmer-s-model/registers/special-purpose-program-status-registers--xpsr-
        ps->pc = reinterpret_cast<uint32_t>(entry) +1;
        ps->r9 = reinterpret_cast<uint32_t>(gotStartAddress);

		//stack_end-1 xpsr
		//stack_end-2 pc
		//stack_end-3 r3
		//stack_end-4 r2
		//stack_end-5 r1
		//stack_end-6 r0
		//stack_end-7 r12
		//stack_end-8 lr
		//stack_end-9 r11
		//stack_end-10 r10
		//stack_end-11 r9

		pid_counter++;
		num_processes = pid_counter;

		return pid_t{pid_counter -1};
	}

	task_result_t destroy(pid_t pid){
		processTable[pid.val].state = SUSPENDED;
		return 0;
	}

}
