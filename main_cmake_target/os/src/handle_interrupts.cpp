//
// Created by seb on 29.11.21.
//


#include "../../../CMSIS/Core/Include/cmsis_compiler.h"
#include <../core-stm32g431/include/timer.h>
#include <global_variables.h>

namespace zebos{

}

extern "C" {
	__attribute__((used))
	void handle_interrupts() {
        using namespace zebos;

		// steps to take here:
		// enable all interrupts
		// wait for interrupts
		// disable interrupts

        zebos::timer_error_generation.enableInterrupt();
		__ISB(); // execute pending interrupts (if they have a higher prio than the handler which called this function)
        zebos::timer_error_generation.disableInterrupt();
	}
}