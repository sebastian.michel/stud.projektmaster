//
// Created by seb on 29.10.20.
//

#ifndef ZEBOS_LOADER_H
#define ZEBOS_LOADER_H

#include <cstdint>
#include "stack.h"
#include "memory_space.h"
#include "address_pointer_helpers.h"
#include "global.h"
#include "application_header_struct.h"
#include <process_group.h>
#include "failable.h"

namespace zebos{

	GLOBAL Stack<uint32_t*, 8> data_addresses_process;

	void init_data_addresses();

	pid_t load_application(uint32_t* process_ramStart, const ApplicationHeaderType& applicationHeader, uint32_t processNumber);

	template<uint32_t NumProcesses>
	void initProcessGroup(const ApplicationHeaderType& applicationHeader, ProcessGroup<NumProcesses>& group){
		for(auto i = 0; i < NumProcesses; i++){
			if(data_addresses_process.num_elements() > 0){
				auto adr_app_ram_start = data_addresses_process.pop();
				auto pid = load_application(adr_app_ram_start, applicationHeader, i);
				group.setMember(i,pid, adr_app_ram_start);
			}
		}
	}

	template<uint32_t NumProcesses>
	Failable<ProcessGroup<NumProcesses>> create_process_group(uint32_t address_application_start, const std::array<std::reference_wrapper<ProcessDataStorage>, NumProcesses> data_storage) {
		if (NumProcesses != 2 && NumProcesses != 3){
			return Error(1);
		}

		auto applicationHeader = getApplicationHeader(address_application_start);
		if (applicationHeader->applicationValidCode != 0x42420042){
			return Error(2);
		}

		ProcessGroup<NumProcesses> group(data_storage);
		initProcessGroup(*applicationHeader, group);

		return group;
	}
}


#endif //ZEBOS_LOADER_H
