//
// Created by seb on 04.11.21.
//

#ifndef ZEBOS_ERROR_DETECTION_STACK_H
#define ZEBOS_ERROR_DETECTION_STACK_H

#include "process_defines.h"
#include <cstdint>
#include <span>
#include <array>

namespace zebos{
    void error_correction_stack(
            const std::array<zebos::pid_t,3>& pids);

}


#endif//ZEBOS_ERROR_DETECTION_STACK_H
