//
// Created by seb on 28.10.19.
//

#ifndef EBSY_PROCESS_DEFINES_H
#define EBSY_PROCESS_DEFINES_H

#include <cstdint>
#include "defines.h"



namespace zebos{
	constexpr uint8_t MaxNumProcesses = 10;
	constexpr uint8_t StackSize = 32;

    struct AddressRange{
        uint32_t * start;
        uint32_t * end;

        bool contains(uint32_t* adr) const {
            return adr >= start && adr < end;
        }

        uint32_t numBytes(){
            return (end-start) * sizeof(uint32_t);
        }
    };
    inline AddressRange current_process_valid_rw_range = {nullptr, nullptr};

	enum ProcessState{
		SUSPENDED,
		RUNNING,
		READY,
		WAITING
	};

	struct pid_t{
		pid_t() = default;
		explicit pid_t(uint32_t val) : val(val){}

		bool operator == (const pid_t& other) const = default;
		bool operator != (const pid_t& other) const = default;
		operator uint32_t() const { return val; }

		uint32_t val;
	};
	typedef int32_t task_result_t;

	struct pcb_t {
		void_function entry;        // offset 0x0
		enum ProcessState state;    // offset 0x4
		pid_t process_number;       // offset 0x8
		uint32_t systick_last_run;  // offset 12 0xc
		uint32_t interval;          // offset 16 0x10
		uint32_t *stack;            // offset 20 0x14
		uint32_t *data_start;       // offset 24 0x18 // this includes everything: got, data, bss
		uint32_t *mpu_protected_region_end;       // offset 24 0x18
		uint32_t *stack_end;        // offset 32 0x20

	}; //Process Control Block Type
}

#include "global.h"
GLOBAL zebos::pcb_t* currentPcbPtr;
GLOBAL uint32_t num_processes;
GLOBAL zebos::pcb_t processTable[zebos::MaxNumProcesses];
//GLOBAL uint32_t stack[MaxNumProcesses][StackSize];

#endif //EBSY_PROCESS_DEFINES_H
