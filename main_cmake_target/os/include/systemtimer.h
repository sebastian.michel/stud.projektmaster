//
// Created by seb on 07.10.20.
//

#ifndef PROJECT_NAME_SYSTEMTIMER_H
#define PROJECT_NAME_SYSTEMTIMER_H

#include "defines.h"
#include <stdint.h>

void systemtimer_stop();
void systemtimer_reset();
void systemtimer_start();
void systemtimer_reload(uint32_t value);

void enable_reoccuring_call(uint32_t intervalMs, void_function function);

#endif //PROJECT_NAME_SYSTEMTIMER_H
