//
// Created by seb on 03.11.20.
//

#ifndef ZEBOS_GLOBAL_H
#define ZEBOS_GLOBAL_H

#if false
#define GLOBAL inline
#define EXTERN_GLOBAL false
#else
#define GLOBAL extern
#define EXTERN_GLOBAL true
#endif

#endif //ZEBOS_GLOBAL_H
