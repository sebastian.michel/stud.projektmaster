//
// Created by seb on 07.05.21.
//

#ifndef ZEBOS_GLOBAL_VARIABLES_H
#define ZEBOS_GLOBAL_VARIABLES_H


#include "../../core-stm32g431/include/TimerErrorGeneration.h"
#include "ErrorGenerator.h"
#include "configuration.h"
#include "process_group.h"

namespace zebos{
    inline ProcessGroup<num_process_group_members>* processGroup;
    inline ErrorGenerator error_generator;
	inline uint32_t test_done_counter = 0;
    inline TimerErrorGeneration timer_error_generation;
}

#endif //ZEBOS_GLOBAL_VARIABLES_H
