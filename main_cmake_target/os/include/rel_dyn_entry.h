//
// Created by seb on 05.10.21.
//

#ifndef ZEBOS_REL_DYN_ENTRY_H
#define ZEBOS_REL_DYN_ENTRY_H

#include <cstdint>

#define GSL_TERMINATE_ON_CONTRACT_VIOLATION
#include <gsl-lite.h>

#include <application_header_struct.h>

struct RelDynEntry{
    uint32_t* address;
	uint32_t type;

	bool operator==(long unsigned int* const val){
		return address == val;
	}
};

inline gsl::span<RelDynEntry> getRelDynEntries() {
	return gsl::span<RelDynEntry> (reinterpret_cast<RelDynEntry*>(APPLICATION_HEADER->_reldyn_start),reinterpret_cast<RelDynEntry*>(APPLICATION_HEADER->_reldyn_end));
};


#endif //ZEBOS_REL_DYN_ENTRY_H

