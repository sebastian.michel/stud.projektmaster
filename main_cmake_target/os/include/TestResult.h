//
// Created by seb on 14.12.21.
//

#ifndef ZEBOS_TESTRESULT_H
#define ZEBOS_TESTRESULT_H



namespace zebos::TestResult{


    /* Kein Fehler aufgetreten, wenn:
     * - Alle Prozesse sind durchgelaufen bis zum Ende
     * - Abschließende Prüfung der Daten stimmen in allen Prozessen überein
     */
    struct NoError {
        static constexpr char code = 'n';
    };

    /* Fehler ist aufgetreten, ein Prozess hat nicht beendet.
     * - Nicht alle Prozesse sind fertig geworden
     * - Watchdog hat die Anwendung zurückgesetzt
     */
    struct DeadlockSituation_Watchdog{
        static constexpr char code = 'x';
    };

    /* Ein unerwarteter Fehlerzustand ist aufgetreten, das Betreibssystem weiß nicht wie es damit umgehen soll
     * - Hardfault oder anderer Handler wurde aufgerufen
     */
	/*
    struct UnknownOSState {
        static constexpr char code = 's';
    };
    */

    /* Fehler wurde erkannt und behoben
     * - Alle Prozesse wurden beendet
     * - Fehlerprüfung wurde einmal durchgeführt
     * - Mind. ein Fehler wurde erkannt und behoben
     */
    struct ErrorDetected_Corrected{
        static constexpr char code = 'c';
    };

    /* Fehler wurde erkannt und behoben
     * - Alle Prozesse wurden beendet
     * - Fehlerprüfung wurde einmal durchgeführt
     * - Mind. ein Fehler wurde erkannt und behoben
     * - Endergebnis is trotzdem noch fehlerbehaftet
     */
    struct ErrorDetected_Corrected_ErrorInResult{
        static constexpr char code = 'f'; //f like fail
    };

    /* Fehler wurde erkannt aber bei der Fehlerbehebung ist nichts korrigiert worden
     * Dies ist ein Fehlerzustand, der eigentlich nicht auftreten sollte
     */

    struct ErrorDetected_But_Not_Corrected{
        static constexpr char code = 'q';
    };

    /* Fehler wurde erkannt, konnte allerdings nicht behoben werden
     * - Alle Prozesse wurden beendet
     * - Bei der Fehlerprüfung wurde ein Fehlerzustand festgestellt, der nicht behoben werden konnte
     */
    struct ErrorDetected_Uncorrectable {
        static constexpr char code = 'u';
    };

    /* Fehler ist aufgetreten, aber ist unerkannt geblieben
     * - Alle Prozesse wurden beendet
     * - Keine Fehlerprüfung wurde durchgeführt
     * - Abschließende Prüfung der Daten hat ergeben, dass ein Prozess fehlerhafte Daten hat
     */
    struct ErrorUndetected{
        static constexpr char code = 't';
    };

    template <typename T>   constexpr bool is_unexpected_error = false;
    //template <>             constexpr bool is_unexpected_error<ErrorDetected_But_Not_Corrected> = true;
    //template <>             constexpr bool is_unexpected_error<UnknownOSState> = true;
	template <>             constexpr bool is_unexpected_error<DeadlockSituation_Watchdog> = true;
	template <>             constexpr bool is_unexpected_error<ErrorDetected_Corrected_ErrorInResult> = true;
	template <>             constexpr bool is_unexpected_error<ErrorDetected_Uncorrectable> = true;

}



#endif//ZEBOS_TESTRESULT_H
