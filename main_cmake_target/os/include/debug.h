//
// Created by seb on 03.12.21.
//

#ifndef ZEBOS_DEBUG_H
#define ZEBOS_DEBUG_H

#include <string_view>
namespace zebos::log{
    void msg(void* ptr);
    void msg(char* ptr);
    void msg(char c);
    void msg(std::string_view strview);
    void msgnl(std::string_view strview);
    void flush();
}





#endif//ZEBOS_DEBUG_H
