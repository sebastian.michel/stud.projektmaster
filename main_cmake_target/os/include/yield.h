//
// Created by seb on 24.11.20.
//

#ifndef ZEBOS_YIELD_H
#define ZEBOS_YIELD_H

/**
 * trigger scheduler and context switch
 * also with the timer which is used by the os to start the context switch:
 *  - reset
 *  - restart
 *  otherwise the scheduled process will not have the same time as other processes
 */

void yield();

#endif //ZEBOS_YIELD_H
