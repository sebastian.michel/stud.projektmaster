//
// Created by seb on 18.11.20.
//

#ifndef ZEBOS_ADDRESS_SPACE_H
#define ZEBOS_ADDRESS_SPACE_H

#include <cstdint>
#include "address_pointer_helpers.h"

inline uint32_t const * const address_application_text_start = valueToAddress(0x08008000);
inline uint32_t const * const address_application_text_end = valueToAddress(0x08010000);

#endif //ZEBOS_ADDRESS_SPACE_H
