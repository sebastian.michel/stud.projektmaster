//
// Created by seb on 29.10.20.
//

#ifndef ZEBOS_APPLICATION_HEADER_STRUCT_H
#define ZEBOS_APPLICATION_HEADER_STRUCT_H

#include <cstdint>
#include <defines.h>

typedef struct {
    uint32_t applicationValidCode;
	void_function fun_entry;

	uint32_t* _reldyn_start;
	uint32_t* _reldyn_end;

	uint32_t* _linkage_adr_ram;

    uint32_t* _start_preinitialized_rwdata_rom;
    uint32_t* _end_preinitialized_rwdata_rom;
    uint32_t* _start_bss_ram;
    uint32_t* _end_bss_ram;
    uint32_t applicationMemoryLen;

} ApplicationHeaderType;

constexpr uint32_t applicationStartAddress = 0x08008000;
#define APPLICATION_HEADER ((const ApplicationHeaderType*)(applicationStartAddress))

inline const ApplicationHeaderType* getApplicationHeader(uint32_t address_application_start){
	return reinterpret_cast<const ApplicationHeaderType*>(address_application_start);
}

inline const ApplicationHeaderType* applicationHeader() {
    return getApplicationHeader(applicationStartAddress);
}





#endif //ZEBOS_APPLICATION_HEADER_STRUCT_H
