//
// Created by seb on 09.10.20.
//

#ifndef ZEBOS_PROCESS_H
#define ZEBOS_PROCESS_H

#include "process_defines.h"
namespace zebos{
	pid_t create_process(uint32_t* ramEnd, void_function entry, uint32_t* gotStartAddress, uint32_t* process_ram_start, uint32_t* process_ram_rwdata_end);

/*
pid_t
create_process_with_interval(uint32_t* stackEnd, void_function entry, uint32_t interval, uint32_t gotStartAddress,
                             uint32_t* data_start, uint32_t* bss_end);
*/

	pid_t duplicatePidEntry(pid_t pid_other_process, uint32_t offset_b);

	task_result_t destroy(pid_t pid);

	uint32_t* get_ram_base_address(pid_t pid);

}

#endif //ZEBOS_PROCESS_H
