//
// Created by seb on 03.11.20.
//

#ifndef ZEBOS_ADDRESS_POINTER_HELPERS_H
#define ZEBOS_ADDRESS_POINTER_HELPERS_H

#include <cstdint>

inline uint32_t numBytes(const uint32_t* start, const uint32_t* end){
    return (static_cast<uint32_t>(end - start)) * sizeof(uint32_t);
}

inline uint32_t numWords(const uint32_t* start, const uint32_t* end){
    return (end - start);
}

inline uint32_t* valueToAddress(uintptr_t value){
    return reinterpret_cast<uint32_t*>(value);
}

inline uintptr_t addressToValue(uint32_t const * const address){
    return reinterpret_cast<uintptr_t>(address);
}

inline uint32_t* addressOffset(uint32_t* address, uint32_t bytesOffset){
    return valueToAddress(addressToValue(address) + bytesOffset);
}

template <typename T>
inline bool inRange(T value, T rangeStart, T rangeEnd){
    return value >= rangeStart && value < rangeEnd;
}


#endif //ZEBOS_ADDRESS_POINTER_HELPERS_H
