//
// Created by seb on 05.10.21.
//

#ifndef ZEBOS_PROCESS_POINTER_HELPER_H
#define ZEBOS_PROCESS_POINTER_HELPER_H

#include "application_header_struct.h"
#include "process_defines.h"
#include "address_pointer_helpers.h"

namespace zebos::procpointers {
    // Goal: provide convenient functions for getting the pointers of a process and the application
    inline uint32_t* get_app_rwdata_start() { return applicationHeader()->_linkage_adr_ram; }
    inline uint32_t* get_app_rwdata_end() { return applicationHeader()->_end_bss_ram; }
    inline uint32_t get_app_rwdata_num_words() { return numWords(applicationHeader()->_linkage_adr_ram, applicationHeader()->_end_bss_ram); }// got, data, bss
    inline uint32_t get_app_rwdata_num_bytes() { return numBytes(applicationHeader()->_linkage_adr_ram, applicationHeader()->_end_bss_ram); }// got, data, bss

    inline uint32_t* get_proc_rw_start(pid_t pid) { return processTable[pid.val].data_start; }
    inline uint32_t* get_proc_rw_end(pid_t pid) { return get_proc_rw_start(pid) + get_app_rwdata_num_words();  }
    inline uint32_t get_proc_patch_offset_bytes(pid_t pid) { return numBytes(get_proc_rw_start(pid), processTable[pid.val].data_start); }
    inline uint32_t* get_proc_stack_start(pid_t pid) { return processTable[pid.val].stack; }
    inline uint32_t* get_proc_stack_end(pid_t pid) { return processTable[pid.val].stack_end; } // end of valid adress range




}// namespace zebos::procpointers


#endif//ZEBOS_PROCESS_POINTER_HELPER_H
