//
// Created by seb on 06.10.21.
//

#ifndef ZEBOS_ERROR_GENERATION_H
#define ZEBOS_ERROR_GENERATION_H

#include <cstdint>
extern "C" {
    void error_generation_from_scheduler(uint32_t* psp);
}


#endif//ZEBOS_ERROR_GENERATION_H
