//
// Created by seb on 23.11.21.
//

#ifndef ZEBOS_DATASEGMENT_RELATIVE_ADDRESS_H
#define ZEBOS_DATASEGMENT_RELATIVE_ADDRESS_H
#include "process_defines.h"

namespace zebos{
	// get the datasegment relative adress from an absolute address
	uint32_t getDatasegmentRelativeAdress(pid_t pid, uint32_t absolute_address);

// calculate the reverse datasegement_relative_address with a patch offset from parameter pid
	uint32_t getReverseDatasegmentRelativeAddress(pid_t pid, uint32_t datasegment_relative_address);

	struct datasegment_relative_address{
		datasegment_relative_address() = default;
		datasegment_relative_address(pid_t pid, uint32_t value);;

		[[nodiscard]] uint32_t getInverseDRA(pid_t pid) const;

		bool operator ==(const datasegment_relative_address& other) const = default;
		bool operator !=(const datasegment_relative_address& other) const = default;

		bool operator <(const datasegment_relative_address& other) const;

	public:
		uint32_t dra_value;
	};

}

#endif //ZEBOS_DATASEGMENT_RELATIVE_ADDRESS_H

