//
// Created by seb on 04.12.20.
//

#ifndef ZEBOS_PROCESS_GROUP_H
#define ZEBOS_PROCESS_GROUP_H

#include <array>
#include "process_defines.h"

#include <algorithm>

#include "process_data_storage.h"


namespace zebos {

    template<uint32_t NumMembers>
    class ProcessGroup {

    public:
        ProcessGroup(const std::array<std::reference_wrapper<ProcessDataStorage>, NumMembers> valueCompareEntries)
                : process_data_storage(
                valueCompareEntries) {

		}

		void resetStorages(){
			for (auto i = 0; i < NumMembers; i++){
				process_data_storage[i].get().reset();
			}
		}

        inline void setMember(uint32_t memberNum, pid_t pid, uint32_t* adr_data_start) {
            members[memberNum] = pid;
			process_data_storage[memberNum].get().set_data_start_address(adr_data_start);
        }

		DONT_OPTIMIZE_IF_DEBUG Result put(pid_t pid, uint32_t lr, uint8_t* address, uint32_t numBytes) {
            auto member_id = pid_to_member_id(pid);

            ProcessDataStorage& dataStorage = process_data_storage[member_id];

            if( current_process_valid_rw_range.contains(reinterpret_cast<uint32_t*>(address)) ){
                dataStorage.put(lr, address, numBytes);
            } else {
                dataStorage.put(lr, address, 0);
                on_invalid_memory_access_error(currentPcbPtr->process_number);
                yield();
            }


            return true;
        }

        const ProcessDataStorage& get_data_storage(pid_t pid) {
            auto member_id = pid_to_member_id(pid);
            return process_data_storage[member_id];
        }

        const pcb_t& get_pcb(uint32_t member_id){
			return processTable[member_id_to_pid(member_id)];
        }

    public:
        Failable<pid_t> member_id_to_pid(uint32_t member_id) {
            if ( !isValidMember(member_id)) { return Error(); }
            return members[member_id];
        }

        inline Result isValidMember(uint32_t member_id) {
            return (member_id < size());
        }

        inline bool isLastMember(pid_t pid) {
            return pid_to_member_id(pid) == NumMembers - 1;
        }

        constexpr uint32_t size() {
            return NumMembers;
        }

		std::array<std::reference_wrapper<ProcessDataStorage>, NumMembers>& get_data_storage(){
			return process_data_storage;
        }

    public:
        Failable<uint32_t> pid_to_member_id(pid_t pid) {
            auto item = std::find(members.begin(), members.end(), pid);
            if ( item == members.end()) { return Error(0); }
            else { return static_cast<uint32_t>(*item); }
        }

    public:
        /*
        ValueCompareEntryCollection<NumMembers>& get_value_compare_collection(){
            return value_compare_entries;
        }
        */

    private:
        std::array<pid_t, NumMembers> members;
        std::array<std::reference_wrapper<ProcessDataStorage>, NumMembers> process_data_storage;
    };
}

#endif //ZEBOS_PROCESS_GROUP_H
