//
// Created by seb on 17.11.20.
//

#ifndef ZEBOS_NUMBER_OF_LSL_META_H
#define ZEBOS_NUMBER_OF_LSL_META_H

#include <cstdint>

template <uint32_t NUM>
struct countNumLslOps {
    enum { value = (1UL + countNumLslOps<(NUM >> 1UL)>::value) };
};

template <>
struct countNumLslOps<1> {
    enum { value = 0UL };
};

template <uint32_t NUM>
struct countSetBits {
    enum { value = ((NUM & 1UL ? 1UL : 0UL)  + countSetBits<(NUM >> 1UL)>::value) };
};

template <>
struct countSetBits<0> {
    enum { value = 0UL };
};

template <uint32_t NUM>
constexpr uint32_t getNumLeftShifts(){
    static_assert(countSetBits<NUM>::value == 1, "this value cannot be expressed as (1UL << x)");
    return (uint32_t) countNumLslOps<NUM>::value;
}

#endif //ZEBOS_NUMBER_OF_LSL_META_H
