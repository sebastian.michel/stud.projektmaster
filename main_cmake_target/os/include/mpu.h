//
// Created by seb on 17.11.20.
//

#ifndef ZEBOS_MPU_H
#define ZEBOS_MPU_H

#include <cstdint>

using memory_region_id_t = int32_t;

void mpu_enable();
void mpu_disable();

void mpu_set_region(memory_region_id_t regionId, uint32_t address, uint32_t byteSize, uint32_t access_permission, uint8_t subregions_disabled);

void mpu_disable_region(memory_region_id_t regionId);

void mpu_enable_region(memory_region_id_t regionId);

void mpu_set_region_ro(memory_region_id_t regionId, uint32_t address, uint32_t byteSize, uint8_t subregions_disabled);
void mpu_set_region_rw(memory_region_id_t regionId, uint32_t address, uint32_t byteSize, uint8_t subregions_disabled);
//memory_region_id_t mpu_add_region(uint32_t address, uint32_t byteSize);
void mpu_clear_region(memory_region_id_t regionId);
void mpu_clear_all_regions();

uint32_t nextPowOfTwoForValidMpuRegions(uint32_t v, uint32_t available_space);

#endif //ZEBOS_MPU_H
