//
// Created by seb on 15.12.21.
//

#ifndef ZEBOS_MEMORYREGION_H
#define ZEBOS_MEMORYREGION_H

#include <cstddef>
#include <cstdint>

struct MemoryRegion {
    MemoryRegion() = default;
    MemoryRegion(uint32_t* adr_start, ptrdiff_t size);

    uint32_t* start(){
        return start_;
    }

    ptrdiff_t size(){
        return size_;
    }

    uint32_t* end(){
        return start_ + size_;
    }
private:
    uint32_t* start_ = nullptr;
    ptrdiff_t size_ = 0;
};

#endif//ZEBOS_MEMORYREGION_H
