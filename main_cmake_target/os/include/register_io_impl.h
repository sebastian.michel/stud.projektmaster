//
// Created by seb on 09.10.20.
//

/*
 * NEVER EVER include this file directly as it has no include guard
 * Include register_io.h instead
 *
 * this file having no include guard is on purpose:
 * the inline function definitions of this file do not have to be redefined for non-volatile versions of the functions (see include in register_io.h)
 * This allows further compiler optimizations without code duplication of these functions when using bitops without volatile registers / variables
 *
 */

#include <cstdint>
#include "register_io.h"

inline void setBitsMasked(BIT_OPS_VOLATILE uint32_t& reg, uint32_t mask){
    reg |= mask;
}

inline void resetBitsMasked(BIT_OPS_VOLATILE uint32_t& reg, uint32_t mask){
    reg &= ~mask;
}

inline void setBit(BIT_OPS_VOLATILE uint32_t& reg, uint8_t bitNum){
    setBitsMasked(reg, 1UL << bitNum);
}

inline void resetBit(BIT_OPS_VOLATILE uint32_t& reg, uint8_t bitNum){
    resetBitsMasked(reg, 1UL << bitNum);
}

inline void setBits(BIT_OPS_VOLATILE uint32_t& reg, uint32_t val, uint8_t pos, uint8_t numBits){
    resetBitsMasked(reg, clearmask(numBits) << pos);
    setBitsMasked(reg, val << pos);
}

inline void modBit(BIT_OPS_VOLATILE uint32_t& reg, uint8_t pos, bool value){
    value ? setBit(reg,pos) : resetBit(reg, pos);
}

