//
// Created by seb on 06.12.21.
//

#ifndef ZEBOS_CHECK_TEST_STATUS_H
#define ZEBOS_CHECK_TEST_STATUS_H

#include "debug.h"
#include "logging_config.h"

namespace zebos{
	void log_test_time();
	void log_test_detection_type();
	void log_hardfault_status();
}


extern "C"{
	DONT_OPTIMIZE_IF_DEBUG void check_test_status();
}

#endif//ZEBOS_CHECK_TEST_STATUS_H
