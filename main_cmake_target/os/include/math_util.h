//
// Created by seb on 27.11.20.
//

#ifndef ZEBOS_MATH_UTIL_H
#define ZEBOS_MATH_UTIL_H

#include <cstdint>
uint32_t log2Base(uint32_t v);
uint32_t nextPowOfTwo(uint32_t v);
uint32_t nextPowOfTwo(uint32_t v, uint32_t offset);
#endif //ZEBOS_MATH_UTIL_H
