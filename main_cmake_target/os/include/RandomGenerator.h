//
// Created by seb on 16.12.21.
//

#ifndef ZEBOS_RANDOMGENERATOR_H
#define ZEBOS_RANDOMGENERATOR_H

#include <cstdint>
#include <random>
#include <limits>

class RandomGenerator{
public:
	RandomGenerator(uint32_t seed) {
		this->seed = seed;
		this->generator.seed(seed);
	}

	uint32_t generate(uint32_t maxValue = std::numeric_limits<uint32_t>::max()){
		std::uniform_int_distribution<uint32_t> distribution(0, maxValue);
		return distribution(generator);
	};

private:
	std::default_random_engine generator;
	uint32_t seed __attribute__((used));
};

inline RandomGenerator* rng;


#endif //ZEBOS_RANDOMGENERATOR_H
