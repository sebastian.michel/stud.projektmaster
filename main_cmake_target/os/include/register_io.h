//
// Created by seb on 12.10.20.
//

#ifndef ZEBOS_REGISTER_IO_H
#define ZEBOS_REGISTER_IO_H

#include <cstdint>
constexpr uint32_t clearmask(uint8_t numBits){
    return 0xFFFFFFFF >> (32UL - numBits);
}

#define BIT_OPS_VOLATILE volatile
#include "register_io_impl.h"

#undef BIT_OPS_VOLATILE
#define BIT_OPS_VOLATILE
#include "register_io_impl.h"

#endif //ZEBOS_REGISTER_IO_H
