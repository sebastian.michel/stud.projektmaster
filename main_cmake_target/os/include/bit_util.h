//
// Created by seb on 03.11.21.
//

#ifndef ZEBOS_BIT_UTIL_H
#define ZEBOS_BIT_UTIL_H

extern "C"{
uint32_t count_leading_zeros(uint32_t number);
uint32_t count_trailing_zeros(uint32_t number);
};

#endif//ZEBOS_BIT_UTIL_H
