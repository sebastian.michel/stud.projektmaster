//
// Created by seb on 12.10.20.
//

#ifndef ZEBOS_TO_BIT_REPRESENTATION_H
#define ZEBOS_TO_BIT_REPRESENTATION_H

//Taken from https://stackoverflow.com/questions/14589417/can-an-enum-class-be-converted-to-the-underlying-type

#include <type_traits>

template <typename T> constexpr auto toBitRepresentation(T e) { return static_cast<std::underlying_type_t<T>>(e); }

#endif //ZEBOS_TO_BIT_REPRESENTATION_H
