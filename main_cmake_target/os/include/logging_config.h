//
// Created by seb on 22.12.21.
//

#ifndef ZEBOS_LOGGING_CONFIG_H
#define ZEBOS_LOGGING_CONFIG_H

#define DONT_OPTIMIZE_FUNCTIONS false

#define DONT_OPTIMIZE __attribute__((optimize("O0")))

#if DONT_OPTIMIZE_FUNCTIONS
#define DONT_OPTIMIZE_IF_DEBUG __attribute__((optimize("O0")))
#else
#define DONT_OPTIMIZE_IF_DEBUG
#endif

struct TestConfig{
	bool log_process_numbers = false;
	bool log_random_seed = true;
	bool log_error_extended = false;
	bool log_syscalls = false;

	bool disable_random = false;
    bool random_seed_list = false;
	bool disable_error_generation_from_interrupt = false;
	bool disable_error_generation = false;

    bool break_on_error_generation = false;
    bool break_after_error_generation = false;
    bool break_after_error_correction = false;

	bool break_on_assertion = false;

	bool break_on_software_reset = false;
	bool break_on_error_before_reset = false;

	bool break_on_unusual_error_before_reset = false;
    bool break_if_error_occured = false;

	bool enable_watchdog = true;

    bool error_generator_register = false;
    bool error_generator_datasegment = !error_generator_register;

};
constexpr TestConfig real_run = {};

constexpr TestConfig debug = {
		.log_process_numbers = true,
		.log_syscalls = true,

		.disable_random = true,
		.disable_error_generation_from_interrupt = false,

		.break_on_error_generation = true,
		.break_on_assertion = true,
		.break_on_software_reset = true,
		.break_on_unusual_error_before_reset = true,
		.enable_watchdog = false,

};



constexpr TestConfig real_run_no_random = []() -> TestConfig {
    TestConfig cfg = real_run;
    cfg.disable_random= true;
    cfg.random_seed_list = true;
    return cfg;
}();

constexpr TestConfig custom_debug = []() -> TestConfig
{
	TestConfig cfg;
    cfg.disable_random = true;
	cfg.break_on_software_reset = true;
    //cfg.break_on_assertion = true; // influenes error creation time
    cfg.break_after_error_generation = true;
    cfg.enable_watchdog = false;
    cfg.break_on_unusual_error_before_reset = true;

    /*
	cfg.break_on_error_generation = true;
    cfg.break_after_error_correction = true;


    cfg.break_if_error_occured = true;
	//cfg.log_syscalls = true;
	cfg.log_process_numbers = false;

    */
	return cfg;
}();

constexpr TestConfig testConfig = real_run;


#endif//ZEBOS_LOGGING_CONFIG_H

