//
// Created by seb on 08.12.21.
//

#ifndef ZEBOS_ZASSERT_H
#define ZEBOS_ZASSERT_H

namespace zebos{
    void zassert(bool condition);
}


#endif//ZEBOS_ZASSERT_H
