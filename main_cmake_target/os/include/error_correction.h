//
// Created by seb on 05.10.21.
//

#ifndef ZEBOS_ERROR_CORRECTION_H
#define ZEBOS_ERROR_CORRECTION_H

#include <array>
#include <cstdint>
#include "process.h"
#include "logging_config.h"

namespace zebos{
	DONT_OPTIMIZE_IF_DEBUG void run_error_correction();
    void correct_error_unkown_if_dra(const std::array<zebos::pid_t,3>& pids, const std::array<uint32_t*,3>& addresses);
}


#endif //ZEBOS_ERROR_CORRECTION_H
