//
// Created by seb on 07.05.21.
//

#ifndef ZEBOS_ERROR_DETECTION_H
#define ZEBOS_ERROR_DETECTION_H

#include "process_defines.h"
#include "logging_config.h"

extern "C"{
    __attribute__((used)) DONT_OPTIMIZE_IF_DEBUG void error_detection();
    void on_invalid_memory_access_error(zebos::pid_t pid);
}

namespace zebos{
	inline int processgroup_member_with_error = -1;

    void on_error_occured(pid_t pid);

    void set_flag_process_data_storage_comparison_required();
    void reset_flag_process_data_storage_comparison_required();

}


#endif//ZEBOS_ERROR_DETECTION_H
