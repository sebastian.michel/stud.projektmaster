//
// Created by seb on 31.10.20.
//

#ifndef ZEBOS_MEMORY_SPACE_H
#define ZEBOS_MEMORY_SPACE_H

#include <cstdint>
#include "address_pointer_helpers.h"
#include <array>

uint32_t* getAddressRamStart();
uint32_t* getAddressRamEnd();
uint32_t* getAddressProcessRamStart();
uint32_t  getProcessRamSizeBytes();
uint32_t  getProcessRamSizeBytesPow2Aligned();

uint32_t getMaxNumProcesses();




#endif //ZEBOS_MEMORY_SPACE_H
