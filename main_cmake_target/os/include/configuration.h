//
// Created by seb on 21.04.21.
//

#ifndef ZEBOS_CONFIGURATION_H
#define ZEBOS_CONFIGURATION_H

constexpr auto num_value_compare_entries = 20;
constexpr auto num_process_group_members = 3;



#endif //ZEBOS_CONFIGURATION_H
