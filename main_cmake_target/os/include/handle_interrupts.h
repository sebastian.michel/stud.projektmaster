//
// Created by seb on 29.11.21.
//

#ifndef ZEBOS_HANDLE_INTERRUPTS_H
#define ZEBOS_HANDLE_INTERRUPTS_H

extern "C"{
	void handle_interrupts();
}

#endif //ZEBOS_HANDLE_INTERRUPTS_H
