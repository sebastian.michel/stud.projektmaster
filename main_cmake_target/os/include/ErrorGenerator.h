//
// Created by seb on 06.04.21.
//

#ifndef ZEBOS_ERRORGENERATOR_H
#define ZEBOS_ERRORGENERATOR_H

#include <random>
#include "error_generator_config.h"
#include "logging_config.h"

extern "C" {

void on_error_generation(uint32_t* psp);

void check_error_generation();

}

namespace zebos{
	inline bool error_generation_scheduled = false;

	class ErrorGenerator {
	public:
		void set_config(const error_generator_config::Config& config);

	public:
		void generate_error_register(uint32_t* psp);
		void generate_error_address_range();

		void config_operation_mode(error_generator_config::OperationMode mode);
		void config_set_address_range(uint32_t* start, uint32_t numBytes);
		void config_set_error_registers(error_generator_config::ErrorRegister reg);
		//void enable_induction_for_registers(uint32_t error_reg_mask);


		void on_error_generation(uint32_t* psp);

        void config_enable_all_registers();

	private:
		error_generator_config::Config config;

		error_generator_config::ErrorRegister chose_error_register();

		uint8_t get_random_bit_pos(uint8_t maxPos);

		uint8_t* chose_error_address();

		DONT_OPTIMIZE_IF_DEBUG  static void generate_error(uint8_t* address, uint8_t bitpos);

		DONT_OPTIMIZE_IF_DEBUG  void generate_error(uint32_t* address, uint8_t bitpos);

		uint8_t get_error_bitpos(uint8_t max_bitpos);

    };

}

#endif //ZEBOS_ERRORGENERATOR_H

