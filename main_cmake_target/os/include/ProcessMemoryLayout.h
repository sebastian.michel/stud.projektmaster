//
// Created by seb on 15.12.21.
//

#ifndef ZEBOS_PROCESSMEMORYLAYOUT_H
#define ZEBOS_PROCESSMEMORYLAYOUT_H

#include "MemoryRegion.h"
struct ProcessMemoryLayout{
    MemoryRegion rwdata __attribute__((used));
    MemoryRegion bss __attribute__((used));
    MemoryRegion stack __attribute__((used));

    bool stack_overflow_occured(const uint32_t* stackpointer){
        return stackpointer < stack.start();
    }
};

inline std::array<ProcessMemoryLayout,3 > processMemoryInfo __attribute__((used));


#endif//ZEBOS_PROCESSMEMORYLAYOUT_H
