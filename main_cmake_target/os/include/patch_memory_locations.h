//
// Created by seb on 03.11.20.
//

#ifndef ZEBOS_PATCH_MEMORY_LOCATIONS_H
#define ZEBOS_PATCH_MEMORY_LOCATIONS_H


#include <cstdint>

/**
 * Patches all values in memory between addressStart (inclusive) and addressEnd (exclusive) with a specified offset
 * (adds the offset to the values pointed to by the addresses).
 * The value of an address will only be patched if it is in the range of patchRangeStart (inclusive) to patchRangeEnd (exclusive)
 *
 * @param addressStart first address that will potentially be patched
 * @param addressEnd last address that will potentially be patched
 * @param patchOffset offset which is added to the value of the addresses
 * @param patchRangeStart determines if the underlying value of an address should be patched
 * @param patchRangeEnd determines if the underlying value of an address should be patched
 */
void patchMemoryLocations(uint32_t* addressStart, const uint32_t* addressEnd, uint32_t patchOffset, uint32_t patchRangeStart, uint32_t patchRangeEnd);

void patchMemoryLocation(uint32_t* address, uint32_t patchOffset, uint32_t patchRangeStart, uint32_t patchRangeEnd);

#endif //ZEBOS_PATCH_MEMORY_LOCATIONS_H
