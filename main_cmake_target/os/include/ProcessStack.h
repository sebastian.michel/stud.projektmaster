//
// Created by seb on 22.11.21.
//

#ifndef ZEBOS_PROCESSSTACK_H
#define ZEBOS_PROCESSSTACK_H

struct ProcessStack {
	uint32_t r4  ;
	uint32_t r5  ;
	uint32_t r6  ;
	uint32_t r7  ;
	uint32_t r8  ;
	uint32_t r9  ;
	uint32_t r10 ;
	uint32_t r11 ;

    uint32_t s16 ;
    uint32_t s17 ;
    uint32_t s18 ;
    uint32_t s19 ;
    uint32_t s20 ;
    uint32_t s21 ;
    uint32_t s22 ;
    uint32_t s23 ;
    uint32_t s24 ;
    uint32_t s25 ;
    uint32_t s26 ;
    uint32_t s27 ;
    uint32_t s28 ;
    uint32_t s29 ;
    uint32_t s30 ;
    uint32_t s31 ;


	uint32_t r0  ;
	uint32_t r1  ;
	uint32_t r2  ;
	uint32_t r3  ;
	uint32_t r12 ;
	uint32_t lr  ;
	uint32_t pc  ;
	uint32_t xpsr;

    uint32_t s0  ;
    uint32_t s1  ;
    uint32_t s2  ;
    uint32_t s3  ;
    uint32_t s4  ;
    uint32_t s5  ;
    uint32_t s6  ;
    uint32_t s7  ;
    uint32_t s8  ;
    uint32_t s9  ;
    uint32_t s10 ;
    uint32_t s11 ;
    uint32_t s12 ;
    uint32_t s13 ;
    uint32_t s14 ;
    uint32_t s15 ;
    uint32_t FPSCR  ;
    uint32_t RESERVED  ;
};

struct ProcessStackInterruptEntry {
    uint32_t r0  ;
    uint32_t r1  ;
    uint32_t r2  ;
    uint32_t r3  ;
    uint32_t r12 ;
    uint32_t lr  ;
    uint32_t pc  ;
    uint32_t xpsr;

    uint32_t s0  ;
    uint32_t s1  ;
    uint32_t s2  ;
    uint32_t s3  ;
    uint32_t s4  ;
    uint32_t s5  ;
    uint32_t s6  ;
    uint32_t s7  ;
    uint32_t s8  ;
    uint32_t s9  ;
    uint32_t s10 ;
    uint32_t s11 ;
    uint32_t s12 ;
    uint32_t s13 ;
    uint32_t s14 ;
    uint32_t s15 ;
    uint32_t FPSCR  ;
    uint32_t RESERVED  ;
};

#endif //ZEBOS_PROCESSSTACK_H
