//
// Created by seb on 03.11.20.
//

#ifndef ZEBOS_SYSCALL_IMPL_H
#define ZEBOS_SYSCALL_IMPL_H

#include <cstdint>

#include "ProcessStack.h"

extern "C"{
    void syscall(ProcessStack* syscallNumber);
    void stub(ProcessStackInterruptEntry* syscallNumber);
}


#endif //ZEBOS_SYSCALL_IMPL_H
