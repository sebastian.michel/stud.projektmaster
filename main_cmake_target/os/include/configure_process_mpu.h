//
// Created by seb on 18.11.20.
//

#ifndef ZEBOS_CONFIGURE_PROCESS_MPU_H
#define ZEBOS_CONFIGURE_PROCESS_MPU_H

extern "C"{
    void configureProcessMpuRegion_ro();
    void configureProcessMpuRegion_rw();
}

namespace zebos{
	void initProcessMpu();
}


#endif //ZEBOS_CONFIGURE_PROCESS_MPU_H
