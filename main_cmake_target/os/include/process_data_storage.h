//
// Created by seb on 29.06.21.
//

#ifndef ZEBOS_PROCESS_DATA_STORAGE_H
#define ZEBOS_PROCESS_DATA_STORAGE_H

// Was brauchen wir? Vordefinierter Speicherbereich, um Variablen beliebiger größe darin abzulegen
// Wenn dieser Speicher voll ist, brauchen wir einen kontextwechsel

#include "error_detection.h"
#include "switchContext.h"
#include "yield.h"
#include "zassert.h"
#include <cstdint>
#include <cstring>
#include "failable.h"


namespace zebos {

    constexpr size_t process_data_storage_count = 3;
    constexpr size_t process_data_storage_size = 512;
    inline uint8_t process_data_storage[process_data_storage_count][process_data_storage_size] __attribute__ ((aligned (4)));

    inline size_t AlignUp(size_t size, size_t align)
    {
        return (size + align - 1) & ~(align - 1);
    }

    class ProcessDataStorage{
    public:
		struct CachedElement{
			uint32_t lr = 0;
			size_t size_bytes = 0;
			uint8_t* data = nullptr;
		};

        struct Element{
            uint32_t lr;
            size_t size_bytes;
            uint8_t data[];

			uint8_t* get_next_adr() {
				return data + AlignUp(size_bytes,4);
			}

            const uint8_t* get_next_adr() const {
                return data + AlignUp(size_bytes,4);
            }

            void set(const void* address, size_t size){
                std::memcpy(this->data,address,size);
            }
        };

		template <typename TElem>
		struct ElementComparator{
			const TElem& elem;
			const uint8_t* datasegment_start;

			bool operator==(const ElementComparator& other) const {
				bool lr_equal = elem.lr == other.elem.lr;
				bool size_equal = elem.size_bytes == other.elem.size_bytes;
				bool mem_equal = std::memcmp(elem.data, other.elem.data, elem.size_bytes) == 0;

				return
						lr_equal &&
						size_equal &&
						(mem_equal || compare_data_as_dra_pointer(other));
			}

		private:
			[[nodiscard]] bool compare_data_as_dra_pointer(const ElementComparator& other) const{
				if (elem.size_bytes != sizeof(void*)) { return false; }

				auto data_as_ptr_1 = reinterpret_cast<const uint8_t *>(elem.data) - datasegment_start;
				auto data_as_ptr_2 = reinterpret_cast<const uint8_t *>(other.elem.data) - other.datasegment_start;

				return data_as_ptr_1 == data_as_ptr_2;
			}
		};

    public:
        ProcessDataStorage(uint8_t* adr, size_t size_bytes) :
                adr_storage_cur(adr),
                adr_storage_start(adr),
                adr_storage_end(adr+size_bytes){}

        bool check_enough_space_available(size_t element_data_size){
            auto cur_element = reinterpret_cast<Element*>(adr_storage_cur);

            // check if enough space is available to hold the data
            // get the size of the remaining memory, starting AFTER the "header" of the current element
            auto available_size = adr_storage_end - reinterpret_cast<uint8_t*>(cur_element->data);
            return available_size >= 0 && available_size >= element_data_size;
        }

        void put(uint32_t lr, uint8_t* adr_data, size_t element_data_size){
            bool enough_space_available = check_enough_space_available(element_data_size);

            if(!enough_space_available){
				set_flag_process_data_storage_comparison_required();

				zebos::zassert(cached_element.data == nullptr);

				cached_element.lr = lr;
				cached_element.size_bytes = element_data_size;
				cached_element.data = adr_data;

                yield();
                return;
            }

            auto cur_element = reinterpret_cast<Element*>(adr_storage_cur);
            cur_element->lr = lr;
            cur_element->size_bytes = element_data_size;
            cur_element->set(adr_data, element_data_size);

            adr_storage_cur = cur_element->get_next_adr();
        }

        bool is_empty(){
            return adr_storage_cur == adr_storage_start;
        }

        Failable<const Element*> get_first(){
            return get_next(adr_storage_start);
        }

        Failable<const Element*> get_next(const Element* current){
            return get_next(current->get_next_adr());
        }

		const CachedElement& get_cached_element(){
			return cached_element;
		}

        void reset(){
            adr_storage_cur = adr_storage_start;
			cached_element = {};
        }

        void set_data_start_address(uint32_t* adr_data_start){
        	this->process_data_start_address = reinterpret_cast<uint8_t*> (adr_data_start);
        }

        const uint8_t* get_data_start_adr(){
        	return process_data_start_address;
        }

    private:
        Failable<const Element*> get_next(const uint8_t* next) const{
            if (next == adr_storage_cur){ return Error(); }

            return reinterpret_cast<const Element*>(next);
        }


    private:
        uint8_t* adr_storage_start;
        uint8_t* adr_storage_end;

        uint8_t* adr_storage_cur;

        uint8_t* process_data_start_address;

		CachedElement cached_element;
    };
}

#endif //ZEBOS_PROCESS_DATA_STORAGE_H
