//
// Created by seb on 14.12.21.
//

#ifndef ZEBOS_HANDLER_HARDFAULT_H
#define ZEBOS_HANDLER_HARDFAULT_H

#include "TestResult.h"
#include "check_test_status.h"
#include "debug.h"
#include "reset.h"
#include "zassert.h"
#include "logging_config.h"
#include "../../../CMSIS/Core/Include/cmsis_compiler.h"
#include <array>
#include <cstdint>

namespace zebos{
    struct handler_HardFault{
        enum class DetectionType{
            NoDetection,
            MemmanageHandler,
            Invalid_PC,
            Invalid_SP,
            SafeData
        };

		auto getDetectionTypeString(){
			switch (error_detection_type){
				case handler_HardFault::DetectionType::NoDetection: return "NoDetection";
				case handler_HardFault::DetectionType::MemmanageHandler: return "MemmanageHandler";
				case handler_HardFault::DetectionType::Invalid_PC: return "Invalid_PC";
				case handler_HardFault::DetectionType::Invalid_SP: return "Invalid_SP";
				case handler_HardFault::DetectionType::SafeData: return "SafeData";
				default:
					zassert(false);
					return "";
			}
		}



        void on_error_detected(DetectionType type){
            error_was_detected = true;

			/*
            if(error_detection_type != DetectionType::NoDetection){
                zebos::zassert(false);
            }
            */

            error_detection_type = type;
        }

        void on_error_corrected(uint32_t* address, uint32_t value_error, uint32_t value_correct, bool wasDRA){
            error_was_corrected = true;

            /*
            std::array<char,100> buffer;
            auto numchars = snprintf ( begin(buffer), 100, "Fix DRA: adr %#p / %#x -> %#x", addresses[error_index], errorWord, fixedAbsoluteAddress);
            zebos::log::msgnl({begin(buffer),static_cast<size_t>(numchars)});
             */
        }

    public:
        [[nodiscard]] bool errorWasDetected() const {
            return error_was_detected;
        }
        [[nodiscard]] bool errorWasCorrected() const {
            return error_was_corrected;
        }

	public:
		DetectionType error_detection_type = DetectionType::NoDetection;
		uint32_t hardFault_status = 0;

    private:
        bool error_was_corrected = false;
		bool error_was_detected = false;
    };

    inline handler_HardFault test_status;

    struct MemoryComparisonRange{
        std::array<uint32_t*,3> adresses;
        uint32_t numWords;
    };

    inline MemoryComparisonRange memory_comparison_range;

    template<typename T>
    void on_test_finished(T result){
        using zebos::log::msg, zebos::log::flush;
		log_test_detection_type();
		log_test_time();
		log_hardfault_status();
        msg(result.code);

        if constexpr(testConfig.break_on_unusual_error_before_reset){
		    if(zebos::TestResult::is_unexpected_error<T>){
				__BKPT(0);
			}
		}

        msg('\n');
        flush();

        reset_system();
        while(1);
    }
}

#endif//ZEBOS_HANDLER_HARDFAULT_H
