//
// Created by seb on 08.10.20.
//

#ifndef ZEBOS_USER_LED_H
#define ZEBOS_USER_LED_H

void user_led_init();
void user_led_set();
void user_led_reset();
void user_led_mod(bool val);
void user_led_toggle();

#endif //ZEBOS_USER_LED_H
