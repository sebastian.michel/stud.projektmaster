//
// Created by seb on 08.12.21.
//

#ifndef ZEBOS_IS_POWER_OF_2_H
#define ZEBOS_IS_POWER_OF_2_H

#include <cstdint>
bool is_power_of_two(uint32_t number);

#endif//ZEBOS_IS_POWER_OF_2_H
