//
// Created by seb on 17.11.20.
//

/**
 * For MPU Functional description, see ARMv7-M Architecture Reference Manual
 * Also see CMSIS page for usage of the MPU Functions
 * https://www.keil.com/pack/doc/CMSIS/Core/html/group__mpu__functions.html#details
 *
 *
 */

#include "mpu.h"
#include <is_power_of_two.h>
#include <cmsis_compiler.h>
#include <math_util.h>

#include <number_of_lsl_meta.h>
#include <stm32g4xx.h>
#include <mpu_armv7.h>
#include <zassert.h>


static inline uint8_t byteSizeToMpuRegionSizeId(uint32_t regionSizeBytes){
    switch ( regionSizeBytes ) {
        case 0x20:
            return ARM_MPU_REGION_SIZE_32B;
        case 0x40:
            return ARM_MPU_REGION_SIZE_64B;
        case 0x80:
            return ARM_MPU_REGION_SIZE_128B;
        case 0x100:
            return ARM_MPU_REGION_SIZE_256B;
        case 0x200:
            return ARM_MPU_REGION_SIZE_512B;
        case 0x400:
            return ARM_MPU_REGION_SIZE_1KB;
        case 0x800:
            return ARM_MPU_REGION_SIZE_2KB;
        case 0x1000:
            return ARM_MPU_REGION_SIZE_4KB;
        case 0x2000:
            return ARM_MPU_REGION_SIZE_8KB;
        case 0x4000:
            return ARM_MPU_REGION_SIZE_16KB;
        case 0x8000:
            return ARM_MPU_REGION_SIZE_32KB;
        case 0x10000:
            return ARM_MPU_REGION_SIZE_64KB;
        case 0x20000:
            return ARM_MPU_REGION_SIZE_128KB;
        case 0x40000:
            return ARM_MPU_REGION_SIZE_256KB;
        case 0x80000:
            return ARM_MPU_REGION_SIZE_512KB;
        case 0x100000:
            return ARM_MPU_REGION_SIZE_1MB;
        case 0x200000:
            return ARM_MPU_REGION_SIZE_2MB;
        case 0x400000:
            return ARM_MPU_REGION_SIZE_4MB;
        case 0x800000:
            return ARM_MPU_REGION_SIZE_8MB;
        case 0x1000000:
            return ARM_MPU_REGION_SIZE_16MB;
        case 0x2000000:
            return ARM_MPU_REGION_SIZE_32MB;
        case 0x4000000:
            return ARM_MPU_REGION_SIZE_64MB;
        case 0x8000000:
            return ARM_MPU_REGION_SIZE_128MB;
        case 0x10000000:
            return ARM_MPU_REGION_SIZE_256MB;
        case 0x20000000:
            return ARM_MPU_REGION_SIZE_512MB;
        case 0x40000000:
            return ARM_MPU_REGION_SIZE_1GB;
        case 0x80000000:
            return ARM_MPU_REGION_SIZE_2GB;
        default:
            return 0;
    }
}


/*
uint32_t nextPowOfTwoForValidMpuRegions(uint32_t v, uint32_t available_space) {
    int i = 0;
    uint32_t pow2;
    for(auto val = 0; val < available_space; val+=pow2){
        pow2 = nextPowOfTwo(v, i);
        if(byteSizeToMpuRegionSizeId(pow2) != 0 && byteSizeToMpuRegionSizeId(available_space-pow2) != 0){
            return pow2;
        }
        i++;
    }

    while(1);
}
 */

constexpr uint8_t max_mpu_regions = 16;

void mpu_enable() {
    ARM_MPU_Enable(MPU_CTRL_PRIVDEFENA_Msk);
}

void mpu_disable() {
    ARM_MPU_Disable();
}

static memory_region_id_t currentMpuRegionId = 0UL;




void mpu_set_region_rw(memory_region_id_t regionId, uint32_t address, uint32_t byteSize, uint8_t subregions_disabled) {
    mpu_set_region(regionId, address, byteSize, ARM_MPU_AP_FULL, subregions_disabled);
}

void mpu_set_region_ro(memory_region_id_t regionId, uint32_t address, uint32_t byteSize, uint8_t subregions_disabled) {
    mpu_set_region(regionId, address, byteSize, ARM_MPU_AP_URO,subregions_disabled);
}

void mpu_set_region(memory_region_id_t regionId, uint32_t address, uint32_t byteSize, uint32_t access_permission, uint8_t subregions_disabled) {
    auto byteSizeFlag = byteSizeToMpuRegionSizeId(byteSize);

    ARM_MPU_SetRegionEx(regionId, ARM_MPU_RBAR(regionId,address), ARM_MPU_RASR(0UL, access_permission, 0UL, 0UL, 1UL, 1UL, subregions_disabled, byteSizeFlag));
}

/*
memory_region_id_t mpu_add_region(uint32_t address, uint32_t byteSize) {
    if (currentMpuRegionId == 16) {
        return -1;
    }

    mpu_set_region(currentMpuRegionId, address, byteSize);
    return currentMpuRegionId++;
}
*/

void mpu_clear_region(memory_region_id_t regionId) {
    ARM_MPU_ClrRegion(regionId);
}

void mpu_clear_all_regions() {
    for (auto i = 0UL; i < max_mpu_regions; ++i) {
        ARM_MPU_ClrRegion(i);
    }

    currentMpuRegionId = 0;
}

void mpu_disable_region(memory_region_id_t regionId) {
    MPU->RNR = regionId;
    MPU->RASR &= ~1ul;
}

void mpu_enable_region(memory_region_id_t regionId) {
    MPU->RNR = regionId;
    MPU->RASR |= 1ul;
}
