
#include <rcc.h>

#include "stm32g4xx.h"
#include "stdbool.h"

void rcc_enable_gpioa(){
	RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN;
}

void rcc_enable_gpiob(){
	RCC->AHB2ENR |= RCC_AHB2ENR_GPIOBEN;
}

void rcc_enable_gpioc(){
	RCC->AHB2ENR |= RCC_AHB2ENR_GPIOCEN;
}

void rcc_enable_gpiod(){
	RCC->AHB2ENR |= RCC_AHB2ENR_GPIODEN;	
}

void rcc_enable_syscfg(){
	RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;
}

void rcc_enable_lpuart1() {
    RCC->APB1ENR2 |= RCC_APB1ENR2_LPUART1EN;
}

void rcc_enable_timer2() {
    RCC->APB1ENR1 |= RCC_APB1ENR1_TIM2EN;
}

void rcc_enable_timer3() {
	RCC->APB1ENR1 |= RCC_APB1ENR1_TIM3EN;
}

void rcc_enable_timer4(void) {
	RCC->APB1ENR1 |= RCC_APB1ENR1_TIM4EN;
}

void rcc_enable_timer5(void) {
	RCC->APB1ENR1 |= RCC_APB1ENR1_TIM5EN;
}
