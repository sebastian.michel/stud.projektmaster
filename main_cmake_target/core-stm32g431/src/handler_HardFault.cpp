//
// Created by seb on 14.12.21.
//

#include "handler_HardFault.h"

#include "debug.h"
#include <stm32g4xx.h>
#include <software_breakpoint.h>

extern "C"{
	extern void emergency_schedule();

    void HardFault_Handler() {
        //zebos::software_breakpoint();

		zebos::test_status.hardFault_status = SCB->CFSR;

		emergency_schedule();
		/*
		if(SCB->CFSR & SCB_CFSR_INVPC_Msk != 0){
			emergency_schedule();
		} else {
			zebos::on_test_finished(zebos::TestResult::UnknownOSState{});
			while(1);
		}
		 */
    }

    void BusFault_Handler() {
        HardFault_Handler();
    }

    void UsageFault_Handler() {
        HardFault_Handler();
    }
}

