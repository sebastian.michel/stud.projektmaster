//
// Created by seb on 17.12.21.
//

#include "DWT.h"

#include <stm32g4xx.h>

extern "C"{
    void check_dwt(){
        uint32_t cycles __attribute__((used));
        cycles = get_dtw_cycles() ;
        __NOP();
    }
}

void enableDWT() {
    if (DWT->CTRL != 0) {        // See if DWT is available

        CoreDebug->DEMCR      |= CoreDebug_DEMCR_TRCENA_Msk;  // Set bit 24

        reset_dwt_cycles();

        DWT->CTRL   |= DWT_CTRL_CYCCNTENA_Msk;   // Set bit 0
    }
}

void reset_dwt_cycles() {
    DWT->CYCCNT = 0;
}
uint32_t get_dtw_cycles() {
    return DWT->CYCCNT;
}

