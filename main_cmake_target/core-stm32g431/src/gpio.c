//
// Created by seb on 08.10.20.
//

#include "assertion.h"
#include <gpio.h>
#include <stm32g4xx.h>


void gpio_mode_pin(GPIO_TypeDef* gpio, unsigned pin, GPIO_MODER_Mask mask){
    const unsigned clear_mask = 0b11 << (pin*2);

    gpio->MODER &= ~clear_mask;
    gpio->MODER |= mask << (pin*2);
}

void gpio_toggle(GPIO_TypeDef* gpio, unsigned pin){
    gpio->ODR ^= (1u << pin);
}

void gpio_set(GPIO_TypeDef* gpio, unsigned pin){
    gpio->BSRR |= (1u << pin);
}

void gpio_reset(GPIO_TypeDef* gpio, unsigned pin){
    gpio->BRR |= (1u << pin);
}

typedef enum{
    GPIO_PULL_NONE = 0b00,
    GPIO_PULL_UP   = 0b01,
    GPIO_PULL_DOWN = 0b10
} GPIO_PULLUP_Type;

void gpio_pullup(GPIO_TypeDef* gpio, unsigned pin, GPIO_PULLUP_Type pull_type){
    const unsigned clearmask = 0b11 << pin*2;
    gpio->PUPDR &= ~(clearmask);
    gpio->PUPDR |= pull_type;
}

void gpio_set_alternate_function(GPIO_TypeDef* gpio, unsigned alternate_function_num, unsigned pin){
    assert(alternate_function_num <= 15);

    unsigned AFR_id = pin >= 9 ? 1 : 0;

    gpio->AFR[AFR_id] |=  alternate_function_num << (pin * 4);
}