//
// Created by seb on 08.12.21.
//

#include "zassert.h"
#include <cmsis_compiler.h>
#include <reset.h>
#include <logging_config.h>

namespace zebos {
    void zassert(bool condition) {
#if DEBUG
        if (not condition)
        {
            if constexpr(testConfig.break_on_assertion){
				__BKPT(0);
			}
            while (1)
                ;
        }
#else
        if(not condition){
			if constexpr(testConfig.break_on_assertion){
				__BKPT(0);
			}
            reset_system();
        }
#endif
    }
}