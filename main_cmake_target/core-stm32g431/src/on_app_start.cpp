//
// Created by seb on 17.12.21.
//

#include "on_app_start.h"
#include "debug.h"
#include <DWT.h>
#include <configure_process_mpu.h>
#include <process_defines.h>
#include <stm32g4xx.h>
#include <systemtimer.h>
#include <systick.h>
#include <timer_general.h>
#include "logging_config.h"
#include "global_variables.h"

void on_app_start() {
    reset_dwt_cycles();
	configureProcessMpuRegion_rw();

    if constexpr (testConfig.error_generator_datasegment){
        zebos::error_generator.config_set_address_range(zebos::current_process_valid_rw_range.start,zebos::current_process_valid_rw_range.numBytes());
    }

    timer_restart();
    if constexpr (testConfig.log_process_numbers){
        zebos::log::msg(static_cast<char>(currentPcbPtr->process_number.val+48));
        zebos::log::msg(' ');
    }
	__NOP();
}
