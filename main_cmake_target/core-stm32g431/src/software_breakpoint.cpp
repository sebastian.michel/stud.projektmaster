//
// Created by seb on 16.07.21.
//

#include <stm32g4xx.h>

namespace zebos{
    void software_breakpoint(){
        __BKPT(0);
    }
}