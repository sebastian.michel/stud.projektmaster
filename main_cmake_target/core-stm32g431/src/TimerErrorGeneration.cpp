//
// Created by seb on 22.12.21.
//

#include "TimerErrorGeneration.h"
#include "rcc.h"
#include <ErrorGenerator.h>
#include <timer_general.h>
#include <stm32g4xx.h>

extern "C"{
void TIM4_IRQHandler(){
    TIM4->SR=0;
    zebos::error_generation_scheduled = true;
}
}

namespace zebos {
    static TIM_TypeDef* timer = TIM4;

    TimerErrorGeneration::TimerErrorGeneration() {
        rcc_enable_timer4();
        timer_only_update_on_overflow_underflow(timer);
        timer_enable_interrupt(timer);
        timer_stop_at_reload_value(timer);
    }

    void TimerErrorGeneration::enableInterrupt() {
        NVIC_EnableIRQ(to_interrupt_number(timer));
    }

    void TimerErrorGeneration::disableInterrupt() {
        NVIC_DisableIRQ(to_interrupt_number(timer));
    }
    void TimerErrorGeneration::prescaler(uint32_t prescaler_value) {
        timer_set_psc_divider(timer, prescaler_value);
        timer_apply(timer);
    }
    void TimerErrorGeneration::setReloadValue(uint32_t reload_value) {
        timer_set_reload_value(timer, reload_value);
        timer_apply(timer);
    }

    void TimerErrorGeneration::start() {
        timer_start(timer);
    }
}