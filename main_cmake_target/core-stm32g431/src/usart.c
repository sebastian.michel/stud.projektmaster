//
// Created by seb on 03.12.21.
//

#include "usart.h"

void lpuart1_init() {
    //pa2 af12
    //pa3 af12

    rcc_enable_gpioa();
    gpio_set_alternate_function(GPIOA,12,2);
    gpio_set_alternate_function(GPIOA,12,3);
    gpio_mode_pin(GPIOA,2,GPIO_MODE_Alternate);
    gpio_mode_pin(GPIOA,3,GPIO_MODE_Alternate);

    rcc_enable_lpuart1();
    usart_set_data_bits(LPUART1, 8);
    usart_set_parity(LPUART1,USART_PARITY_None);
    usart_set_stop_bits(LPUART1,USART_STOP_BITS_1);
    usart_set_baud_rate(LPUART1, 9600);
    usart_enable_fifo(LPUART1);
    usart_enable_transmit(LPUART1);
	usart_enable(LPUART1);
/*
	auto msg = "zebOS Serial Line Ready\n";
	usart_send(LPUART1, msg, strlen(msg));
 */
}
void usart_enable_fifo(USART_TypeDef* usart) {
    usart->CR1 |= USART_CR1_FIFOEN;
}
void usart_send_byte(USART_TypeDef* usart, const unsigned char byte) {
	while((usart->ISR & USART_ISR_TXE_TXFNF) == 0){}

    usart->TDR = byte;
}

void usart_send(USART_TypeDef* usart, const char* characters, unsigned len){
    for(unsigned i=0; i<len; i++){
        usart_send_byte(usart,characters[i]);
    }
}

void usart_enable_receive(USART_TypeDef* usart) {
    usart->CR1 |= USART_CR1_RE;
}
void usart_enable_transmit(USART_TypeDef* usart) {
    usart->CR1 |= USART_CR1_TE;
}
void usart_set_baud_rate(USART_TypeDef* usart, unsigned int baudrate) {
    //assume clk usart = 16 Mhz
    if(usart == LPUART1){
        usart->BRR = 16000000u * 256u / baudrate;
    } else {
        usart->BRR = 16000000u / baudrate;
    }
}
void usart_set_stop_bits(USART_TypeDef* usart, USART_STOP_BITS stopBits) {
    if(usart == LPUART1 && (stopBits != USART_STOP_BITS_1 && stopBits != USART_STOP_BITS_2) ){
        assert(false);
    }

    const unsigned clear_mask = 0b11;
    usart->CR2 &= ~ (clear_mask << USART_CR2_STOP_Pos);
    usart->CR2 |=   (stopBits << USART_CR2_STOP_Pos);
}
void usart_set_parity(USART_TypeDef* usart, USART_Parity parity) {
    if(parity == USART_PARITY_None){
        usart->CR1 &= ~(USART_CR1_PCE);
    } else {
        usart->CR1 |= (USART_CR1_PCE);
        if(parity == USART_PARITY_Even){
            usart->CR1 &= ~(USART_CR1_PS);
        } else if (parity == USART_PARITY_Odd){
            usart->CR1 |= (USART_CR1_PS);
        } else {
            assert(false);
        }
    }
}
void usart_set_data_bits(USART_TypeDef* usart, unsigned int n_data_bits) {
    if(n_data_bits == 8){
        usart->CR1 &= ~(1u << USART_CR1_M0_Pos);
        usart->CR1 &= ~(1u << USART_CR1_M1_Pos);
    } else if (n_data_bits == 9){
        usart->CR1 |=  (1u << USART_CR1_M0_Pos);
        usart->CR1 &= ~(1u << USART_CR1_M1_Pos);
    } else if (n_data_bits == 7){
        usart->CR1 &= ~(1u << USART_CR1_M0_Pos);
        usart->CR1 |=  (1u << USART_CR1_M1_Pos);
    } else {
        assert(false);
    }
}

void usart_enable(USART_TypeDef* usart){
	usart->CR1 |= USART_CR1_UE;
}

bool usart_send_queue_empty(USART_TypeDef* usart){
    return
            ( (usart->ISR & USART_ISR_TXFE) != 0 ) &&
            ( (usart->ISR & USART_ISR_TC) != 0 );
}