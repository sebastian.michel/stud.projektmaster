//
// Created by seb on 25.11.20.
//

#include <stdint.h>
#include "mem_manage_fault_magic.h"

uint32_t ram_function_memmanagemagic[3];
uint32_t memmagic_prev_msp;

uint32_t process_handler_stack_frame[8];


void init_memmanage_magic(){
    ram_function_memmanagemagic[1] = 0x0002f06f; //ldr r0, =0xfffffffd, orig: 0xf06f0002
    ram_function_memmanagemagic[2] = 0x00004700; //bx lr, orig: 0x00004700
}

#ifdef __cplusplus
extern "C" {
#endif
/*
void MemManage_Handler_C(){
    configureProcessMpuRegion_rw();
    timer2_restart();
}
 */
#ifdef __cplusplus
}
#endif
