//
// Created by seb on 07.10.20.
//

#include <cstdint>
#include <system_stm32g4xx.h>
#include <stm32g4xx.h>
#include <timer_general.h>
#include "systemtimer.h"
#include "systick.h"

void enable_reoccuring_call(uint32_t intervalMs, void_function function){
    systick_reload_ms(intervalMs, SystemCoreClock);
    systick_set_interrupt(function);
}

void systemtimer_reload(uint32_t value){
	//timer_reload(TIM5, value);
	//timer_apply(TIM5);
    systick_reload(value);
}

void systemtimer_stop() {
	//timer_stop(TIM5);
    systick_disable();
}

void systemtimer_reset() {
	//timer_reset(TIM5);
    systick_reset();
}

void systemtimer_start(){
	//timer_start(TIM5);
    systick_enable();
}

extern "C"{
    void systemtimer_start_ven(){
        systemtimer_start();
    }
}

