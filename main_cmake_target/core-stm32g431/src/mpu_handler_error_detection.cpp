//
// Created by seb on 14.07.21.
//

#include "error_detection.h"
#include "process.h"
#include <stm32g4xx.h>

extern "C"{
// This Handler is for detecting errors which caused an invalid memory access
void MemManage_Handler_C(){
    /* goals:
     * - find out which process caused the fault
     * - find out what that process is allowed to access
     * - find out on which address the invalid access occurred
     * */

    // which process caused the fault?
    auto pid_current_process = currentPcbPtr->process_number;

    // what is that process allowed to access?
    auto valid_mem_region_process_start = currentPcbPtr->data_start;
    auto valid_mem_region_process_end = currentPcbPtr->stack_end;


    // what is the fault address (address of invalid access?)
    // more info in ISBN 978-0-12-408082-9, table 12.3

    // Relevant Registers
    // SCB->CFSR (Fault Status Register)
    // SCB->MMFAR (MemManage Fault Address Register)

    auto fault_address_valid    = (SCB->CFSR & ~(1<<7)) != 0; // read bit 7 of CFSR
    auto fault_address          = SCB->MMFAR;

    on_invalid_memory_access_error(pid_current_process);
}
}