//
// Created by seb on 12.10.20.
//

#include <process_defines.h>
#include "controlRegister.h"
#include "first_context.h"
#include <svc_macros.h>
#include <syscalls.h>

extern "C" void initializeFirstContext();

void firstContext() {
    _first_context();
}