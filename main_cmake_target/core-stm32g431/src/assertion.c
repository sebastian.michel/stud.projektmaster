//
// Created by seb on 03.12.21.
//

#include "assertion.h"
#include "cmsis_compiler.h"

void assert(bool assertion){
    if( ! assertion){
        __BKPT(0);
        while(1);
    }
}
