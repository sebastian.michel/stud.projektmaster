//
// Created by seb on 28.10.19.
//

#include <register_io.h>
#include "systick.h"
#include "defines.h"
#include "stm32g4xx.h"

static void_function systickInterruptFunction;

extern "C"{
    void SysTick_Handler_C(){
        setBit(SCB->ICSR, 25);
        systickInterruptFunction();
    }

    void systick_enable() {
        //clocksource
        setBit(SysTick->CTRL, 2);
        setBit(SysTick->CTRL, 0);
    }

    void systick_log_value(){
        auto val = systick_get_value();
        __NOP();
    }
}




void systick_disable(){
    resetBit(SysTick->CTRL, 0);
}

void systick_set_interrupt_enabled() {
    setBit(SysTick->CTRL,1);
}
void systick_set_interrupt_disabled() {
    resetBit(SysTick->CTRL,1);
}

void systick_set_interrupt_function(void_function function) {
    systickInterruptFunction = function;
}

void systick_set_interrupt(void_function function) {
    systickInterruptFunction = function;
    systick_set_interrupt_enabled();
}

void systick_reload(uint32_t reloadValue) {
    //clockSpeed / 1000 -1 (e.g. 40000 - 1) on clockSpeed (e.g.) 40MHz is 1ms
    SysTick->LOAD = reloadValue;
}

uint32_t systick_get_value() {
    return SysTick->VAL;
}

void systick_reload_ms(uint32_t reloadValueMs, uint32_t clockSpeed) {
    systick_reset();

    //clockSpeed / 1000 -1 (e.g. 400000 - 1) on clockSpeed (e.g.) 40MHz is 1ms
	auto reload_value = reloadValueMs * clockSpeed / 1000 - 1;
    systick_reload(reload_value);
}

void systick_reset() {
    SysTick->VAL = 0;
}
