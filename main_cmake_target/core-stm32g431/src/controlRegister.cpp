//
// Created by seb on 12.10.20.
//

#include "controlRegister.h"

void setProcessorState(ProcessorState state) {
    modBitControlRegister(0, toBitRepresentation(state));
}