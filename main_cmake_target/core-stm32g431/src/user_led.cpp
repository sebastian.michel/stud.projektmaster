//
// Created by seb on 08.10.20.
//

#include <user_led.h>
#include <gpioa.h>
#include <software_breakpoint.h>

void user_led_init() {
    //led is connected to gpio a5
    gpioa_init();

    gpioa_mode(5, GpioMode::Output);
}

void user_led_set() {
    gpioa_set(5);
}

void user_led_reset(){
    gpioa_reset(5);
}

void user_led_mod(bool val){
    gpioa_mod(5,val);
}

void user_led_toggle(){
    gpioa_toggle(5);
    //zebos::software_breakpoint();
}