//
// Created by seb on 26.11.20.
//

#include "timer.h"
#include "assertion.h"
#include "rst_and_clk_control.h"
#include "timer_general.h"
#include <ProcessStack.h>
#include <rcc.h>
#include <stm32g4xx.h>

void_function tim2_irq_fun;

void timer2_init() {
    rcc_enable_timer2();
}

void timer2_stop_at_reload_value() {
    TIM2->CR1 |= TIM_CR1_OPM;
}

void timer2_enable_irq(){
	NVIC_EnableIRQ(TIM2_IRQn);
}

void timer2_disable_irq(){
	NVIC_DisableIRQ(TIM2_IRQn);
}

void timer2_set_irq(void_function fun) {
    tim2_irq_fun = fun;

	timer_only_update_on_overflow_underflow(TIM2);
	__DSB();
	__ISB();
    TIM2->DIER |= TIM_DIER_UIE;
}

void timer2_prescaler(uint16_t val) {
	TIM2->PSC = val-1;
}

void timer2_reload(uint32_t val) {
    TIM2->ARR = val;
    //TIM2->CNT = val;
}

void timer2_start() {
    TIM2->CR1 |= TIM_CR1_CEN;
	__DSB();
	__ISB();
	TIM2->CR1 &= ~TIM_CR1_URS; // only owerflow / underflow can trigger update event
}

void timer2_stop() {
	TIM2->CR1 &= ~TIM_CR1_CEN;
	__DSB();
	__ISB();
}

void timer2_restart() {
    TIM2->CNT = 0;
    TIM2->CR1 |= TIM_CR1_CEN;
}

void timer2_apply(){
	timer_apply(TIM2);
}

void timer3_restart() {
	timer_restart(TIM3);
}

void timer3_stop() {
	timer_stop(TIM3);
}

void timer3_start() {
	timer_start(TIM3);
}

void timer3_apply() {
	timer_apply(TIM3);
}

void timer3_stop_at_reload_value() {
	timer_stop_at_reload_value(TIM3);
}

void timer3_reload(uint16_t val) {
	timer_reload(TIM3, val);
}

void timer3_prescaler(uint16_t val) {
	timer_set_psc_divider(TIM3, val);
}

void timer3_init() {
	rcc_enable_timer3();
}

void timer_stop(TIM_TypeDef* timer) {
	timer->CR1 &= ~TIM_CR1_CEN;
}

void timer_start(TIM_TypeDef* timer) {
	timer->CR1 |= TIM_CR1_CEN;
}

void timer_reset(TIM_TypeDef* timer) {
	timer->CNT = 0;
}


void timer_apply(TIM_TypeDef* timer) {
	/*
	Reference Manual, TIMx_EGR Bit 0 UG

	Re-initialize the counter and generates an update of the registers. Note that the prescaler
	counter is cleared too (anyway the prescaler ratio is not affected). The counter is cleared if
	the center-aligned mode is selected or if DIR=0 (upcounting), else it takes the auto-reload
	value (TIMx_ARR) if DIR=1 (downcounting).
	*/

	timer->EGR |= TIM_EGR_UG;
}

void timer_set_reload_value(TIM_TypeDef* timer, unsigned int value) {
	timer->ARR = value;
}

void timer_enable_interrupt(TIM_TypeDef* timer) {
	NVIC_EnableIRQ(to_interrupt_number(timer));

	timer->DIER |= TIM_DIER_UIE; // timer update interrupt enable
}



IRQn_Type to_interrupt_number(TIM_TypeDef* timer) {
    if (timer == TIM2)
        return TIM2_IRQn;
    if (timer == TIM3)
        return TIM3_IRQn;
    if (timer == TIM4)
        return TIM4_IRQn;
    if (timer == TIM5)
        return TIM5_IRQn;
    if (timer == TIM6)
        return TIM6_DAC_IRQn;
    if (timer == TIM7)
        return TIM7_DAC_IRQn;

    assert(false);
    return HardFault_IRQn;
}

void timer_set_psc_divider(TIM_TypeDef* timer, unsigned int value) {
	assert(value != 0);

	timer->PSC = value - 1;
}

void timer_stop_at_reload_value(TIM_TypeDef* timer) {
	timer->CR1 |= TIM_CR1_OPM;
}

void timer_reload(TIM_TypeDef* timer, uint32_t val) {
	timer->ARR = val;
}

uint32_t timer_getValue(TIM_TypeDef* timer){
	return timer->CNT;
}

unsigned timer3_getValue(){
	return timer_getValue(TIM3);
}

void timer_restart(TIM_TypeDef* timer){
	timer->CNT = 0;
	timer->CR1 |= TIM_CR1_CEN;
}

void timer_only_update_on_overflow_underflow(TIM_TypeDef* timer){
	timer->CR1 |= TIM_CR1_URS;
}