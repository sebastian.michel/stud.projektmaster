//
// Created by seb on 12.05.21.
//

#include "random.h"
#include <stm32g4xx.h>

static bool initialized = false;

static void init(){
	RCC->CRRCR |= RCC_CRRCR_HSI48ON;
	while( (RCC->CRRCR & RCC_CRRCR_HSI48RDY)  == 0){}

	RCC->AHB2ENR |= RCC_AHB2ENR_RNGEN;
	initialized = true;
}

uint32_t generate_random_number(){
	if (!initialized){ init(); }
	//enable generator
	RNG->CR = RNG_CR_RNGEN;

	//wait while data not ready
	auto rdy = false;
	while( (RNG->SR & RNG_SR_DRDY) == 0 ) {	}

	//disable generator
	RNG->CR &= ~RNG_CR_RNGEN;

	return RNG->DR;
}