//
// Created by seb on 12.10.20.
//

#include "switchContext.h"
#include "pendsv.h"
#include <cmsis_compiler.h>
#include <systick.h>
#include <timer_general.h>
#include <stm32g4xx.h>
#include <systemtimer.h>
#include <process_defines.h>

static uint32_t last_timer_val = 0;

void startContextSwitch(){
    __DMB();
    __DSB();
    __ISB();

    systemtimer_stop();
    systemtimer_reset();

    PendSV_SetPending();
    __DMB();
    __DSB();
    __ISB();
}

#include <timer_general.h>
