//
// Created by seb on 08.10.20.
//

#include "system_init.h"
#include "stm32g4xx.h"
#include "user_led.h"
#include <DWT.h>
#include <cmsis_compiler.h>
#include <mem_manage_fault_magic.h>
#include <pendsv.h>

void system_init(){
    PendSV_Init();
    init_memmanage_magic();
    user_led_init();

    //floating point operations
    CONTROL_Type ctrl;
    ctrl.w = __get_CONTROL();
    ctrl.b.FPCA = true;
    __set_CONTROL(ctrl.w);
    FPU->FPCCR &= ~FPU_FPCCR_LSPEN_Msk;

    enableDWT();
}