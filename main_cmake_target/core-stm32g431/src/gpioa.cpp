//
// Created by seb on 03.12.21.
//

#include "gpioa.h"
#include "rcc.h"
#include <stm32g4xx.h>
#include "to_bit_representation.h"

#include <register_io.h>

void gpioa_init() {
    rcc_enable_gpioa();
}

void gpioa_mode(uint8_t pin, GpioMode mode) {
    setBits(GPIOA->MODER, toBitRepresentation(mode), pin * 2, 2);
}

void gpioa_set(uint8_t pin){
    GPIOA->BSRR = 1UL << pin;
}

void gpioa_reset(uint8_t pin){
    GPIOA->BRR = 1UL << pin;
}

bool gpioa_read(uint8_t pin){
    return GPIOA->IDR & 1UL << pin;
}

void gpioa_mod(uint8_t pin, bool val){
    val ? gpioa_set(pin) : gpioa_reset(pin);
}

void gpioa_toggle(uint8_t pin){
    gpioa_mod(pin, !gpioa_read(pin));
}