//
// Created by seb on 09.12.21.
//

// target: reset all values used by the interrupt handler on the current process-stack to 0

#include "cmsis_compiler.h"
#include <cstdbool>
#include <ProcessStack.h>

extern "C"{
	void reset_stack_after_interrupt(){
		auto psp = __get_PSP();


		unsigned magic_number = 0; // this depends on the algorithm of the error generation
		unsigned num_words_to_reset = sizeof(ProcessStack) / 4 + magic_number;

		bool is_8_byte_aligned = psp % 8;
		if( not is_8_byte_aligned){
			num_words_to_reset += 1;
		}

	}
}
