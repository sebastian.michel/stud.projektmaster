//
// Created by seb on 31.10.20.
//

#include <application_header_struct.h>
#include <memory_space.h>
#include <next_power_of_2.h>
extern "C" {
/* defined by the linker script */
extern uint32_t address_ram_start;
extern uint32_t address_ram_end;
extern uint32_t address_os_ram_end;
extern uint32_t _demoapp_memory_len;
}

uint32_t* getAddressRamStart() {
    return &address_ram_start;
}

uint32_t* getAddressRamEnd() {
    return &address_ram_end;
}

uint32_t* getAddressProcessRamStart() {
    return reinterpret_cast<uint32_t*>(0x20000000);
}

uint32_t getProcessRamSizeBytes() {
    return APPLICATION_HEADER->applicationMemoryLen;
    return 0x1800; // 6k
}

uint32_t getProcessRamSizeBytesPow2Aligned() {
    return next_power_of_2(getProcessRamSizeBytes());
}

uint32_t getMaxNumProcesses() {
	auto available_bytes_ram = addressToValue(getAddressRamEnd()) - addressToValue(getAddressProcessRamStart());

    //mpu requirement: processes must be power of 2 aligned
    auto processSizeWithAlignment = getProcessRamSizeBytesPow2Aligned();

	auto numProcesses = available_bytes_ram / processSizeWithAlignment;
	return numProcesses;
}
