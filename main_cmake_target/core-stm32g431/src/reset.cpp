//
// Created by seb on 07.12.21.
//

#include "reset.h"
#include <stm32g4xx.h>
#include <handler_HardFault.h>
#include "logging_config.h"

void reset_system(){
	if constexpr(testConfig.break_on_software_reset){
		__BKPT(0);
	}

	if constexpr(testConfig.break_on_error_before_reset){
		if (zebos::test_status.error_detection_type != zebos::handler_HardFault::DetectionType::NoDetection){
			__BKPT(0);
		}
	}
    NVIC_SystemReset();
}