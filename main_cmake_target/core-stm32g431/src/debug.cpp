//
// Created by seb on 03.12.21.
//

#include <array>
#include "debug.h"
#include "usart.h"
#include "to_string.h"

namespace zebos::log{
    void msg(void* ptr){
        msg(std::string_view {to_string(ptr).begin() });
    }

    void msg(char* ptr){
        msg(std::string_view {ptr });
    }

    void msg(char c){
        usart_send_byte(LPUART1, c);
    }

    void msg(std::string_view strview){
        for (auto c : strview){
            usart_send_byte(LPUART1, c);
        }
    }
    void msgnl(std::string_view strview){
        for (auto c : strview){
            usart_send_byte(LPUART1, c);
        }
        usart_send_byte(LPUART1, '\n');
    }

    void flush(){
        while (not usart_send_queue_empty(LPUART1)){
            __NOP();
        }
    }
}
