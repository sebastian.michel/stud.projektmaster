//
// Created by seb on 24.11.20.
//


#include <prepareFirstContext.h>
#include <controlRegister.h>
#include <mpu.h>

void prepareFirstContext() {
    // enable mpu here, as the os is in handler mode atm. therefore the enabled mpu will not hinder the system startup

    mpu_enable();
    setProcessorState(ProcessorState::User);
}