#include "pendsv.h"
#include <stm32g4xx.h>
#include <register_io.h>

void PendSV_SetPriority(){
    NVIC_SetPriority(PendSV_IRQn, 32);
}

void PendSV_Init(){
    PendSV_SetPriority();
}

void PendSV_SetPending(){
setBit(SCB->ICSR, SCB_ICSR_PENDSVSET_Pos);
}

void PendSV_ClearPending(){
setBit(SCB->ICSR, SCB_ICSR_PENDSVCLR_Pos);
}

