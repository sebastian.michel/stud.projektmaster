//
// Created by seb on 24.11.20.
//

#include <yield.h>
#include <pendsv.h>
#include <systick.h>

void yield(){
    systick_disable();
    systick_reset();
    PendSV_SetPending();
}