.syntax unified
.cpu cortex-m4
.fpu softvfp
.thumb

.global SysTick_Handler_C
.global SysTick_Handler

.section .text.SysTick_Handler
.balign 4
.thumb_func

SysTick_Handler:
    push {lr}
    push {r2}
    bl SysTick_Handler_C
    pop {r2}
    pop {lr}
    bx lr
.end