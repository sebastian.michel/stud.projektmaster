.syntax unified
.cpu cortex-m4
.thumb

.global asm_stack_reset
.global stack_reset_saved_pc
.global test_link_to_ram
.global tmp_r0

.section .ramtext.asm_stack_reset,"ax",%progbits
.balign 4

/* this needs to be places in ram, so variable "stack_reset_saved_pc" can be written */
.thumb_func
.type	asm_stack_reset, %function
asm_stack_reset:
    cpsid i /* disable interrupts */
    /*bkpt 0*/

    push {r1, r2}


    ldr r1, =tmp_r0
    str r0, [r1] /* safe r0 to tmp_r0 */

    mrs r0, xpsr
    push {r0}

    /* r2 = number of words to reset (in bytes) */
    mov r2, #(34*4 - 3*4) /* stack frame with all controller saved registers (incl. fp registers) are 34 words, -2 words which were just pushed on the stack

    /* if sp is not 8 byte aligned, clear an additional word*/
    mov r1, sp
    sub r1, #(3*4)
    and r1, 0b111 /* r1 = sp % 8 */
    /* if ( (sp % 8) != 0 ) { r0++; } */
    cmp r1, 0
    it ne
    addne r2, #4

    /* r0 is loaded with value 0x55555555, this is written to all words on the stack modified by the exception entry sequence */
    /* this value then ist ignored */
    mov r0, #0x55555555
    mov r1, sp
    sub r1, r2
    mov r2, sp
reset_loop: /* reset all words from r1 until sp to 0x55555555 */
    str r0, [r1]
    add r1, #4
    cmp r1, r2
    bne reset_loop

	pop {r1}
	msr xpsr, r1
    pop {r1, r2} /* restore the previously saved registers */

    /* write 0 to all stack values which where pushed in this function */
    /* r0 & r1, so 2 words */
    str r0, [sp, #-4]
    str r0, [sp, #-8]
    str r0, [sp, #-12]

    ldr r0, [pc, #8]
    .balign 4
    cpsie i /* enable interrupts */

	.balign 4

	/* load actual return address */
	ldr pc, [pc, #0]
	.balign 4
stack_reset_saved_pc:
	.word 0
tmp_r0:
	.word 0

	.end