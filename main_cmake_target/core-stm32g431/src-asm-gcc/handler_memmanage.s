.syntax unified
.cpu cortex-m4
.fpu softvfp
.thumb

.global MemManage_Handler_C
.global MemManage_Handler
.global currentPcbPtr
.global on_invalid_memory_access_error

.section .text.MemManage_Handler
.balign 4
.thumb_func

MemManage_Handler:
	//bkpt 0
    ldr r0, =currentPcbPtr
    ldr r0, [r0]
    ldr r0, [r0, 8] // faulty process id

    bl on_invalid_memory_access_error
    b emergency_schedule
.end
