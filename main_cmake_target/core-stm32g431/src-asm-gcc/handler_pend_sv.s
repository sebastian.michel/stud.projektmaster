.syntax unified
.cpu cortex-m4
.fpu fpv4-sp-d16
.thumb

.global PendSV_Handler
.global firstContext_mark
.global systick_enable
.global handle_interrupts
.global check_error_generation
.global emergency_schedule
.global check_test_status

.section .text.PendSV_Handler

.thumb_func
PendSV_Handler:
    ;/*;# r4-r7 auf psp sichern*/
	//cpsid i
register_save:
    MRS   r0, psp

    SUBS  r0, #(8*4 + 16*4) //8 regular registers + 16 fp registers
    STMIA r0!,{r4-r11}
    VSTM.32 r0!,{s16-s31}
    SUBS  r3, r0, #(8*4 + 16*4) ; /* r3 = psp */


    ;/*  ;speicher aktuellen stack in die pcb_t struktur (.stack)*/
    ldr r0, =currentPcbPtr  ;/* currentPcbPtr entspricht adresse auf die variable*/
    ldr r1, [r0] ;/* [currentPcbPtr] entspricht adresse auf das erste element im struct*/
    add r1, #20 ;/* adresse auf das erste element im struct +20 =adresse vom .stack feld*/

    str r3, [r1] ;/*speichere den stackpointer in das .stack feld des aktuellen pcb*/

firstContext_mark:
	bl handle_interrupts

	//bl check_dwt
    bl check_error_generation
emergency_schedule:
	bl error_detection
	bl check_test_status
	bl schedule

    ;/*lade process stack in psp*/
    ldr r0, =currentPcbPtr ;/*currentPcbPtr entspricht adresse auf die variable*/
    ldr r1, [r0] ;/* [currentPcbPtr] entspricht adresse auf das erste element im struct*/
    add r1, #20 ;/* adresse auf das erste element im struct +20 =adresse vom .stack feld*/
    ldr r2, [r1]
    ;/*setze psp auf korrekten stack*/
    MSR psp, r2

    bl on_app_start
    dmb
    dsb
    isb

    bl systemtimer_start_ven

//start timer 2
    ldr r0, =0x40000000 // timer 2 cr1
    ldr r1, [r0]
    orr r1, (1<<0)
    str r1, [r0]

//register_restore:
    ;/*r4-r7 laden von psp*/
    MRS r0, psp
    LDMIA r0!, {r4-r11}
    VLDM.32 r0!, {s16-s31}
    msr psp, r0

    ldr r0, =0xFFFFFFeD
    //cpsie i
    bx r0

    	/*
    	last 4 bit:
    	EXC_RETURN
    	Return to  Return stack    Frame type (FP support)

    	0xFFFFFFE1
    	Handler mode.     Main     Extended

    	0xFFFFFFE9
    	Thread mode     Main     Extended

    	0xFFFFFFED
    	Thread mode      Process     Extended

    	0xFFFFFFF1
    	Handler mode.     Main     Basic

    	0xFFFFFFF9
    	Thread mode     Main     Basic

    	0xFFFFFFFD
    	Thread mode     Process     Basic
    		 */

    .end
	