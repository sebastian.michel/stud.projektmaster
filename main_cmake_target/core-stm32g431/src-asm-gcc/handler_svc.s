.syntax unified
.cpu cortex-m4
.fpu fpv4-sp-d16
.thumb

.global syscall
.global SVC_Handler

.section .text.SVC_Handler
.balign 4
.thumb_func

SVC_Handler:
    TST lr, #4
    beq msp_used
    bne psp_used

msp_used:
    MRS r0, MSP
    bl safe_registers
    MSR msp, r0

    BL syscall

    MRS r0, MSP
    BL restore_registers
    MSR MSP, r0
    b jump_back

psp_used:
    MRS r0, PSP
    bl safe_registers
    MSR psp, r0

    BL syscall

    MRS r0, PSP
    BL restore_registers
    MSR psp, r0
    b jump_back

jump_back:
    ldr r0, =0xffffffed
    bx r0

safe_registers:
    mov r1, #(8*4 + 16*4)
    SUBS  r0, r1
    STMIA r0!,{r4-r11}
    VSTM.32 r0!,{s16-s31}
    SUBS  r0, r1

    bx lr

restore_registers:
    LDMIA r0!, {r4-r11}
    VLDM.32 r0!,{s16-s31}
    bx lr

.end
