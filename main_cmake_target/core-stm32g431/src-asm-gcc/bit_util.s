.syntax unified
.cpu cortex-m4
.fpu softvfp
.thumb

.global is_power_of_two
.global count_leading_zeros
.global count_trailing_zeros

.section .text.bit_util
.balign 4
.thumb_func

count_leading_zeros:
    clz r0, r0
    bx lr

count_trailing_zeros:
    rbit r0, r0
    clz r0, r0
    bx lr
