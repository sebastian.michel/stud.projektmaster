//
// Created by seb on 26.11.20.
//

#ifndef ZEBOS_TIMER_H
#define ZEBOS_TIMER_H

#include <stdint.h>
#include <defines.h>

void timer2_init();
void timer2_prescaler(uint16_t val);
void timer2_reload(uint32_t val);
void timer2_stop_at_reload_value();
void timer2_set_irq(void_function fun);
void timer2_enable_irq();
void timer2_disable_irq();

void timer2_apply();

void timer2_start();
void timer2_stop();
void timer2_restart();

void timer3_init();
void timer3_prescaler(uint16_t val);
void timer3_reload(uint16_t val);
void timer3_stop_at_reload_value();
void timer3_set_irq(void_function fun);
void timer3_enable_irq();
void timer3_disable_irq();

void timer3_apply();

void timer3_start();
void timer3_stop();
void timer3_restart();
unsigned timer3_getValue();



#endif //ZEBOS_TIMER_H
