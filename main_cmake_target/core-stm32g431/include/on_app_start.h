//
// Created by seb on 17.12.21.
//

#ifndef ZEBOS_ON_APP_START_H
#define ZEBOS_ON_APP_START_H

extern "C" {
    extern void timer_restart();
    void on_app_start() __attribute__((used));
}

#endif//ZEBOS_ON_APP_START_H
