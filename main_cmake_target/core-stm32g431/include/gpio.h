//
// Created by seb on 08.10.20.
//

#ifndef ZEBOS_GPIO_H
#define ZEBOS_GPIO_H

#include <stm32g4xx.h>


#ifdef __cplusplus
#include <cstdint>
extern "C"{
#else
#include <stdint.h>
#endif

typedef enum {
    GPIO_MODE_Input = 0b00,
    GPIO_MODE_Output = 0b01,
    GPIO_MODE_Alternate = 0b10,
    GPIO_MODE_Analog = 0b11,
    } GPIO_MODER_Mask;

void gpio_mode_pin(GPIO_TypeDef* gpio, unsigned pin, GPIO_MODER_Mask mask);

void gpio_toggle(GPIO_TypeDef* gpio, unsigned pin);
void gpio_set(GPIO_TypeDef* gpio, unsigned pin);
void gpio_reset(GPIO_TypeDef* gpio, unsigned pin);

void gpio_set_alternate_function(GPIO_TypeDef* gpio, unsigned alternate_function_num, unsigned pin);

#ifdef __cplusplus
}
#endif

#endif //ZEBOS_GPIO_H
