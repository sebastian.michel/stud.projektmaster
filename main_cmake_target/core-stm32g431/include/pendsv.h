
#ifndef PENDSV_H
#define PENDSV_H

#include "defines.h"

void PendSV_Init(void);
extern "C" void PendSV_Handler(void);
void PendSV_ClearPending(void);
void PendSV_SetPending(void);

#endif //PENDSV_H
