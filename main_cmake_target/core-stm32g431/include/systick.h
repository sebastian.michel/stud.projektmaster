//
// Created by seb on 28.10.19.
//

#ifndef EBSY_SYSTICK_H
#define EBSY_SYSTICK_H

#include <stdint.h>
#include <stdbool.h>
#include "defines.h"

extern uint32_t systemTimeMs;

extern "C"{
    void systick_enable();
	void systick_log_value() __attribute__((used));
}

void systick_disable();

void systick_set_interrupt_enabled();
void systick_set_interrupt(void_function function);
void systick_reload(uint32_t reloadValue);
void systick_reload_ms(uint32_t reloadValueMs, uint32_t clockSpeed);
void systick_reset();
uint32_t systick_get_value() __attribute__((used));

void systick_set_interrupt_function(void_function function);

#endif //EBSY_SYSTICK_H
