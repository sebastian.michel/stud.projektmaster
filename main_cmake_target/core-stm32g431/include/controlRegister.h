//
// Created by seb on 12.10.20.
//

#ifndef ZEBOS_CONTROLREGISTER_H
#define ZEBOS_CONTROLREGISTER_H

#include <register_io.h>
#include <cmsis_compiler.h>
#include "to_bit_representation.h"

enum class ProcessorState : uint8_t{
    Privileged = 0b0,
    User = 0b1
};

enum class StackPointer : uint8_t{
    Main = 0b0,
    Process = 0b1
};

__attribute__((always_inline)) inline void modBitControlRegister(uint8_t pos, bool value){
    auto curState = __get_CONTROL();
    modBit(curState, pos, value);
    __set_CONTROL(curState);
}

void setProcessorState(ProcessorState state);

__attribute__((always_inline)) inline void selectStackpointer(StackPointer sp){
	modBitControlRegister(1, toBitRepresentation(sp));
}



#endif //ZEBOS_CONTROLREGISTER_H
