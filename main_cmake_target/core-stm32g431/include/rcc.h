#ifndef RCC_H
#define RCC_H

#ifdef __cplusplus
extern "C"{
#endif

void rcc_enable_gpioa(void);
void rcc_enable_gpiob(void);
void rcc_enable_gpioc(void);
void rcc_enable_gpiod(void);
void rcc_enable_syscfg(void);
void rcc_enable_timer6(void);
void rcc_enable_lpuart1(void);
void rcc_enable_timer2(void);
void rcc_enable_timer3(void);
void rcc_enable_timer4(void);
void rcc_enable_timer5(void);

#ifdef __cplusplus
}
#endif

#endif
