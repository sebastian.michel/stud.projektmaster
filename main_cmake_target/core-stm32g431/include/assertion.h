#ifndef ASSERT_H
#define ASSERT_H

#ifdef __cplusplus
#include <cstdbool>

extern "C"{
#else
#include <stdbool.h>
#endif

#undef assert
void assert(bool assertion);

#ifdef __cplusplus
}
#endif

#endif
