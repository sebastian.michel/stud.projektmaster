//
// Created by seb on 03.12.21.
//

#ifndef ZEBOS_LPUART_H
#define ZEBOS_LPUART_H

#ifdef __cplusplus
extern "C" {
#endif

#include "assertion.h"
#include "gpio.h"
#include "rcc.h"

void usart_set_data_bits(USART_TypeDef* usart, unsigned n_data_bits);

typedef enum {
    USART_PARITY_None,
    USART_PARITY_Even,
    USART_PARITY_Odd,
} USART_Parity;

void usart_set_parity(USART_TypeDef* usart, USART_Parity parity);

typedef enum {
    USART_STOP_BITS_1 = 0b00,
    USART_STOP_BITS_1_5 = 0b01,
    USART_STOP_BITS_2 = 0b10,
} USART_STOP_BITS;


void usart_set_stop_bits(USART_TypeDef* usart, USART_STOP_BITS stopBits);

void usart_set_baud_rate(USART_TypeDef* usart, unsigned baudrate);

void usart_enable_transmit(USART_TypeDef* usart);
void usart_enable_receive(USART_TypeDef* usart);

void usart_send_byte(USART_TypeDef* usart, unsigned char byte);
void usart_send(USART_TypeDef* usart, const char* characters, unsigned len);

void usart_enable_fifo(USART_TypeDef* usart);
void usart_enable(USART_TypeDef* usart);
bool usart_send_queue_empty(USART_TypeDef* usart);


void lpuart1_init(void);

#ifdef __cplusplus
}
#endif

#endif//ZEBOS_LPUART_H
