//
// Created by seb on 25.11.20.
//

#ifndef ZEBOS_MEM_MANAGE_FAULT_MAGIC_H
#define ZEBOS_MEM_MANAGE_FAULT_MAGIC_H
#ifdef __cplusplus
extern "C" {
#endif
void init_memmanage_magic();
void disable_process_ro_data();
void reenable_process_ro_data();

#ifdef __cplusplus
}

#endif

#endif //ZEBOS_MEM_MANAGE_FAULT_MAGIC_H
