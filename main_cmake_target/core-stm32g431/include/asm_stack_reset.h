//
// Created by seb on 10.12.21.
//

#ifndef ZEBOS_ASM_STACK_RESET_H
#define ZEBOS_ASM_STACK_RESET_H

#include <cstdint>
#include "global_variables.h"

extern "C"{
    void asm_stack_reset();
    //inline unsigned stack_reset_saved_pc __attribute__((section("os_proc_shared_rw"))) __attribute__((used));
	extern uint32_t _rom_os_proc_shared_rw;
	extern uint32_t _os_proc_shared_rw;
	extern uint32_t _os_proc_shared_rw_end;

};

#endif//ZEBOS_ASM_STACK_RESET_H
