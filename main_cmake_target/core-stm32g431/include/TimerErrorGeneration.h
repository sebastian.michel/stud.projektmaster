//
// Created by seb on 22.12.21.
//

#ifndef ZEBOS_TIMERERRORGENERATION_H
#define ZEBOS_TIMERERRORGENERATION_H


#include <cstdint>
namespace zebos{
    class TimerErrorGeneration {

    public:
        TimerErrorGeneration();
        void enableInterrupt();
        void disableInterrupt();

        void prescaler(uint32_t prescaler_value);
        void setReloadValue(uint32_t reload_value);

        void start();
    };
}


#endif//ZEBOS_TIMERERRORGENERATION_H
