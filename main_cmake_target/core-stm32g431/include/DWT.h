//
// Created by seb on 17.12.21.
//

#ifndef ZEBOS_DWT_H
#define ZEBOS_DWT_H

#include <cstdint>

void enableDWT();
void reset_dwt_cycles();
uint32_t get_dtw_cycles();

#endif//ZEBOS_DWT_H
