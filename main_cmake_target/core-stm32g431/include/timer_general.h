//
// Created by seb on 06.12.21.
//

#ifndef ZEBOS_TIMER_GENERAL_H
#define ZEBOS_TIMER_GENERAL_H

#include <stm32g4xx.h>

void timer_set_psc_divider(TIM_TypeDef *timer, unsigned value);

IRQn_Type to_interrupt_number(TIM_TypeDef *timer);

void timer_enable_interrupt(TIM_TypeDef *timer);
void timer_set_reload_value(TIM_TypeDef *timer, unsigned value);
void timer_apply(TIM_TypeDef *timer);
void timer_start(TIM_TypeDef *timer);
void timer_stop(TIM_TypeDef *timer);
void timer_stop_at_reload_value(TIM_TypeDef *timer);
void timer_reload(TIM_TypeDef *timer, uint32_t val);
void timer_restart(TIM_TypeDef *timer);
void timer_only_update_on_overflow_underflow(TIM_TypeDef *timer);
void timer_reset(TIM_TypeDef* timer);
uint32_t timer_getValue(TIM_TypeDef* timer);

#endif //ZEBOS_TIMER_GENERAL_H
