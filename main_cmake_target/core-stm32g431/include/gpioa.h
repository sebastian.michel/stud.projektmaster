//
// Created by seb on 03.12.21.
//

#ifndef ZEBOS_GPIOA_H
#define ZEBOS_GPIOA_H
#include <cstdint>

enum class GpioMode : uint8_t{
    Input,
    Output,
    AlternateFunction,
    Analog
};

void gpioa_init();
void gpioa_mode(uint8_t pin, GpioMode mode);
void gpioa_set(uint8_t pin);
void gpioa_reset(uint8_t pin);
void gpioa_mod(uint8_t pin, bool val);
void gpioa_toggle(uint8_t pin);


#endif//ZEBOS_GPIOA_H
