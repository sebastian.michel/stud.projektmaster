//
// Created by seb on 21.11.21.
//
#include <syscalls.h>
#include "error_generator_config.h"

bool jumpCondition = true;

const error_generator_config::Config config{
    .error_address_range = {.startAddress = reinterpret_cast<uint32_t*>(&jumpCondition), .numBytes = 1 },

    .operationMode = error_generator_config::OperationMode::AddressRange,

    .bitposMode = error_generator_config::BitposMode::Defined,
    .bitpos = 0,
    };

void func(){
    while(1);
}

int main(){
    _configure_error_generator(config);
    _generate_error_from_config();

    if(jumpCondition){
        while(1);
    } else {
        func();
    }

	while(1);
}