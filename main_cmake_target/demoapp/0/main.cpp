//#include <stdio.h>
#include <cmath>

#include "SafeDatatype.h"

#include <cstring>

#define N 5000
SafeData<bool> gestrichen[ (N/2) ];

class Test{
public:
	~Test(){
		int a = 0;
	}

};

Test t;
Test t2;
Test t3;
Test t4;

__attribute__((destructor))
void test(){}



int main() {
	//printf("Hello, World!\n");

	//auto n = cos(*reinterpret_cast<uint32_t*>(0x4321));
	//auto n = 1;

#pragma clang diagnostic push
#pragma ide diagnostic ignored "EndlessLoop"

	for (int i = 0; i < N/2; i++){
		gestrichen[i] = false;
	}

	int sqrt_of_n = sqrt(N);

	//printf("%d, ",2);

	for (int i = 3; i <= sqrt_of_n; i++){
		if (i % 2 == 0){continue;}
		int index = (i/2-1);

		if (!gestrichen[index]){
			// i ist eine primzahl

			// streiche seine Vielfachen, beginnend mit i*i
			// (denn k*i mit k<i wurde schon als Vielfaches von k gestrichen)
			for (int j = i*i; j < N; j += i){
				if (j % 2 == 0) { continue; }
				int index_j = (j/2-1);
				gestrichen[index_j] = true;
			}
		}
	}

	_test_done();
	while(1);

#pragma clang diagnostic pop

	return 0;
}

