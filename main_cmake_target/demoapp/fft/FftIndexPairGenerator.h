//
// Created by seb on 04.11.21.
//

#ifndef CPP_FFT_FFTINDEXPAIRGENERATOR_H
#define CPP_FFT_FFTINDEXPAIRGENERATOR_H


#include <cstddef>
#include <iterator>
#include <utility>

class FftIndexPairGenerator {
    using IndexPair = std::pair<size_t, size_t>;
public:
    class iterator {
    public:
        // iterator traits
        using difference_type = ptrdiff_t;
        using value_type = IndexPair;
        using pointer = const IndexPair*;
        using reference = const IndexPair&;
        using iterator_category = std::input_iterator_tag;

        iterator(size_t current_stage, size_t index);

        iterator& operator++();
        iterator operator++(int);

        reference operator*() const;
        pointer operator->() const;
        friend bool operator==(const iterator& _this, const iterator& _other);
        friend bool operator!=(const iterator& _this, const iterator& _other);

    private:
        /*
     * This will yield the start index dependent on the current stage
     *
     *  stage | 0   | 1   | 2
     * ------------------
     *   i=0  | 0   | 0   | 0
     *   i=1  | 1   | 0   | 0
     *   i=2  | 0   | 1   | 0
     *   i=3  | 1   | 1   | 0
     *   i=4  | 0   | 0   | 1
     *   i=5  | 1   | 0   | 1
     *   i=6  | 0   | 1   | 1
     *   i=7  | 1   | 1   | 1
     *        | ... | ... | ...
     */
        [[nodiscard]] static size_t nextStartIndex(size_t currentIndex, size_t current_stage) ;

        static size_t getSecondForStartIndex(size_t index, size_t current_stage);

        static IndexPair getNextIndexPair(size_t current_stage, size_t index);

    private:
        IndexPair indices;
        const size_t cur_butterfly_stage;
    };

public:
    FftIndexPairGenerator(const size_t currentStage, const size_t numFFTSamples);

public:
    iterator begin() const;
    iterator end() const;

private:
    const size_t currentStage;
    const size_t numFFTSamples;
};

#endif//CPP_FFT_FFTINDEXPAIRGENERATOR_H
