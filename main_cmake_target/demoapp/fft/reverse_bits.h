//
// Created by seb on 31.10.2021.
//

#ifndef CPP_FFT_REVERSE_BITS_H
#define CPP_FFT_REVERSE_BITS_H


#include <cstdint>

uint32_t reverse_bits(uint32_t input, uint8_t num_bits);
uint32_t reverse_bits_cortex(uint32_t input, uint8_t num_bits);


#endif//CPP_FFT_REVERSE_BITS_H
