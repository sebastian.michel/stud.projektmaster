//
// Created by seb on 07.12.21.
//

#include "fft.h"
#include "syscalls.h"
#include "fft_num_samples.h"

static constexpr std::array<float, fft_num_samples> fft_samples{
        []() constexpr {
                //array initialisierung
                constexpr size_t size = fft_num_samples;
                std::array<float, size> result{};
                for (int i = 0; i < size; ++i)
                {
                    result[i] = float(i);
                }
                return result;
        } () };

static std::array<std::complex<float>, fft_num_samples>  fft_result __attribute__((used));

int main() {
	_compare_result_memory(reinterpret_cast<uint32_t*>(&fft_result), sizeof(fft_result) / 4);
    fft(fft_samples, fft_result);

    _test_done();
    while (1);
}