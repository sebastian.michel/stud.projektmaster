//
// Created by seb on 21.11.21.
//
#include <syscalls.h>
#include "error_generator_config.h"

/* create error:
 * - in register pc
 * - (do a bitflip) at bitpos 31
 * */

const error_generator_config::Config config{
        .error_registers = static_cast<uint32_t>(error_generator_config::ErrorRegister::PC),
        .error_address_range{},

        .operationMode = error_generator_config::OperationMode::Register,

        .bitposMode = error_generator_config::BitposMode::Defined,
        .bitpos = 31,
};

/*
 * stack und register fehlererkennung / fehlerkorrektur können auf die gleiche art ausgeführt werden
 * da bei beiden unbekannt ist, ob die datenworte dras sind und beide hintereinander im speicher liegen
 */

int main(){
    _configure_error_generator(config);
    _generate_error_from_config();

	while(1);
}