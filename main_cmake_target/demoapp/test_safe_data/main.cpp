//
// Created by seb on 25.11.21.
//

#include <syscalls.h>
#include "SafeDatatype.h"

int main(){
	int a = 42;

	_generate_error(&a, sizeof(a));

	SafeData<int> safe_foo{a};
	safe_foo.operator int();

	while (1);
}