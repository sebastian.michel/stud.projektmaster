//
// Created by seb on 07.12.21.
//

#ifndef ZEBOS_FFT_H
#define ZEBOS_FFT_H

#include "fft_num_samples.h"
#include <array>
#include <complex>
#include <SafeData.h>

void fft(const std::array<float, fft_num_samples>& samples, std::array<SafeData<std::complex<float>>, fft_num_samples>& buffer_out);



#endif //ZEBOS_FFT_H
