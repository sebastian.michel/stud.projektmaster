//
// Created by seb on 04.11.21.
//

#include "FftIndexPairGenerator.h"

FftIndexPairGenerator::iterator::iterator(size_t current_stage, size_t index)
    : indices(getNextIndexPair(current_stage, index)),
      cur_butterfly_stage(current_stage) {}

FftIndexPairGenerator::iterator& FftIndexPairGenerator::iterator::operator++() {
    this->indices = getNextIndexPair(cur_butterfly_stage, indices.first + 1);
    return *this;
}
FftIndexPairGenerator::iterator FftIndexPairGenerator::iterator::operator++(int) {
    return {cur_butterfly_stage, nextStartIndex(indices.first, cur_butterfly_stage)};
}
const FftIndexPairGenerator::IndexPair& FftIndexPairGenerator::iterator::operator*() const {
    return indices;
}
FftIndexPairGenerator::iterator::pointer FftIndexPairGenerator::iterator::operator->() const {
    return &indices;
}
bool operator==(const FftIndexPairGenerator::iterator& _this, const FftIndexPairGenerator::iterator& _other) {
    return _this.indices.first == _other.indices.first;
}
bool operator!=(const FftIndexPairGenerator::iterator& _this, const FftIndexPairGenerator::iterator& _other) {
    return _this.indices.first != _other.indices.first;
}

FftIndexPairGenerator::FftIndexPairGenerator(const size_t currentStage, const size_t numFFTSamples) : currentStage(currentStage), numFFTSamples(numFFTSamples) {}

FftIndexPairGenerator::iterator FftIndexPairGenerator::begin() const {
    return {currentStage, 0};
}
FftIndexPairGenerator::iterator FftIndexPairGenerator::end() const {
    return {currentStage, numFFTSamples};
}

size_t FftIndexPairGenerator::iterator::nextStartIndex(size_t currentIndex, size_t current_stage) {
    while ((currentIndex & (1 << current_stage)) != 0)
    {
        currentIndex++;// increment until next occurence of 0
    }
    return currentIndex;
}
size_t FftIndexPairGenerator::iterator::getSecondForStartIndex(size_t index, size_t current_stage) {
    return index + (1 << current_stage);
}
FftIndexPairGenerator::IndexPair FftIndexPairGenerator::iterator::getNextIndexPair(size_t current_stage, size_t index) {
    const auto next_start_index = nextStartIndex(index, current_stage);
    return {next_start_index, getSecondForStartIndex(next_start_index, current_stage)};
}
