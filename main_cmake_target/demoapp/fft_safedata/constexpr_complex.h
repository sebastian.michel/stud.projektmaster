//
// Created by seb on 08.12.21.
//

#ifndef ZEBOS_CONSTEXPR_COMPLEX_H
#define ZEBOS_CONSTEXPR_COMPLEX_H

//implementations taken from the header <complex> but added constexpr to all functions
template<typename Tp>
constexpr std::complex<Tp> constexpr_polar(const Tp& rho, const Tp& theta)
{
    __glibcxx_assert( rho >= 0 );
    return std::complex<Tp>(rho * std::cos(theta), rho * std::sin(theta));
}

template<typename Tp>
constexpr std::complex<Tp> constexpr_complex_exp(const std::complex<Tp>& z){
    return constexpr_polar<Tp>(std::exp(z.real()), z.imag());
}

using std::complex;
template<typename Tp>
complex<Tp>
        constexpr constexpr_complex_pow_unsigned(complex<Tp> x, unsigned n)
{
    complex<Tp> y = n % 2 ? x : complex<Tp>(1);

    while (n >>= 1)
    {
        x *= x;
        if (n % 2)
            y *= x;
    }

    return y;
}

template<typename Tp>
constexpr inline complex<Tp> constexpr_complex_pow(const complex<Tp>& z, int n)
{
    return n < 0
                   ? complex<Tp>(1) / constexpr_complex_pow_unsigned(z, -(unsigned)n)
                   : constexpr_complex_pow_unsigned(z, n);
}

#endif//ZEBOS_CONSTEXPR_COMPLEX_H
