//
// Created by seb on 07.12.21.
//

#include <array>
#include <complex>
#include "FftIndexPairGenerator.h"
#include "fft.h"
#include "next_power_of_2.h"
#include "reverse_bits.h"
#include "constexpr_complex.h"

template<typename T, std::size_t SIZE>
constexpr std::array<std::complex<T>, SIZE> get_twiddle_factors() {
	std::array<std::complex<T>, SIZE> twiddle_factors;

	using std::numbers::pi_v;

	const auto twiddle_base_complex_exponent = std::complex<T>(0, -1 * 2 * pi_v<T> / SIZE);
	const auto twiddle_base = constexpr_complex_exp(twiddle_base_complex_exponent);// e^(-I * 2pi / NUM_FFT_SAMPLES)

	twiddle_factors[0] = 1;           // twiddle_base ^ 0 = 1
	twiddle_factors[1] = twiddle_base;// twiddle_base ^ 1 = twiddle_base

	std::generate(
			twiddle_factors.begin() + 2, twiddle_factors.end(),
			[i = 2, &twiddle_base]() mutable {

                return constexpr_complex_pow(twiddle_base, i++);
			});

	return twiddle_factors;
}

size_t fast_modulo_power_of_two(size_t number, size_t mod){
	/*
	 * https://stackoverflow.com/questions/6670715/mod-of-power-2-on-bitwise-operators
	 * ...
	 * So in other words, "number mod 4" is the same as "number & 00000011"
	 */
	return number & (mod-1);
}

size_t get_twiddle_index(const size_t idx_sample, const size_t butterfly_stage, const size_t num_samples_fft){
	size_t a = (num_samples_fft >> (butterfly_stage + 1) );

	auto first = (idx_sample * a);
	const auto mod = (num_samples_fft / 2);

	return first % mod;
}

template <typename T, std::size_t SIZE>
T get_sample_or_zero(const size_t idx, const std::array<T, SIZE>& samples){
	if(idx > samples.size()){
		return 0;
	}
	return samples[idx];
}

template <size_t SIZE>
constexpr std::array<std::complex<float>, SIZE> twiddle_factors;

template <>
constexpr std::array<std::complex<float>, fft_num_samples> twiddle_factors<fft_num_samples> = get_twiddle_factors<float, fft_num_samples>();

template<size_t SIZE>
void fft_impl(const std::array<float, SIZE>& samples, std::array<SafeData<std::complex<float>>, fft_num_samples>& buffer_out) {
	using std::array, std::complex;

	// for this algorithm to work, the sample size must be a power of 2
	// because of this, calculate which is the next power of 2
	const auto fft_size = next_power_of_2(samples.size());

	const auto butterfly_graph_depth = log2_round_up(fft_size);

	for (size_t butterfly_stage = 0; butterfly_stage < butterfly_graph_depth; butterfly_stage++)
	{
		FftIndexPairGenerator indexPairGenerator(butterfly_stage, fft_size);
		for (auto [idx_first, idx_second] : indexPairGenerator)
		{
			complex<float> pair_val_1, pair_val_2;
			if (butterfly_stage == 0)
			{
				pair_val_1 = get_sample_or_zero(reverse_bits_cortex(idx_first, butterfly_graph_depth), samples);
				pair_val_2 = get_sample_or_zero(reverse_bits_cortex(idx_second, butterfly_graph_depth), samples);
			} else {
				pair_val_1 = buffer_out[idx_first];
				pair_val_2 = buffer_out[idx_second];
			}

			const auto twiddle_index = get_twiddle_index(idx_second,butterfly_stage,fft_size);
			auto twiddle_factor = twiddle_factors<SIZE>[twiddle_index];
			pair_val_2 = pair_val_2* twiddle_factor;

            buffer_out[idx_first] = pair_val_1 + pair_val_2;
            buffer_out[idx_second] = pair_val_1 - pair_val_2;
		}
	}
}

void fft(const std::array<float, fft_num_samples>& samples, std::array<SafeData<std::complex<float>>, fft_num_samples>& buffer_out){
	return fft_impl(samples, buffer_out);
}