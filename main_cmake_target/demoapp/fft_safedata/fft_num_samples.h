//
// Created by seb on 08.12.21.
//

#ifndef ZEBOS_FFT_NUM_SAMPLES_H
#define ZEBOS_FFT_NUM_SAMPLES_H

constexpr unsigned fft_num_samples = 1024;

#endif//ZEBOS_FFT_NUM_SAMPLES_H
