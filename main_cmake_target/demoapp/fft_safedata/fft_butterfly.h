//
// Created by seb on 30.10.2021.
//

#ifndef CPP_FFT_FFT_BUTTERFLY_H
#define CPP_FFT_FFT_BUTTERFLY_H

#include "next_power_of_2.h"
#include "reverse_bits.h"
#include <algorithm>
#include <complex>
#include <iostream>
#include <numbers>
#include <ranges>
#include <vector>
#include "FftIndexPairGenerator.h"


template<typename T>
static std::vector<std::complex<T>>
get_twiddle_factors(const size_t num_twiddle_factors) {
    std::vector<std::complex<T>> twiddle_factors(num_twiddle_factors);

    using std::numbers::pi_v;

    const auto twiddle_base = std::exp(std::complex<T>(0, -1 * 2 * pi_v<T> / num_twiddle_factors));// e^(-I * 2pi / NUM_FFT_SAMPLES)

    twiddle_factors[0] = 1;           // twiddle_base ^ 0 = 1
    twiddle_factors[1] = twiddle_base;// twiddle_base ^ 1 = twiddle_base

    std::generate(
            twiddle_factors.begin() + 2, twiddle_factors.end(),
            [i = 2, &twiddle_base]() mutable {
                return std::pow(twiddle_base, i++);
            });

    return twiddle_factors;
}

size_t fast_modulo_power_of_two(size_t number, size_t mod){
    /*
     * https://stackoverflow.com/questions/6670715/mod-of-power-2-on-bitwise-operators
     * ...
     * So in other words, number mod 4 is the same as number & 00000011
     */
    return number & (mod-1);
}

size_t get_twiddle_index(const size_t idx_sample, const size_t butterfly_stage, const size_t num_samples_fft){
    size_t a = (num_samples_fft >> (butterfly_stage + 1) );

    auto first = (idx_sample * a);
    const auto mod = (num_samples_fft / 2);

    return first % mod;
    //return fast_modulo_power_of_two(first,mod);
}

template <typename T>
T get_sample_or_zero(const size_t idx, const std::vector<T>& samples){
    if(idx > samples.size()){
        return 0;
    }
    return samples[idx];
}

template<typename TReturn, typename TIn>
std::vector<std::complex<TReturn>> fft(const std::vector<TIn>& samples) {
    using std::vector, std::complex;
    using std::cout, std::endl;
    using std::ranges::iota_view;

    // for this algorithm to work, the sample size must be a power of 2
    // because of this, calculate which is the next power of 2
    const auto fft_size = next_power_of_2(samples.size());

    auto fft_output = vector<complex<TReturn>>(fft_size);
    auto twiddle_factors = get_twiddle_factors<TReturn>(fft_size);
    const auto butterfly_graph_depth = log2_round_up(fft_size);


    for (size_t butterfly_stage : iota_view(size_t{0}, butterfly_graph_depth))
    {
        FftIndexPairGenerator indexPairGenerator(butterfly_stage, fft_size);
        for (auto [idx_first, idx_second] : indexPairGenerator)
        {
            complex<TReturn> pair_val_1, pair_val_2;
            if (butterfly_stage == 0)
            {
                pair_val_1 = get_sample_or_zero(reverse_bits(idx_first, butterfly_graph_depth), samples);
                pair_val_2 = get_sample_or_zero(reverse_bits(idx_second, butterfly_graph_depth), samples);
            } else {
                pair_val_1 = fft_output[idx_first];
                pair_val_2 = fft_output[idx_second];
            }

            const auto twiddle_index = get_twiddle_index(idx_second,butterfly_stage,fft_size);
            auto twiddle_factor = twiddle_factors[twiddle_index];
            pair_val_2 = pair_val_2* twiddle_factor;

            fft_output[idx_first] = pair_val_1 + pair_val_2;
            fft_output[idx_second] = pair_val_1 - pair_val_2;
        }
    }

    return fft_output;
}

#endif// CPP_FFT_FFT_BUTTERFLY_H
