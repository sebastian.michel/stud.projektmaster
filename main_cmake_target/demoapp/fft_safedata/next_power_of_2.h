//
// Created by seb on 30.10.2021.
//

#ifndef CPP_FFT_NEXT_POWER_OF_2_H
#define CPP_FFT_NEXT_POWER_OF_2_H


#include <cstddef>
std::size_t log2_round_up(std::size_t v);
std::size_t next_power_of_2(std::size_t v);

#endif//CPP_FFT_NEXT_POWER_OF_2_H
