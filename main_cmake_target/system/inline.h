//
// Created by seb on 19.04.21.
//

#ifndef ZEBOS_INLINE_H
#define ZEBOS_INLINE_H

#define FORCE_INLINE inline __attribute__((always_inline))

#endif //ZEBOS_INLINE_H
