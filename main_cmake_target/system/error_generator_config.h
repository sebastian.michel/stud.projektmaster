//
// Created by seb on 21.11.21.
//

#ifndef ZEBOS_ERROR_GENERATOR_CONFIG_H
#define ZEBOS_ERROR_GENERATOR_CONFIG_H

#include <cstdint>
#include <bitset>
#include <array>

namespace error_generator_config{
    enum class ErrorRegister  {
        R0   ,
        R1   ,
        R2   ,
        R3   ,
        R4   ,
        R5   ,
        R6   ,
        R7   ,
        R8   ,
        R9   ,
        R10  ,
        R11  ,
        R12  ,

		S16  ,
		S17  ,
		S18  ,
		S19  ,
		S20  ,
		S21  ,
		S22  ,
		S23  ,
		S24  ,
		S25  ,
		S26  ,
		S27  ,
		S28  ,
		S29  ,
		S30  ,
		S31  ,

        SP   ,
        PC   ,
        LR   ,
        xPSR ,
        S0   ,
        S1   ,
        S2   ,
        S3   ,
        S4   ,
        S5   ,
        S6   ,
        S7   ,
        S8   ,
        S9   ,
        S10  ,
        S11  ,
        S12  ,
        S13  ,
        S14 ,
        S15 ,
        FPSCR,
        SIZE
        };

    enum class OperationMode {
        Disabled,
        AddressRange,
        Register
    };

    enum class BitposMode {
        Random,
        Defined
    };

    struct Range{
        uint32_t* startAddress;
        uint32_t numBytes;

        bool isValid(){
            return (startAddress != nullptr );
        }

        uint32_t get_num_bytes() const{
            return numBytes; // end address is inclusive, so add 1
        }
    };

    struct Config {
        std::array<bool,static_cast<unsigned>(ErrorRegister::SIZE)> error_registers;

        Range error_address_range = Range{.startAddress = nullptr, .numBytes = 0 };

        OperationMode operationMode = OperationMode::Disabled;

        BitposMode bitposMode = BitposMode::Random;
        uint8_t bitpos = 0;
    };

}

#endif//ZEBOS_ERROR_GENERATOR_CONFIG_H
