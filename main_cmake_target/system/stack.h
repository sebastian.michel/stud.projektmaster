//
// Created by seb on 03.11.20.
//

#ifndef ZEBOS_STACK_H
#define ZEBOS_STACK_H

#include <stdint.h>

template<typename T, uint32_t Capacity>
class Stack{
public:
    void push(T val){
        if(current_pos == Capacity){ return; }

        memory[current_pos] = val;
		current_pos++;
    }

    T pop(){
        if(current_pos > 0) current_pos--;
        return memory[current_pos];
    }

    inline uint32_t num_elements() { return current_pos; }
    inline uint32_t num_elements_free() { return Capacity - current_pos; }
private:
    T memory[Capacity];
    uint32_t current_pos = 0;
};

#endif //ZEBOS_STACK_H
