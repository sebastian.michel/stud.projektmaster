//
// Created by seb on 23.11.20.
//

#ifndef ZEBOS_SVC_MACROS_H
#define ZEBOS_SVC_MACROS_H

//unfortunately the svc instruction requires an immediate value in the instruction
//so we need to build the whole instruction at compile time
//macros to the rescue


// macro magic to concat a preprocessor number and a string
#define SVC_OPCODE "SVC #"
#define PARAM_TO_STR(param) #param
#define SVC(param) SVC_OPCODE PARAM_TO_STR(param)

#endif //ZEBOS_SVC_MACROS_H
