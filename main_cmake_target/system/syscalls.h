//
// Created by seb on 02.11.20.
//

#ifndef ZEBOS_DEMOAPP_SYSCALLS_H
#define ZEBOS_DEMOAPP_SYSCALLS_H

#ifdef __cplusplus
#include "error_generator_config.h"
#endif
#include "inline.h"
#include "svc_macros.h"
#include <stdint.h>

//required to be a define, see docs/error error-swi-relocation.txt

#define SVC_EXIT            0
#define SVC_FORK            1
#define SVC_FIRST_CONTEXT   2
#define SVC_VALUE_COMPARE   3
#define SVC_GENERATE_ERROR  4
#define SVC_YIELD           5
#define SVC_SET_ERROR_GENERATOR_CONFIG  6
#define SVC_GENERATE_ERROR_FROM_CONFIG  7
#define SVC_SCHEDULE_ERROR_CORRECTION 8
#define SVC_TEST_DONE 9
#define SVC_COMPARE_RESULT_MEMORY 10


// __attribute__ ((noipa)) is required so we can be sure that the arguments are
// placed in the correct registers, disregarding any optimizations that might be happening
#define SVC_WITH_ARGUMENTS __attribute__ ((noipa))

SVC_WITH_ARGUMENTS static void _exit(uint32_t exitcode) {
    __asm volatile ( SVC(SVC_EXIT) );
    // Best practice is to always terminate non-returning functions with while(1) (from https://www.keil.com/support/man/docs/armcc/armcc_chr1359124976881.htm)
    while(1);
}



static inline int _fork() {
    __asm__ volatile ( SVC(SVC_FORK) );
    //todo return value
}

static inline void _first_context(){
    __asm__ volatile ( SVC(SVC_FIRST_CONTEXT) );
}

static inline void _yield(){
    __asm__ volatile ( SVC(SVC_YIELD) );
}

SVC_WITH_ARGUMENTS static void _value_compare(void* address, uint32_t numBytes )  {
	__asm__ volatile ( SVC(SVC_VALUE_COMPARE) );
}

#ifdef __cplusplus
SVC_WITH_ARGUMENTS static void _configure_error_generator(const error_generator_config::Config& config){
    __asm__ volatile ( SVC(SVC_SET_ERROR_GENERATOR_CONFIG) );
}
#endif

static void _generate_error_from_config(){
    __asm__ volatile ( SVC(SVC_GENERATE_ERROR_FROM_CONFIG) );
}

SVC_WITH_ARGUMENTS static void _generate_error(void* address, uint32_t numBytes){
    __asm__ volatile ( SVC(SVC_GENERATE_ERROR) );
}

static void _schedule_error_correction(){
	__asm__ volatile ( SVC(SVC_SCHEDULE_ERROR_CORRECTION) );
}

SVC_WITH_ARGUMENTS static void _compare_result_memory(uint32_t * adr, uint32_t numWords){
	__asm__ volatile ( SVC(SVC_COMPARE_RESULT_MEMORY) );
}

static void _test_done(){
	__asm__ volatile ( SVC(SVC_TEST_DONE) );
}

#endif //ZEBOS_DEMOAPP_SYSCALLS_H
