//
// Created by seb on 01.11.20.
//

#include "syscalls.h"

extern void __libc_init_array(); // defined in newlibs init.c
extern void __libc_fini_array(); // defined in newlibs fini.c
extern int main(void);

__attribute__((noreturn))
void exit(int exitcode){
	__libc_fini_array(); // static object destruction and functions with gcc attribute "destructor"

	_exit(exitcode); // system call
	while(1); // do not return (explicitly for safety reasons)
}

void __start(){
	__libc_init_array(); //static object initialization and functions with gcc attribute "constructor"

	main(); // call application entry
	exit(0); // exit normally
}
