//
// Created by seb on 19.03.21.
//

#ifndef ZEBOS_ERRORCHECKCOMPARE_H
#define ZEBOS_ERRORCHECKCOMPARE_H

#include "inline.h"
#include "syscalls.h"

template < typename T >
class SafeData{
public:
    SafeData ()  = default;
	SafeData (T value_) : value(value_) {}

	//Wert schreiben
	SafeData& operator = (const T& other){
	    this->value = other;
	    return *this;
	}

    // Wert lesen
    // das hier muss zwingend inline sein, sonst zeigt der PC, welcher zwischengespeichert wird für den Prozessdatenvergleich immer auf diese Funktion
	inline operator T() {
	    //System-Aufruf
	    _value_compare(&value, sizeof(value));

		return value;
	}

private:
	T value;

};

#endif //ZEBOS_ERRORCHECKCOMPARE_H
